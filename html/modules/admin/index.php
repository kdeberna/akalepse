<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">

  <title></title>
  <link rel="stylesheet" media="screen" href="styles/uploader.css">
  <script src="scripts/multiuploadprototype.js"></script>
  <script src="scripts/init-multiupload.js"></script>
</head>
<body>
    
<section id="wrapper">
    <h1>Upload your files :</h1>
    <input id="multi-upload" type="file" />
</section>

</body>
</html>