require(["dojo/ready", "dijit/Dialog"], function(ready, Dialog){
    ready(function(){
        // create the dialog
        myDialog = new Dialog({
            title: "Delete Entry",
            id: 'ModalDialog',
            draggable: false,
            autofocus: false,
            style: "width: 350px;",
            onLoad: function() {
                var cancel = dojo.byId('CancelButton');
                dojo.connect(cancel, 'onclick', function(e) {
                        dojo.stopEvent(e);
                        myDialog.hide();
                });
            }
        });
        dojo.query('button.delete').forEach(function(item){
			dojo.connect(item, 'onclick', function(e){
				dojo.stopEvent(e);
				var what = e.target.value;
				var path = e.target.parentNode.pathname;
				myDialog.set('href', path);
				myDialog.show();
			});
        });
    });
});