require([
	"dojo/dom",
	"dojo/_base/connect",
	"dojo/_base/xhr",
	"dojo/domReady!"], function(dom, connect, xhr2) {
	
	var album_cover = dom.byId('album_cover_image').value;
	var album_interior = dom.byId('album_interior_image').value;
	var album_back_cover = dom.byId('album_back_cover_image').value;

	if(album_cover != '') {
		dojo.place('<img id="AlbumCover" src="/releases/' + dojo.byId('dir').value + '/' + album_cover + '" width="220" />', dom.byId('UploadZone'), 'last');
	}
	if(album_interior != '') {
		dojo.place('<img id="AlbumInterior" src="/releases/' + dojo.byId('dir').value + '/' + album_interior + '" width="220" />', dom.byId('UploadZone2'), 'last');
	}
	if(album_back_cover != '') {
		dojo.place('<img id="AlbumBackCover" src="/releases/' + dojo.byId('dir').value + '/' + album_back_cover + '" width="220" />', dom.byId('UploadZone3'), 'last');
	}

	var xhr = new XMLHttpRequest();
	xhr.open("POST", '/admin/discography/tracks/' + dojo.byId('id').value + '/', true);  
	xhr.onreadystatechange = function() {
		if(xhr.readyState == 4 && xhr.status == 200) {
			if(xhr.responseText != '') {
				dojo.place(xhr.responseText, dom.byId('Tracklist'), 'last');
				dojo.query('#Tracklist tr td a').forEach(function(node, index, err) {
					connect.connect(node, 'onclick', function(e) {
						dojo.stopEvent(e);
						dojo.destroy(node.parentNode.parentNode);
					});
				});
			}
		} else {
			// error message for bad upload
		}
	}
	xhr.send();
});