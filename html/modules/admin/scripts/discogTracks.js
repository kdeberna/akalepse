dojo.require("dijit.form.Button");
dojo.require("dojox.editor.plugins.PrettyPrint");
dojo.require("dijit._editor.plugins.ViewSource");

require([
	"dojo/dom",
	"dojo/_base/connect",
	"dojo/_base/xhr",
	"dojo/domReady!",
	"dojo/dom-construct"], function(dom, connect, xhr2, domConstruct) {

    var tracks = dom.byId('Tracklist');
    var tracksForm = dom.byId('tracksForm-ContentPane');
    var file = dom.byId('tracksZip');
    var addTrackButton = dom.byId('AddNewTrack');
    var increment = 0;
    var filename;
	var uploadID = dom.byId('tracksZip').previousSibling.previousSibling.value;

	dojo.place('<tr><th class="track">Track</th><th class="artist">Artist</th><th class="file">File</th><th class="actions">Actions</th></tr>', tracks, 'last');

    connect.connect(addTrackButton, 'onclick', function() {
		dojo.place('<tr id="track' + increment + '"><td><input type="text" name="track[]" class="trackInput" value="Track Name" /></td><td><input type="text" name="artist[]" class="artistInput" value="Artist Name" /></td><td><input type="hidden" name="filename[]" value="" /></td><td><button>Upload MP3</button> <a href="delete_track"><img src="/images/icons/delete.png" width="18" height="18" border="0" /></a></td></tr>', tracks, 'last');
		dojo.query('#Tracklist tr#track' + increment + ' td a').forEach(function(node, index, err) {
			connect.connect(node, 'onclick', function(e) {
				dojo.stopEvent(e);
				dojo.destroy(node.parentNode.parentNode);
			});
		});
		dojo.query('#Tracklist tr#track' + increment + ' td button').forEach(function(node, index, err) {
			connect.connect(node, 'onclick', function(e) {
				dojo.stopEvent(e);
				var event = document.createEvent("HTMLEvents");
				event.initEvent("click", false, true);
				file.dispatchEvent(event);
				filename = node.parentNode.previousElementSibling;
			});
		});
		increment++;
    });
    
    connect.connect(file, 'onchange', function() {
		var xhr = new XMLHttpRequest();
		//if(xhr.upload && file.type == "image/jpeg" && file.size <= dojo.byId("MAX_FILE_SIZE").value) {
		xhr.open("POST", '/admin/discography/unzip/', true);
		xhr.upload.onprogress = function(e) {
			dom.byId('Tools-label').innerHTML = 'Upload Progress: ' + (e.loaded / e.total) * 100 + '%';
			//console.log('Loaded: ' + (e.loaded / e.total) * 100 + '%');
		}
		xhr.setRequestHeader("X_FILENAME", file.files[0].fileName);
		xhr.onreadystatechange = function() {
			if(xhr.readyState == 4 && xhr.status == 200) {
				if(xhr.responseText.substring(0,1) == ':') {
					filename.innerHTML = xhr.responseText.substring(1) + '<input type="hidden" name="filename[]" value="' + xhr.responseText.substring(1) + '" />';
					filename = null;
				} else {
					dojo.place(xhr.responseText, tracks, 'last');
					dojo.style(file, 'display', 'none');
					dojo.query('#Tracklist tr td a').forEach(function(node, index, err) {
						connect.connect(node, 'onclick', function(e) {
							dojo.stopEvent(e);
							dojo.destroy(node.parentNode.parentNode);
						});
					});
				}
			} else {
				// error message for bad upload
			}
		}
		xhr.send(file.files[0]);
		// getProgress();
    });

    connect.connect(dom.byId('album_cover'), 'onchange', function() {
		var xhr = new XMLHttpRequest();
		//if(xhr.upload && file.type == "image/jpeg" && file.size <= dojo.byId("MAX_FILE_SIZE").value) {
		xhr.open("POST", '/admin/discography/upload/', true);  
		xhr.setRequestHeader("X_FILENAME", dom.byId('album_cover').files[0].fileName);
		xhr.onreadystatechange = function() {
			if(xhr.readyState == 4 && xhr.status == 200) {
				if(dom.byId('AlbumCover')) {
					dojo.place('<img id="AlbumCover" src="/releases/tmp/' + xhr.responseText + '" width="220" />', dom.byId('AlbumCover'), 'replace');
				} else {
					dojo.place('<img id="AlbumCover" src="/releases/tmp/' + xhr.responseText + '" width="220" />', dom.byId('UploadZone'), 'last');
				}
			} else {
				// error message for bad upload
			}
		}
		xhr.send(dom.byId('album_cover').files[0]);  
    });

    connect.connect(dom.byId('album_interior'), 'onchange', function() {
		var xhr = new XMLHttpRequest();
		//if(xhr.upload && file.type == "image/jpeg" && file.size <= dojo.byId("MAX_FILE_SIZE").value) {
		xhr.open("POST", '/admin/discography/upload/', true);  
		xhr.setRequestHeader("X_FILENAME", dom.byId('album_interior').files[0].fileName);
		xhr.onreadystatechange = function() {
			if(xhr.readyState == 4 && xhr.status == 200) {
				if(dom.byId('AlbumInterior')) {
					dojo.place('<img id="AlbumInterior" src="/releases/tmp/' + xhr.responseText + '" width="220" />', dom.byId('AlbumInterior'), 'replace');
				} else {
					dojo.place('<img id="AlbumInterior" src="/releases/tmp/' + xhr.responseText + '" width="220" />', dom.byId('UploadZone2'), 'last');
				}
			} else {
				// error message for bad upload
			}
		}
		xhr.send(dom.byId('album_interior').files[0]);  
    });

    connect.connect(dom.byId('album_back_cover'), 'onchange', function() {
		var xhr = new XMLHttpRequest();
		//if(xhr.upload && file.type == "image/jpeg" && file.size <= dojo.byId("MAX_FILE_SIZE").value) {
		xhr.open("POST", '/admin/discography/upload/', true);  
		xhr.setRequestHeader("X_FILENAME", dom.byId('album_back_cover').files[0].fileName);
		xhr.onreadystatechange = function() {
			if(xhr.readyState == 4 && xhr.status == 200) {
				if(dom.byId('AlbumBackCover')) {
					dojo.place('<img id="AlbumBackCover" src="/releases/tmp/' + xhr.responseText + '" width="220" />', dom.byId('AlbumBackCover'), 'replace');
				} else {
					dojo.place('<img id="AlbumBackCover" src="/releases/tmp/' + xhr.responseText + '" width="220" />', dom.byId('UploadZone3'), 'last');
				}
			} else {
				// error message for bad upload
			}
		}
		xhr.send(dom.byId('album_back_cover').files[0]);  
    });
    
});