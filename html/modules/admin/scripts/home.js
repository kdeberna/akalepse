dojo.require("dojo.dnd.Source");
dojo.require('dijit.Dialog');
dojo.require("dojo.store.Memory");
dojo.require("dojo.store.Observable");

var akalepse = akalepse || {};

akalepse.collection = dojo.store.Observable(new dojo.store.Memory({idProperty: 'id'}));
akalepse.dropZone = '';

akalepse.image = {

	connectElement: function(element) {
		var actions = element.childNodes[1];
		
		dojo.connect(actions, 'onmouseenter', function() {
			var fadeArgs = {
				node: actions,
				duration: 250
			};
			dojo.fadeIn(fadeArgs).play();
		});
		dojo.connect(actions, 'onmouseleave', function() {
			var fadeArgs = {
				node: actions,
				duration: 250
			};
			dojo.fadeOut(fadeArgs).play();
		});

		dojo.connect(element, 'onclick', function(event) {
			var imageName = this.firstChild.src;
			imageName = imageName.substring(imageName.lastIndexOf('/')+1);
			var thisImage = akalepse.collection.get(imageName);
			
			switch(event.target.id) {
				case 'Link':
					dojo.byId('CCImage').src = thisImage.source;
					dojo.byId('CaptionInput').value = thisImage.url;
					dijit.byId('CCDialog').show();
					break;
				case 'Delete':
					akalepse.collection.remove(imageName);

// 					if(thisImage.databaseID == '') {
// 						thisImage.databaseID = 'invalid';
// 					}
// 		
// 					var xhrArgs = {
// 						url: "/admin/home/clearimage/?id=" + thisImage.databaseID + "&filename=" + thisImage.id,
// 						form: theForm,
// 						handleAs: "text",
// 						load: function(response, ioArgs) {
// 							//
// 						},
// 						error: function(error) {
// 							//
// 						}
// 					};
// 					dojo.xhrPost(xhrArgs);
// 

// 					var xhrArgs = {
// 						url: "/admin/home/clearimage?filename=" + thisImage.filename,
// 						handleAs: "text",
// 						//postData: '?filename=' + thisImage.filename,
// 						load: function(data){
// 							akalepse.collection.remove(imageName);
// 						},
// 						error: function(error){
// 							alert(error);
// 						}
// 					}
// 					var deferred = dojo.xhrPost(xhrArgs);
					break;
			}
		});
	},

	createElement: function(source) {
		var div = document.createElement('div');
		div.setAttribute('class', 'slide dojoDndItem');
		dojo.style(div, 'opacity', '0');

		var actions = document.createElement('div');
		actions.setAttribute('class', 'slideAction');

		var link = document.createElement('div');
		link.setAttribute('class', 'actionOption');
		link.innerHTML = 'Link';
		link.id = 'Link';

		var remove = document.createElement('div');
		remove.setAttribute('class', 'actionOption');
		remove.innerHTML = 'Delete';
		remove.id = 'Delete';

		var image = document.createElement('img');
		image.src = source;

		div.appendChild(image);
		actions.appendChild(link);
		actions.appendChild(remove);
		div.appendChild(actions);

		var arr = [];
		arr.push(div);
		akalepse.dropZone.insertNodes(false, arr, true, dojo.byId('Clear'));

		var fadeArgs = {
			node: div,
			duration: 1000
		};
		dojo.fadeIn(fadeArgs).play();
		
		return div;
	}
};

// file drag hover
function FileDragHover(e) {
	e.stopPropagation();
	e.preventDefault();
	if(e.target.id == 'TheSlides'){
		e.target.className = (e.type == "dragover" ? "hover" : "");
	}
}

// file selection
function FileSelectHandler(e) {
	// cancel event and hover styling
	FileDragHover(e);
	// fetch FileList object
	var files = e.target.files || e.dataTransfer.files;
	// process all File objects
	for (var i = 0, f; f = files[i]; i++) {
		UploadFile(f);
	}
}

function UploadFile(file) {
	var xhr = new XMLHttpRequest();
	//if(xhr.upload && file.type == "image/jpeg" && file.size <= dojo.byId("MAX_FILE_SIZE").value) {
	var response = akalepse.image.createElement('/images/backgrounds/loading.gif');
	xhr.open("POST", '/admin/home/upload/', true);  
	xhr.setRequestHeader("X_FILENAME", file.name);
	xhr.onreadystatechange = function() {
		if(xhr.readyState == 4 && xhr.status == 200) {
			akalepse.collection.add({
				id: file.name,
				databaseID: '',
				elementID: response.id,
				url: '',
				source: '/images/home_slideshow/' + xhr.responseText,
			});
			dojo.style(response.firstChild, 'opacity', '0');
			response.firstChild.src = '/images/home_slideshow/' + xhr.responseText;
			var args = {
				node: response.firstChild,
				duration: 500
			};
			dojo.fadeIn(args).play();
			akalepse.image.connectElement(response);
 			} else {
 				// error message for bad upload
 			}
			dojo.animateProperty({
				node: dojo.byId('SaveChanges'),
				duration: 300,
				properties: {
					opacity: 1
				}
			}).play();
		}
		xhr.send(file);  
	//}
}

dojo.addOnLoad(home);

function home() {
	var filedrag = dojo.byId("TheSlides");
	akalepse.dropZone = new dojo.dnd.AutoSource('TheSlides');

	var xhrArgs = {
      url: "/admin/home/",
      handleAs: "json",
      load: function(data){
		dojo.forEach(data, function(item, index){
			var response = akalepse.image.createElement('/images/home_slideshow/' + item._image_url);
			akalepse.collection.add({
 				id: item._image_url,
 				databaseID: item._id,
 				elementID: response.id,
 				url: item._caption,
 				order: item._order,
 				source: '/images/home_slideshow/' + item._image_url
			});
			akalepse.image.connectElement(response);
		});
      },
      error: function(error){
        dojo.byId("TheSlides").innerHTML = "Error retreiving images: " + error;
      }
    }
    var deferred = dojo.xhrPost(xhrArgs);

	var results = akalepse.collection.query();
	var observeHandle = results.observe(function(object, removedFrom, insertedInto){
		if(removedFrom > -1){ // existing object removed
			var args = {
				node: dojo.byId(object.elementID),
				duration: 500,
				onEnd: function() {
				dojo.animateProperty({
					node: dojo.byId(object.elementID),
					properties: {
						width: 0,
						margin: 0,
						padding: 0,
						border: 'none'
					},
					onEnd: function() {
						dojo.byId('TheSlides').removeChild(dojo.byId(object.elementID))
					}
					}).play();
				}
			};
			dojo.fadeOut(args).play();
			if(object.databaseID == '') {
				object.databaseID = 'invalid';
			}

			var xhrArgs = {
				url: "/admin/home/clearimage/?id=" + object.databaseID + "&filename=" + object.id,
				form: theForm,
				handleAs: "text",
				load: function(response, ioArgs) {
					//
				},
				error: function(error) {
					//
				}
			};
			dojo.xhrPost(xhrArgs);
		}
		if(insertedInto > -1){ // new or updated object inserted
			// console.log(object);
		}
	});

	var myDialog = new dijit.Dialog({
		id: 'CCDialog',
		title: "Set Link",
		style: "width: 600px; height: 200px;",
		draggable: false
	}, dojo.byId('CreditCaption'));
	myDialog.hide();

	dojo.connect(dojo.byId('CCSave'), 'onclick', function(event) {
		var imageName = event.target.parentNode.parentNode.childNodes[1].childNodes[1].src;
		imageName = imageName.substring(imageName.lastIndexOf('/')+1);
		var thisImage = akalepse.collection.get(imageName);
		thisImage.url = dojo.byId('CaptionInput').value;
		akalepse.collection.put(thisImage);
		dojo.forEach(akalepse.dropZone.getAllNodes(), function(item, index){
			var fileUrl = item.firstChild.src;
			var filename = fileUrl.substring(fileUrl.lastIndexOf('/')+1);
			var image = akalepse.collection.get(filename);
			image.order = index;
			// Clean up some fo the json of unecessary objects
			akalepse.collection.put(image);
			dojo.byId('theForm').elements['theShit'].value = JSON.stringify(image);
			var xhrArgs = {
				url: "/admin/home/save/",
				form: theForm,
				handleAs: "text",
				load: function(response, ioArgs) {
					if(response != 'Homepage updated.') {
						image.databaseID = response;
						akalepse.collection.put(image);
					}
				},
				error: function(error) {
					//
				}
			};
			dojo.xhrPost(xhrArgs);
		});
		dijit.byId('CCDialog').hide();
	});

	// is XHR2 available?
	var xhr = new XMLHttpRequest();
	if(xhr.upload) {
		// file drop
		filedrag.addEventListener("dragover", FileDragHover, false);
		filedrag.addEventListener("dragleave", FileDragHover, false);
		filedrag.addEventListener("drop", FileSelectHandler, false);
	}

	dojo.query('.slide').forEach(function(node, index, arr) {
		dojo.connect(node, 'onmouseenter', function() {
			dojo.animateProperty({
				node: node.lastElementChild,
				duration: 200,
				properties: {
					opacity: 1
				}
			}).play();
		});
		dojo.connect(node, 'onmouseleave', function() {
			dojo.animateProperty({
				node: node.lastElementChild,
				duration: 200,
				delay: 100,
				properties: {
					opacity: 0
				}
			}).play();
		});
	});


	dojo.connect(akalepse.dropZone, "onDrop", function() {
		var buttons = dojo.byId('SaveChanges');
		dojo.animateProperty({
			node: buttons,
			duration: 300,
			properties: {
				opacity: 1
			}
		}).play();
	});
	dojo.connect(dojo.byId('SaveChangesBtn'), "onclick", function() {
		dojo.forEach(akalepse.dropZone.getAllNodes(), function(item, index){
			var fileUrl = item.firstChild.src;
			var filename = fileUrl.substring(fileUrl.lastIndexOf('/')+1);
			var image = akalepse.collection.get(filename);
			image.order = index;
			akalepse.collection.put(image);
			dojo.byId('theForm').elements['theShit'].value = JSON.stringify(image);
			var xhrArgs = {
				url: "/admin/home/save/",
				form: theForm,
				handleAs: "text",
				load: function(response, ioArgs) {
					if(response != 'Homepage updated.') {
						image.databaseID = response;
						akalepse.collection.put(image);
					}
				},
				error: function(error) {
					//
				}
			};
			dojo.xhrPost(xhrArgs);
		});
// 		dojo.byId('theForm').elements['theShit'].value = JSON.stringify(akalepse.collection.data);
// 		var xhrArgs = {
// 			url: "/admin/home/save/",
// 			form: theForm,
// 			handleAs: "text",
// 			load: function(response, ioArgs) {
// 				//
// 			},
// 			error: function(error) {
// 				//
// 			}
// 		};
// 		dojo.xhrPost(xhrArgs);

		dojo.animateProperty({
			node: dojo.byId('SaveChanges'),
			duration: 300,
			properties: {
				opacity: 0
			}
		}).play();
	});
	dojo.connect(dojo.byId('CancelChangesBtn'), "onclick", function() {
		// Pop Confirm modal dialog to continue
		dojo.animateProperty({
			node: dojo.byId('SaveChanges'),
			duration: 300,
			properties: {
				opacity: 0
			}
		}).play();
	});
}