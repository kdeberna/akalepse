dojo.require("dojo.dnd.Source");
dojo.require('dijit.Dialog');
dojo.require("dojo.store.Memory");
dojo.require("dojo.store.Observable");

var akalepse = akalepse || {};

akalepse.collection = dojo.store.Observable(new dojo.store.Memory({idProperty: 'filename'}));
akalepse.dropZone = '';

akalepse.image = {

	connectElement: function(element) {
		var actions = element.childNodes[1];
		
		dojo.connect(actions, 'onmouseenter', function() {
			var fadeArgs = {
				node: actions,
				duration: 250
			};
			dojo.fadeIn(fadeArgs).play();
		});
		dojo.connect(actions, 'onmouseleave', function() {
			var fadeArgs = {
				node: actions,
				duration: 250
			};
			dojo.fadeOut(fadeArgs).play();
		});

		dojo.connect(element, 'onclick', function(event) {
			var imageName = this.firstChild.src;
			imageName = imageName.substring(imageName.lastIndexOf('/')+1);
			var thisImage = akalepse.collection.get(imageName);
			
			switch(event.target.id) {
				case 'MakeCover':
					akalepse.collection.query().forEach(function(image){
						if(image.cover == 1) {
							image.cover = 0;
							dojo.style(image.element, 'background-color', 'rgba(0,0,0,0.8)');
						}
					});
					thisImage.cover = 1;
					dojo.style(thisImage.element, 'background-color', 'rgba(175,175,175,0.7)');
					akalepse.collection.put(thisImage);
					break;
				case 'EditMeta':
					dojo.byId('CCImage').src = thisImage.source;
					dojo.byId('CaptionInput').value = thisImage.caption;
					dojo.byId('CreditInput').value = thisImage.credit;
					dijit.byId('CCDialog').show();
					dijit.byId('CCDialog').set('title', 'Edit ' + thisImage.title);
					break;
				case 'Delete':
					akalepse.collection.remove(imageName);
					break;
			}
//			console.log(JSON.stringify(akalepse.collection.data));
		});
	},

	createElement: function() {
		var div = document.createElement('div');
		div.setAttribute('class', 'image dojoDndItem');
		dojo.style(div, 'opacity', '0');

		var actions = document.createElement('div');
		actions.setAttribute('class', 'imageActions');

		var makeCover = document.createElement('div');
		makeCover.setAttribute('class', 'actionOption');
		makeCover.innerHTML = 'Set as Cover';
		makeCover.id = 'MakeCover';

		var editMeta = document.createElement('div');
		editMeta.setAttribute('class', 'actionOption');
		editMeta.innerHTML = 'Edit MetaData';
		editMeta.id = 'EditMeta';

		var remove = document.createElement('div');
		remove.setAttribute('class', 'actionOption');
		remove.innerHTML = 'Delete Image';
		remove.id = 'Delete';

		var image = document.createElement('img');
		image.src = '/images/backgrounds/loading.gif';

		div.appendChild(image);
		actions.appendChild(makeCover);
		actions.appendChild(editMeta);
		actions.appendChild(remove);
		div.appendChild(actions);

		var arr = [];
		arr.push(div);
		akalepse.dropZone.insertNodes(false, arr, false);

		var fadeArgs = {
			node: div,
			duration: 1000
		};
		dojo.fadeIn(fadeArgs).play();
		
		return div;
	}
};

// file drag hover
function FileDragHover(e) {
	e.stopPropagation();
	e.preventDefault();
	if(e.target.id == 'UploadZone'){
		e.target.className = (e.type == "dragover" ? "hover" : "");
	}
}

// file selection
function FileSelectHandler(e) {
	// cancel event and hover styling
	FileDragHover(e);
	// fetch FileList object
	var files = e.target.files || e.dataTransfer.files;
	// process all File objects
	for (var i = 0, f; f = files[i]; i++) {
		UploadFile(f);
	}
}

function UploadFile(file) {
	var xhr = new XMLHttpRequest();
	//if(xhr.upload && file.type == "image/jpeg" && file.size <= dojo.byId("MAX_FILE_SIZE").value) {
	if(document.getElementById('UploadZone').innerHTML == '<p class="dragTitle">Drag and drop images to upload</p>') {
		document.getElementById('UploadZone').innerHTML = '';
	}
	var response = akalepse.image.createElement();

	xhr.open("POST", '/admin/galleries/upload/', true);  
	xhr.setRequestHeader("X_FILENAME", file.name);
	xhr.onreadystatechange = function() {
		if(xhr.readyState == 4 && xhr.status == 200) {
			akalepse.collection.add({
				id: file.name,
				filename: file.name,
				caption: '',
				credit: '',
				title: file.name,
				source: '/images/tmp/' + xhr.responseText,
				cover: 0,
				order: '',
				element: response
			});
			console.log(response);
			dojo.style(response.firstChild, 'opacity', '0');
			response.firstChild.src = '/images/tmp/' + xhr.responseText;
			var args = {
				node: response.firstChild,
				duration: 500
			};
			dojo.fadeIn(args).play();
			akalepse.image.connectElement(response);
// 				// Add hidden field to get hold the filename to pass on to the database
// 				var filename = document.createElement("input");
// 				filename.setAttribute("type", "hidden");
// 				filename.setAttribute("name", "filename[]");
// 				filename.setAttribute("value", xhr.responseText);
// 				document.getElementById("NewsForm").appendChild(filename);
 			} else {
 				// error message for bad upload
 			}
		}
		xhr.send(file);  
	//}
}

dojo.addOnLoad(function() {
	var filedrag = dojo.byId("UploadZone");
	filedrag.innerHTML = '<p class="dragTitle">Drag and drop images to upload</p>';	

	akalepse.dropZone = new dojo.dnd.AutoSource('UploadZone');

	var results = akalepse.collection.query();
	var observeHandle = results.observe(function(object, removedFrom, insertedInto){
		if(removedFrom > -1){ // existing object removed
			var args = {
				node: object.element,
				duration: 500,
				onEnd: function() {
				dojo.animateProperty({
					node: object.element,
					properties: {
						width: 0,
						margin: 0,
						padding: 0,
						border: 'none'
					},
					onEnd: function() {
						dojo.byId('UploadZone').removeChild(object.element)
					}
					}).play();
				}
			};
			dojo.fadeOut(args).play();
		}
		if(insertedInto > -1){ // new or updated object inserted
			// Do nothing
		}
	});

	dojo.connect(dojo.byId('NewsForm'), 'onsubmit', function(event) {
//		dojo.stopEvent(event);
		dojo.forEach(akalepse.dropZone.getAllNodes(), function(item, index) {
			var fileUrl = item.firstChild.src;
			var filename = fileUrl.substring(fileUrl.lastIndexOf('/')+1);
			var image = akalepse.collection.get(filename);
			image.order = index;
			// Clean up some fo the json of unecessary objects
			delete image.element;
			delete image.id;
			akalepse.collection.put(image);
		});
		var coverSet = akalepse.collection.query({cover: 1});
		if(coverSet == '') {
			var first = akalepse.collection.query({order: 0});
			first[0].cover = 1;
			akalepse.collection.put(first[0]);
		}
		dojo.byId('NewsForm').elements['images'].value = JSON.stringify(akalepse.collection.data);
//		images.setAttribute("value", JSON.stringify(akalepse.collection.data));
//		console.log(JSON.stringify(akalepse.collection.data));
	});

		var images = document.createElement("input");
		images.setAttribute("type", "hidden");
		images.setAttribute("name", "images");
		dojo.byId("NewsForm").appendChild(images);

	var myDialog = new dijit.Dialog({
		id: 'CCDialog',
		title: "Set the Caption and Credit",
		style: "width: 600px; height: 200px;",
		draggable: false
	}, dojo.byId('CreditCaption'));
	myDialog.hide();

	dojo.connect(dojo.byId('CCSave'), 'onclick', function(event) {
		var imageName = event.target.parentNode.parentNode.childNodes[1].childNodes[1].src;
		imageName = imageName.substring(imageName.lastIndexOf('/')+1);
		var thisImage = akalepse.collection.get(imageName);
		thisImage.caption = dojo.byId('CaptionInput').value;
		thisImage.credit = dojo.byId('CreditInput').value;
		akalepse.collection.put(thisImage);
		dijit.byId('CCDialog').hide();
	});

	// is XHR2 available?
	var xhr = new XMLHttpRequest();
	if(xhr.upload) {
		// file drop
		filedrag.addEventListener("dragover", FileDragHover, false);
		filedrag.addEventListener("dragleave", FileDragHover, false);
		filedrag.addEventListener("drop", FileSelectHandler, false);
	}
});