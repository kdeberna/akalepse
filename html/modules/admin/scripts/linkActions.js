dojo.require("dijit.Dialog");
dojo.require("dijit.Editor");
dojo.require("dijit._editor.plugins.LinkDialog");
dojo.require("dojox.widget.Dialog");
dojo.require("dojo.fx");
dojo.require("dojo.fx.easing");
dojo.require("dojo.dnd.Source");

dojo.addOnLoad(function() {
	dojo.query("#LinkItems button").forEach(function(item) {
		dojo.connect(item, 'onclick', function(e){
			dojo.stopEvent(e);
			// What are we doing, adding, editing, etc?
			// Reads from the button value in the HTML
			var action = e.target.value;
			// The path to the action and ID we want to work with
			var path = e.target.parentNode.pathname;
			startDialog(action, path);
		});
	});
	dojo.query("#ToolBar button").forEach(function(item) {
		dojo.connect(item, 'onclick', function(e){
			dojo.stopEvent(e);
			// What are we doing, adding, editing, etc?
			// Reads from the button value in the HTML
			var action = e.target.value;
			// The path to the action and ID we want to work with
			var path = e.target.parentNode.pathname;
			startDialog(action, path);
		});
	});
	var slides = new dojo.dnd.AutoSource('LinkItems');
	dojo.connect(slides, "onDrop", function() {
		var buttons = dojo.byId('SaveChanges');
		dojo.animateProperty({
			node: buttons,
			duration: 300,
			properties: {
				opacity: 1
			}
		}).play();
	});
	dojo.connect(dojo.byId('SaveChangesBtn'), "onclick", function() {
		slides.getAllNodes().forEach(function(item, index) {
			var xhrArgs = {
				url: '/admin/links/reorder/' + item.id + '/' + index,
				handleAs: "text"
			}
			var deferred = dojo.xhrPost(xhrArgs);
		});
		dojo.animateProperty({
			node: dojo.byId('SaveChanges'),
			duration: 300,
			properties: {
				opacity: 0
			}
		}).play();
	});
	dojo.connect(dojo.byId('CancelChangesBtn'), "onclick", function() {
		// Pop Confirm modal dialog to continue
		dojo.animateProperty({
			node: dojo.byId('SaveChanges'),
			duration: 300,
			properties: {
				opacity: 0
			}
		}).play();
	});
});

function startDialog(action, path) {
	var responseDialog = new dojox.widget.Dialog({
		id: 'LinkDialog',
		dimensions: [475,300],
		href: path,
		draggable: false,
		preload: true,
		parseOnLoad: true,
		executeScripts: true,
		autofocus: false,
		onCancel: destroyDialog,
		onLoad: function(data) {
			dojo.query(".cancel").forEach(function(item) {
				dojo.connect(item, 'onclick', destroyDialog);
			});
			dojo.query(".action").forEach(function(item) {
				dojo.connect(item, 'onclick', doAction);
			});
			if(action == 'add') {
				dojo.style(dojo.byId('ConfirmAddEvent'), 'display', 'inline');			
			}
			if(action == 'edit') {
				dojo.style(dojo.byId('ComfirmSaveEvent'), 'display', 'inline');			
				var img = dojo.doc.createElement('img');
				dojo.attr(img, {
					src: "/images/links/thumbs/" + dojo.byId('LinkImageInput').value,
					alt: "Link image."
				});
                dojo.place(img, dojo.byId('LinkImage'), "first");
			}
		}
	}, 'PopUp');
	dojo.style(responseDialog, 'backgroundColor', '#ffffff');
	switch(action) {
		case 'add':
			responseDialog.setAttribute('title', 'Add New Link');
			break;
		case 'edit':
			responseDialog.setAttribute('title', 'Edit Link');
			break;
		case 'delete':
			responseDialog.setAttribute('title', 'Delete Link');
			break;
	}
	responseDialog.theAction = action;
	responseDialog.startup();
	responseDialog.show();
}

function doAction(evt) {
	dojo.stopEvent(evt);
	var theDialog = dijit.byId('LinkDialog');
	var xhrPath = this.parentNode.pathname;
	var theForm = dojo.byId('LinkForm');
	var xhrArgs = {
		url: xhrPath,
		form: theForm,
		handleAs: "text",
		load: function(ioArgs, data) {
			switch(theDialog.theAction) {
				case 'add':
					dojo.place(data.xhr.responseText, dojo.byId('LinkItems'), 'last');
					dojo.query('#LinkItems div:last-child div.actions button').forEach(function(item) {
						dojo.style(item, 'marginLeft', '4px');
						dojo.connect(item, 'onclick', function(e){
							dojo.stopEvent(e);
							// What are we doing, adding, editing, etc?
							// Reads from the button value in the HTML
							var action = e.target.value;
							// The path to the action and ID we want to work with
							var path = e.target.parentNode.pathname;
							startDialog(action, path);
						});
					});
					// need to connect edit and delete buttons
					break;
				case 'edit':
					var newImageFilename = theForm.elements[3].value;
					var newTitle = theForm.elements[0].value;
					var newUrl = theForm.elements[1].value;
					var newDescription = theForm.elements[2].value;
					
					dojo.query('p', dojo.byId(data.xhr.responseText)).forEach(function(item) {
						item.innerHTML = newDescription;
					});
					dojo.query('h3 a', dojo.byId(data.xhr.responseText)).forEach(function(item) {
						item.innerHTML = newTitle;
						item.href = 'http://' + newUrl;
					});
					dojo.query('.linkImage a', dojo.byId(data.xhr.responseText)).forEach(function(item) {
						item.href = 'http://' + newUrl;
					});
					dojo.query('.linkImage img', dojo.byId(data.xhr.responseText)).forEach(function(item) {
						item.src = '/images/links/thumbs/' + newImageFilename;
					});
					dojo.animateProperty({
						node: data.xhr.responseText,
						easing: dojo.fx.easing['expoIn'],
						duration: 2500,
						properties: {
							backgroundColor: { end: '#f2f2f2', start: 'yellow' }
					  }
					}).play();
					break;
				case 'delete':
					var faded = dojo.fadeOut({
						node: evt.target.id
					});
					var reduceHeight = dojo.animateProperty({
						node: evt.target.id,
						duration: 100,
						properties: {
							height: 0
						},
						onEnd: function() {
							dojo.destroy(evt.target.id);
						}
					});
					dojo.fx.chain([faded, reduceHeight]).play();
					break;
			}
		},
		error: function(error) {
			alert(error);
		}
	}
	var deferred = dojo.xhrPost(xhrArgs);
	setTimeout(destroyDialog, 500);
}

function destroyDialog(event) {
	if(event) {
		dojo.stopEvent(event);
	}
	var dialog = dijit.byId("LinkDialog");
	dialog.hide();
	setTimeout(dojo.hitch(dialog, 'destroyRecursive'), dijit.defaultDuration);
}