dojo.require("dojo.dnd.Source");
dojo.require('dijit.Dialog');
dojo.require("dojo.store.Memory");
dojo.require("dojo.store.Observable");

var akalepse = akalepse || {};

akalepse.collection = dojo.store.Observable(new dojo.store.Memory({idProperty: 'filename'}));
akalepse.dropZone = '';

akalepse.image = {

	connectElement: function(element) {
		var actions = element.childNodes[1];
		
		dojo.connect(actions, 'onmouseenter', function() {
			var fadeArgs = {
				node: actions,
				duration: 250
			};
			dojo.fadeIn(fadeArgs).play();
		});
		dojo.connect(actions, 'onmouseleave', function() {
			var fadeArgs = {
				node: actions,
				duration: 250
			};
			dojo.fadeOut(fadeArgs).play();
		});

		dojo.connect(element, 'onclick', function(event) {
			var imageName = this.firstChild.src;
			imageName = imageName.substring(imageName.lastIndexOf('/')+1);
			var thisImage = akalepse.collection.get(imageName);
			
			switch(event.target.id) {
				case 'MakeCover':
					akalepse.collection.query().forEach(function(image){
						if(image.cover == 1) {
							image.cover = 0;
							dojo.style(image.element, 'background-color', 'rgba(0,0,0,0.8)');
						}
					});
					thisImage.cover = 1;
					dojo.style(thisImage.element, 'background-color', 'rgba(175,175,175,0.7)');
					akalepse.collection.put(thisImage);
					break;
				case 'EditMeta':
					dojo.byId('CCImage').src = thisImage.source;
					dojo.byId('CaptionInput').value = thisImage.caption;
					dojo.byId('CreditInput').value = thisImage.credit;
					dijit.byId('CCDialog').show();
					dijit.byId('CCDialog').set('title', 'Edit ' + thisImage.title);
					break;
				case 'Delete':
					var xhrArgs = {
						url: "/admin/galleries/clearimage?filename=" + thisImage.filename,
						handleAs: "text",
						//postData: '?filename=' + thisImage.filename,
						load: function(data){
							akalepse.collection.remove(imageName);
						},
						error: function(error){
							alert(error);
						}
					}
					var deferred = dojo.xhrPost(xhrArgs);
					break;
			}
		});
	},

	createElement: function(source) {
		var div = document.createElement('div');
		div.setAttribute('class', 'image dojoDndItem');
		dojo.style(div, 'opacity', '0');

		var actions = document.createElement('div');
		actions.setAttribute('class', 'imageActions');

		var makeCover = document.createElement('div');
		makeCover.setAttribute('class', 'actionOption');
		makeCover.innerHTML = 'Set as Cover';
		makeCover.id = 'MakeCover';

		var editMeta = document.createElement('div');
		editMeta.setAttribute('class', 'actionOption');
		editMeta.innerHTML = 'Edit MetaData';
		editMeta.id = 'EditMeta';

		var remove = document.createElement('div');
		remove.setAttribute('class', 'actionOption');
		remove.innerHTML = 'Delete Image';
		remove.id = 'Delete';

		var image = document.createElement('img');
		image.src = source;

		div.appendChild(image);
		actions.appendChild(makeCover);
		actions.appendChild(editMeta);
		actions.appendChild(remove);
		div.appendChild(actions);

		var arr = [];
		arr.push(div);
		akalepse.dropZone.insertNodes(false, arr, false);

		var fadeArgs = {
			node: div,
			duration: 1000
		};
		dojo.fadeIn(fadeArgs).play();
		
		return div;
	}
};

// file drag hover
function FileDragHover(e) {
	e.stopPropagation();
	e.preventDefault();
	if(e.target.id == 'UploadZone'){
		e.target.className = (e.type == "dragover" ? "hover" : "");
	}
}

// file selection
function FileSelectHandler(e) {
	// cancel event and hover styling
	FileDragHover(e);
	// fetch FileList object
	var files = e.target.files || e.dataTransfer.files;
	// process all File objects
	for (var i = 0, f; f = files[i]; i++) {
		UploadFile(f);
	}
}

function UploadFile(file) {
	var xhr = new XMLHttpRequest();
	//if(xhr.upload && file.type == "image/jpeg" && file.size <= dojo.byId("MAX_FILE_SIZE").value) {
	var response = akalepse.image.createElement('/images/backgrounds/loading.gif');

	xhr.open("POST", '/admin/galleries/upload/', true);  
	xhr.setRequestHeader("X_FILENAME", file.name);
	xhr.onreadystatechange = function() {
		if(xhr.readyState == 4 && xhr.status == 200) {
			akalepse.collection.add({
				id: file.name,
				filename: file.name,
				caption: '',
				credit: '',
				title: file.name,
				source: '/images/tmp/' + xhr.responseText,
				cover: 0,
				order: '',
				element: response,
				added: 1
			});
			dojo.style(response.firstChild, 'opacity', '0');
			response.firstChild.src = '/images/tmp/' + xhr.responseText;
			var args = {
				node: response.firstChild,
				duration: 500
			};
			dojo.fadeIn(args).play();
			akalepse.image.connectElement(response);
 			} else {
 				// error message for bad upload
 			}
		}
		xhr.send(file);  
	//}
}

dojo.addOnLoad(function() {
	var filedrag = dojo.byId("UploadZone");
	var location = window.location.pathname;
	location = location.slice(0, -1)
	var dir = location.substring(location.lastIndexOf('/')+1);
	
	var xhrArgs = {
      url: "/admin/galleries/images/" + dir + "/",
      handleAs: "json",
      load: function(data){
		dojo.forEach(data, function(item, index){
			var response = akalepse.image.createElement('/images/galleries/' + dir + '/' + item._filename);
			akalepse.collection.add({
				id: item._filename,
				imageID: item._id,
				filename: item._filename,
				caption: item._caption,
				credit: item._credit,
				title: item._filename,
				source: '/images/galleries/' + dir + '/' + item._filename,
				cover: item._cover,
				order: item._order,
				element: response,
				added: 0
			});
			akalepse.image.connectElement(response);
			if(item._cover == 1) {
				dojo.style(response, 'background-color', 'rgba(175,175,175,0.7)');
			}
		});
      },
      error: function(error){
        // We'll 404 in the demo, but that's okay.  We don't have a 'postIt' service on the
        // docs server.
        dojo.byId("UploadZone").innerHTML = "Error retreiving images.";
      }
    }
    dojo.byId("UploadZone").innerHTML = '';
    var deferred = dojo.xhrPost(xhrArgs);

	akalepse.dropZone = new dojo.dnd.AutoSource('UploadZone');

	var results = akalepse.collection.query();
	var observeHandle = results.observe(function(object, removedFrom, insertedInto){
		if(removedFrom > -1){ // existing object removed
			var args = {
				node: object.element,
				duration: 500,
				onEnd: function() {
				dojo.animateProperty({
					node: object.element,
					properties: {
						width: 0,
						margin: 0,
						padding: 0,
						border: 'none'
					},
					onEnd: function() {
						dojo.byId('UploadZone').removeChild(object.element)
					}
					}).play();
				}
			};
			dojo.fadeOut(args).play();
		}
		if(insertedInto > -1){ // new or updated object inserted
			// Do nothing
		}
	});

	dojo.connect(dojo.byId('NewsForm'), 'onsubmit', function(event) {
//		dojo.stopEvent(event);
		dojo.forEach(akalepse.dropZone.getAllNodes(), function(item, index){
			var fileUrl = item.firstChild.src;
			var filename = fileUrl.substring(fileUrl.lastIndexOf('/')+1);
			var image = akalepse.collection.get(filename);
			image.order = index;
			// Clean up some fo the json of unecessary objects
			delete image.element;
			delete image.id;
			akalepse.collection.put(image);
		});
		var coverSet = akalepse.collection.query({cover: 1});
		if(coverSet == '') {
			var first = akalepse.collection.query({order: 0});
			first[0].cover = 1;
			akalepse.collection.put(first[0]);
		}
		dojo.byId('NewsForm').elements['images'].value = JSON.stringify(akalepse.collection.data);
	});

	var images = document.createElement("input");
	images.setAttribute("type", "hidden");
	images.setAttribute("name", "images");
	dojo.byId("NewsForm").appendChild(images);

	var myDialog = new dijit.Dialog({
		id: 'CCDialog',
		title: "Set the Caption and Credit",
		style: "width: 600px; height: 200px;",
		draggable: false
	}, dojo.byId('CreditCaption'));
	myDialog.hide();

	dojo.connect(dojo.byId('CCSave'), 'onclick', function(event) {
		var imageName = event.target.parentNode.parentNode.childNodes[1].childNodes[1].src;
		imageName = imageName.substring(imageName.lastIndexOf('/')+1);
		var thisImage = akalepse.collection.get(imageName);
		thisImage.caption = dojo.byId('CaptionInput').value;
		thisImage.credit = dojo.byId('CreditInput').value;
		akalepse.collection.put(thisImage);
		dijit.byId('CCDialog').hide();
	});

	// is XHR2 available?
	var xhr = new XMLHttpRequest();
	if(xhr.upload) {
		// file drop
		filedrag.addEventListener("dragover", FileDragHover, false);
		filedrag.addEventListener("dragleave", FileDragHover, false);
		filedrag.addEventListener("drop", FileSelectHandler, false);
	}
});