/*
filedrag.js - HTML5 File Drag & Drop demonstration
Featured on SitePoint.com
Developed by Craig Buckler (@craigbuckler) of OptimalWorks.net
*/
(function() {

	// getElementById
	function $id(id) {
		return document.getElementById(id);
	}


	// output information
	function Output(msg) {
		var m = $id("messages");
		m.innerHTML = msg + m.innerHTML;
	}


	// file drag hover
	function FileDragHover(e) {
		e.stopPropagation();
		e.preventDefault();
		e.target.className = (e.type == "dragover" ? "hover" : "");
	}


	// file selection
	function FileSelectHandler(e) {

		// cancel event and hover styling
		FileDragHover(e);

		// fetch FileList object
		var files = e.target.files || e.dataTransfer.files;

		// process all File objects
		for (var i = 0, f; f = files[i]; i++) {
			UploadFile(f);
		}

	}

	function UploadFile(file) {
		var xhr = new XMLHttpRequest();
		//if(xhr.upload && file.type == "image/jpeg" && file.size <= $id("MAX_FILE_SIZE").value) {
			// start upload  
			xhr.open("POST", $id("upload").action, true);  
			xhr.setRequestHeader("X_FILENAME", file.name);
			xhr.onreadystatechange = function() {//Call a function when the state changes.
				if(xhr.readyState == 4 && xhr.status == 200) {
					document.getElementById('LinkForm').elements["image_url"].value = xhr.responseText;
					document.getElementById('LinkImage').innerHTML = '<img src="/images/links/thumbs/' + xhr.responseText + '" width="75" height="75" border="0" />';
				}
			}
			xhr.send(file);  
		//}
	}

	var filedrag = $id("LinkImage");

	// is XHR2 available?
	var xhr = new XMLHttpRequest();
	if(xhr.upload) {
		// file drop
		filedrag.addEventListener("dragover", FileDragHover, false);
		filedrag.addEventListener("dragleave", FileDragHover, false);
		filedrag.addEventListener("drop", FileSelectHandler, false);
	}

})();