require([
	"dojo/dom",
	"dojo/_base/connect",
	"dojo/_base/xhr",
	"dojo/domReady!",
	"dijit/Dialog"], function(dom, connect, xhr2, ready, Dialog) {

	var myDialog = new Dialog({
		title: "Delete Entry",
		id: 'ModalDialog',
		draggable: false,
		autofocus: false,
		style: "width: 350px;",
		onLoad: function() {
			var cancel = dojo.byId('CancelButton');
			dojo.connect(cancel, 'onclick', function(e) {
					dojo.stopEvent(e);
					myDialog.hide();
			});
		}
	});

	dojo.query('.discog_item').forEach(function(node, index, err) {
		connect.connect(node, 'onmouseenter', function() {
			var fadeArgs = {
				node: node.childNodes[1].childNodes[0],
				duration: 100
			};
			dojo.fadeIn(fadeArgs).play();
		});
		connect.connect(node, 'onmouseleave', function() {
			var fadeArgs = {
				node: node.childNodes[1].childNodes[0],
				duration: 100
			};
			dojo.fadeOut(fadeArgs).play();
		});
		connect.connect(node.childNodes[1], 'onclick', function(e) {
			dojo.stopEvent(e);
//			var what = e.target.value;
			var path = dojo.attr(this, 'href');
			myDialog.set('href', path);
			myDialog.show();
		});
	});
    
//     connect.connect(file, 'onchange', function() {
// 		var xhr = new XMLHttpRequest();
// 		//if(xhr.upload && file.type == "image/jpeg" && file.size <= dojo.byId("MAX_FILE_SIZE").value) {
// 		xhr.open("POST", '/admin/discography/unzip/', true);  
// 		xhr.setRequestHeader("X_FILENAME", file.files[0].fileName);
// 		xhr.onreadystatechange = function() {
// 			if(xhr.readyState == 4 && xhr.status == 200) {
// 				if(xhr.responseText.substring(0,1) == ':') {
// 					filename.innerHTML = xhr.responseText.substring(1) + '<input type="hidden" name="filename[]" value="' + xhr.responseText.substring(1) + '" />';
// 					filename = null;
// 				} else {
// 					dojo.place(xhr.responseText, tracks, 'last');
// 					dojo.style(file, 'display', 'none');
// 				}
// 			} else {
// 				// error message for bad upload
// 			}
// 		}
// 		xhr.send(file.files[0]);  
//     });
    
});