dojo.require("dojo.dnd.Source");
dojo.require('dijit.Dialog');
dojo.require("dojo.store.Memory");
dojo.require("dojo.store.Observable");

// file drag hover
function FileDragHover(e) {
	e.stopPropagation();
	e.preventDefault();
	if(e.target.id == 'UploadZoneFront'){
		e.target.className = (e.type == "dragover" ? "hover" : "");
	}
}

// file selection
function FileSelectHandler(e) {
	// cancel event and hover styling
	FileDragHover(e);
	// fetch FileList object
	var files = e.target.files || e.dataTransfer.files;
	// process all File objects
	for (var i = 0, f; f = files[i]; i++) {
		UploadFile(f);
	}
}

function UploadFile(file) {
	var xhr = new XMLHttpRequest();
	//if(xhr.upload && file.type == "image/jpeg" && file.size <= dojo.byId("MAX_FILE_SIZE").value) {
	//if(document.getElementById('UploadZoneFront').innerHTML == '<p class="dragTitle">Drag and drop</p><p class="dragTitle" style="margin-top: 0;">the Front Image</p>') {
		document.getElementById('UploadZoneFront').innerHTML = '';
	//}
//	var response = akalepse.image.createElement();


		var div = document.createElement('div');
		div.setAttribute('class', 'image');
		dojo.style(div, 'opacity', '0');

		var image = document.createElement('img');
		dojo.style(image, 'max-width', '280px');
		dojo.style(image, 'max-height', '330px');
		image.src = '/images/backgrounds/loading.gif';

		div.appendChild(image);

		dojo.byId("UploadZoneFront").appendChild(div);

		var fadeArgs = {
			node: div,
			duration: 1000
		};
		dojo.fadeIn(fadeArgs).play();



	xhr.open("POST", '/admin/events/upload/', true);  
	xhr.setRequestHeader("X_FILENAME", file.name);
	xhr.onreadystatechange = function() {
		if(xhr.readyState == 4 && xhr.status == 200) {
			var form = document.forms['EventForm'];

			if(document.forms['EventForm'].elements['flyerfront']) {
				document.forms['EventForm'].elements['flyerfront'].value = xhr.responseText;
			} else {
				var input = document.createElement("input");
				input.setAttribute("type", "hidden");
				input.setAttribute("name", "flyerfront");
				input.setAttribute("value", xhr.responseText);
				form.appendChild(input);
			}

			dojo.style(div.firstChild, 'opacity', '0');
			div.firstChild.src = '/images/tmp/' + xhr.responseText;
			var args = {
				node: div.firstChild,
				duration: 500
			};
			dojo.fadeIn(args).play();
//			akalepse.image.connectElement(response);
 			} else {
 				// error message for bad upload
 			}
		}
		xhr.send(file);  
	//}
}

dojo.addOnLoad(function() {
	var filedrag = dojo.byId("UploadZoneFront");
	if(dojo.byId('hiddenFront').value == '') {
		filedrag.innerHTML = '<p class="dragTitle">Drag and drop</p><p class="dragTitle" style="margin-top: 0;">the Front Image</p>';
	} else {
		dojo.query("#flyerfront").orphan();
		var input = document.createElement("input");
		input.setAttribute("type", "hidden");
		input.setAttribute("name", "flyerfront");
		input.setAttribute("value", dojo.byId('hiddenFront').value);
		document.forms['EventForm'].appendChild(input);

		var div = document.createElement('div');
		div.setAttribute('class', 'image');

		var image = document.createElement('img');
		dojo.style(image, 'max-width', '280px');
		dojo.style(image, 'max-height', '330px');
		image.src = '/images/events/' + dojo.byId('hiddenFront').value;

		div.appendChild(image);

		dojo.byId("UploadZoneFront").appendChild(div);
	}

	// is XHR2 available?
	var xhr = new XMLHttpRequest();
	if(xhr.upload) {
		// file drop
		filedrag.addEventListener("dragover", FileDragHover, false);
		filedrag.addEventListener("dragleave", FileDragHover, false);
		filedrag.addEventListener("drop", FileSelectHandler, false);
	}
});