dojo.require("dijit.Dialog");

dojo.addOnLoad(showLoginForm);

function showLoginForm() {
	var loginWindow = new dijit.Dialog({
		id: 'LoginModal',
		title: 'Login',
		href: '/admin/',
		draggable: false,
		autofocus: false,
		style: 'width: 300px; height: 200px;',
		onLoad: function(data) {

		}
	});
	dojo.style(loginWindow.closeButtonNode, 'display', 'none');
	dojo.style(loginWindow.titleBar, 'background', '#ccc');
	dojo.style(loginWindow.containerNode, 'border', '0');
	dojo.style(loginWindow.containerNode, 'padding-top', '7px');
	loginWindow.startup();
	loginWindow.show();
}