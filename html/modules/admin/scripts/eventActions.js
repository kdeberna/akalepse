dojo.require("dijit.Dialog");
dojo.require("dijit.form.DateTextBox");
dojo.require("dijit.form.TimeTextBox");
dojo.require("dijit.Editor");
dojo.require("dijit._editor.plugins.LinkDialog");
dojo.require("dojox.widget.Dialog");
dojo.require("dojox.layout.ContentPane");
dojo.require("dijit.layout.TabContainer");
dojo.require("dijit.form.Button");

dojo.addOnLoad(function() {
	dojo.query("#EventsList button").forEach(function(item) {
		dojo.connect(item, 'onclick', function(e){
			dojo.stopEvent(e);
			// What are we doing, adding, editing, etc?
			// Reads from the button value in the HTML
			var what = e.target.value;
			// The path to the action and ID we want to work with
			var path = e.target.parentNode.pathname;
			startDialog();
		});
	});
	var responseDialog = new dojox.widget.Dialog({
		title: 'Thinger Event',
		id: 'Dialog',
		//href: '/admin/events/save/',
		dimensions: [750,608],
		draggable: false,
		preload: true,
		parseOnLoad: true,
		executeScripts: true,
		autofocus: false,
		onCancel: destroyDialog,
		onLoad: function(data) {
			dojo.query(".cancel").forEach(function(item) {
				dojo.connect(item, 'onclick', destroyDialog);
			});
			dojo.query(".action").forEach(function(item) {
				dojo.connect(item, 'onclick', doAction);
			});
// 			if(what == 'add') {
// 				dojo.style(dojo.byId('ConfirmAddEvent'), 'display', 'inline');			
// 			}
// 			if(what == 'edit') {
				dojo.style(dojo.byId('ComfirmSaveEvent'), 'display', 'inline');			
//			}
			createUploaders();
			addHandlers();
		}
	}, 'Nutz');
	responseDialog.startup();

});

function startDialog() {
	// Build the empty modal dialog. All contents will be handled later at appropriate times
// 	switch(what) {
// 		case 'delete':
// 			var style = 'width: 250px; height: 100px;';
// 			var button = null;
// 			break;
// 		case 'add':
// 			var style = 'width: 700px; height: 558px;';
// 			break;
// 		case 'edit':
// 			var style = 'width: 700px; height: 558px;';
// 			break;
// 		default:
// 			var style = 'width: 700px; height: 558px;';
// 			break;
// 	}
	var responseDialog = dijit.byId('Dialog');
	responseDialog.show();
}

function addHandlers() {
//	var description = new dijit.Editor({ name: 'descriptionn', extraPlugins: ['createLink', 'unlink'] }, dojo.byId('description'));
//	var synopsis = new dijit.Editor({ name: 'synopsis' }, dojo.byId('synopsis'));
	//var calendar = new dijit.form.DateTextBox({	name: "date", constraints: { formatLength: "long" } }, dojo.byId('date'));
	//calendar.setDisplayedValue(dojo.byId('hDate').value);
	//var startWidget = new dijit.form.TimeTextBox({ name: 'start' }, dojo.byId('start'));
	//startWidget.setDisplayedValue(dojo.byId('hStart').value);
	//var endWidget = new dijit.form.TimeTextBox({ name: 'end' }, dojo.byId('end'));
	//endWidget.setDisplayedValue(dojo.byId('hEnd').value);
}

function doAction(evt) {
	dojo.stopEvent(evt);
	var xhrPath = this.parentNode.pathname;
	var theForm = dojo.byId('eventForm');
	var xhrArgs = {
		url: xhrPath,
		form: theForm,
		handleAs: "text",
		load: function(ioArgs, data) {
			dojo.byId('Dialog').innerHTML = data.xhr.responseText;
		},
		error: function(error) {
			alert(error);
		}
	}
	var deferred = dojo.xhrPost(xhrArgs);
	// Remove the event DIV so no refresh is needed
	setTimeout(destroyDialog, 2000);
}

function destroyDialog(event) {
	if(event) {
		dojo.stopEvent(event);
	}
	var dialog = dijit.byId("Dialog");
	dialog.hide();
	//setTimeout(dojo.hitch(dialog, 'destroyRecursive'), dijit.defaultDuration);
}

function createUploaders() {
	var thumbUploader = new qq.FileUploader({
		element: document.getElementById('ThumbnailUpload'),
		action: 'do-nothing.htm',
		debug: true
	});
	var frontUploader = new qq.FileUploader({
		element: document.getElementById('FlyerFrontUpload'),
		action: 'do-nothing.htm',
		debug: true
	});
	var backUploader = new qq.FileUploader({
		element: document.getElementById('FlyerBackUpload'),
		action: 'do-nothing.htm',
		debug: true
	});
}