dojo.require("dojo.dnd.Source");
dojo.require('dijit.Dialog');
dojo.require("dojo.store.Memory");
dojo.require("dojo.store.Observable");


// file drag hover
function FileDragHoverB(e) {
	e.stopPropagation();
	e.preventDefault();
	if(e.target.id == 'UploadZoneBack'){
		e.target.className = (e.type == "dragover" ? "hover" : "");
	}
}

// file selection
function FileSelectHandlerB(e) {
	// cancel event and hover styling
	FileDragHoverB(e);
	// fetch FileList object
	var files = e.target.files || e.dataTransfer.files;
	// process all File objects
	for (var i = 0, f; f = files[i]; i++) {
		UploadFileB(f);
	}
}

function UploadFileB(file) {
	var xhr = new XMLHttpRequest();
	//if(xhr.upload && file.type == "image/jpeg" && file.size <= dojo.byId("MAX_FILE_SIZE").value) {
	//if(document.getElementById('UploadZoneBack').innerHTML == '<p class="dragTitle">Drag and drop</p><p class="dragTitle" style="margin-top: 0;">the Back Image</p>') {
		document.getElementById('UploadZoneBack').innerHTML = '';
	//}
//	var response = akalepse.image.createElement();


		var div = document.createElement('div');
		div.setAttribute('class', 'image');
		dojo.style(div, 'opacity', '0');

		var image = document.createElement('img');
		dojo.style(image, 'max-width', '280px');
		dojo.style(image, 'max-height', '330px');
		image.src = '/images/backgrounds/loading.gif';

		div.appendChild(image);

		dojo.byId("UploadZoneBack").appendChild(div);

		var fadeArgs = {
			node: div,
			duration: 1000
		};
		dojo.fadeIn(fadeArgs).play();



	xhr.open("POST", '/admin/events/upload/', true);  
	xhr.setRequestHeader("X_FILENAME", file.name);
	xhr.onreadystatechange = function() {
		if(xhr.readyState == 4 && xhr.status == 200) {
			var form = document.forms['EventForm'];

			if(document.forms['EventForm'].elements['flyerback']) {
				document.forms['EventForm'].elements['flyerback'].value = xhr.responseText;
			} else {
				var input = document.createElement("input");
				input.setAttribute("type", "hidden");
				input.setAttribute("name", "flyerback");
				input.setAttribute("value", xhr.responseText);
				form.appendChild(input);
			}

			dojo.style(div.firstChild, 'opacity', '0');
			div.firstChild.src = '/images/tmp/' + xhr.responseText;
			var args = {
				node: div.firstChild,
				duration: 500
			};
			dojo.fadeIn(args).play();
//			akalepse.image.connectElement(response);
 			} else {
 				// error message for bad upload
 			}
		}
		xhr.send(file);  
	//}
}

dojo.addOnLoad(function() {
	var filedrag = dojo.byId("UploadZoneBack");
	if(dojo.byId('hiddenBack').value == '') {
		filedrag.innerHTML = '<p class="dragTitle">Drag and drop</p><p class="dragTitle" style="margin-top: 0;">the Back Image</p>';
	} else {
		dojo.query("#flyerback").orphan();
		var input = document.createElement("input");
		input.setAttribute("type", "hidden");
		input.setAttribute("name", "flyerback");
		input.setAttribute("value", dojo.byId('hiddenBack').value);
		document.forms['EventForm'].appendChild(input);

		var div = document.createElement('div');
		div.setAttribute('class', 'image');

		var image = document.createElement('img');
		dojo.style(image, 'max-width', '280px');
		dojo.style(image, 'max-height', '330px');
		image.src = '/images/events/' + dojo.byId('hiddenBack').value;

		div.appendChild(image);

		dojo.byId("UploadZoneBack").appendChild(div);
	}

	// is XHR2 available?
	var xhr = new XMLHttpRequest();
	if(xhr.upload) {
		// file drop
		filedrag.addEventListener("dragover", FileDragHoverB, false);
		filedrag.addEventListener("dragleave", FileDragHoverB, false);
		filedrag.addEventListener("drop", FileSelectHandlerB, false);
	}
});