dojo.addOnLoad(function() {
	dojo.query('div.discog_item div.image').connect('onmouseover', showOverlay);
	dojo.query('div.discog_item div.image').connect('onmouseout', hideOverlay);
});

function showOverlay(event) {
	// alert(this.children[1].children[0].src);
	this.children[1].children[0].style.display = 'inline';
}

function hideOverlay(event) {
	// alert(this.children[1].children[0].src);
	this.children[1].children[0].style.display = 'none';
}