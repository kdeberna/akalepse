dojo.require("dijit.Dialog");
dojo.require("dijit.form.Button");

dojo.addOnLoad(function() {
	var embedButton = dojo.byId("EmbedCode");
	if(embedButton) {
		dojo.connect(embedButton, 'onclick', embedCodeDialog);
	}
});

function embedCodeDialog(evt) {
	dojo.stopEvent(evt);
	// Build the empty modal dialog. All contents will be handled later at appropriate times
	var responseDialog = new dijit.Dialog({
		title: 'Media Player Embed Code',
		id: 'theEmbedDialog',
		content: '<p><b>To embed the Akalepse.com Media Player into your website, please cut and paste the code below:</b><br /><br />&lt;a href="http://akalepse.com"&gt;&lt;img src="http://akalepse.com/images/headers/embedHeader.gif" width="266" height="45" border="0" style="margin-bottom: 0;" /&gt;&lt;/a&gt;&lt;object id="player" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="267" height="435" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0"&gt;&lt;param name="name" value="media_player_cs3" /&gt;&lt;param name="bgcolor" value="#ffffff" /&gt;&lt;param name="align" value="middle" /&gt;&lt;param name="src" value="http://akalepse.com/media_player_cs3.swf?dataUrl=http://akalepse.com/xml/releases.xml" /&gt;&lt;param name="allowfullscreen" value="false" /&gt;&lt;param name="quality" value="high" /&gt;&lt;embed id="player" type="application/x-shockwave-flash" width="267" height="435" src="http://akalepse.com/media_player_cs3.swf?dataUrl=http://akalepse.com/xml/releases.xml" quality="high" allowfullscreen="false" align="middle" bgcolor="#ffffff" name="media_player_cs3"&gt;&lt;/embed&gt;&lt;/object&gt;</p>',
		draggable: false,
		autofocus: false,
		style: "width: 500px; height: 300px; color: #333333; background-color: #ffffff;"
	});
	dojo.style(responseDialog.closeButtonNode, "display", "none");
	handleEmbedCodeDialog();
	responseDialog.show();
}

function handleEmbedCodeDialog(address, status) {
	var closeButton = new dijit.form.Button({label: "Close", id: 'Close'});
	var theWindow = dijit.byId('theEmbedDialog');
	closeButton.attr('style', 'float: right;');
	theWindow.containerNode.appendChild(closeButton.domNode);
	dojo.connect(dojo.byId('Close'), "onclick", function() {
		theWindow.hide();
		theWindow.destroyRecursive();
	});
}