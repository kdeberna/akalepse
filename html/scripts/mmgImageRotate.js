dojo.require("dojo.fx");

dojo.addOnLoad(function() {
	dojo.query('.image').forEach(function(element) {
		dojo.connect(element, 'onclick', showDialog);
	});
	createDialog();
});

var slideshow = '';
var player = '';
var caption = new Array();

function createDialog() {
	var dialogWindow = new dijit.Dialog({
		id: 'Dialog',
		draggable: false,
		autofocus: false,
		onLoad: function(data) {
			dojo.connect(dojo.byId('dijit_DialogUnderlay_0'), 'onclick', destroyDialog);
			dojo.connect(dojo.byId('ShowClose'), 'onclick', destroyDialog);
			dojo.connect(dojo.byId('LeftSideNav'), 'onclick', changeImage);
			dojo.connect(dojo.byId('RightSideNav'), 'onclick', changeImage);
			dojo.connect(dojo.byId('Play'), 'onclick', setSlideshow);
			dojo.query('#CaptionHolder > p').forEach( function(element, iteration) {
				caption[iteration] = element;
			});
			dojo.query('#SlideImageHolder > img').forEach( function(element) {
				// This is the initial element, so hide it until the padding adjustments are made
				dojo.style('SlideImage', 'opacity', '0');
				setImageMargin(element.src);
				dojo.fadeIn({node: 'SlideImage', duration: 250}).play();
			});
			dojo.connect(dojo.byId('LeftSideNav'), 'onmouseenter', function() {
				dojo.fadeIn({node: 'LeftSideNavArrow', duration: 150}).play();
			});
			dojo.connect(dojo.byId('LeftSideNav'), 'onmouseleave', function() {
				dojo.fadeOut({node: 'LeftSideNavArrow', duration: 150}).play();
			});
			dojo.connect(dojo.byId('RightSideNav'), 'onmouseenter', function() {
				dojo.fadeIn({node: 'RightSideNavArrow', duration: 150}).play();
			});
			dojo.connect(dojo.byId('RightSideNav'), 'onmouseleave', function() {
				dojo.fadeOut({node: 'RightSideNavArrow', duration: 150}).play();
			});
		}
	});
	dojo.style(dialogWindow.closeButtonNode, 'display', 'none');
	dojo.style(dialogWindow.titleNode, 'display', 'none');
	dojo.style(dialogWindow.titleBar, 'background', '#fff');
	dojo.style(dialogWindow.titleBar, 'height', '2px');
	dojo.style(dialogWindow.titleBar, 'padding', '0');
	dojo.style(dialogWindow.containerNode, 'border', '0');
	dojo.style(dialogWindow.containerNode, 'padding-top', '7px');
	dialogWindow.startup();
}

function showDialog(event) {
	dojo.stopEvent(event);
	var initialImage = event.target.id.split('/'); // In the form of 'gallery/image'
	var jsonHref = event.target.parentNode.pathname.replace('photos', 'photos/slideshow');
	var dialog = dijit.byId('Dialog');
	dialog.attr('href', jsonHref);
	dialog.attr('style', 'width: 800px; height: 594px;');
	slideshow = getImages();
	dialog.show();
}

function getImages() {
	var slideshowPre = new Array();
	var i = 0;
	dojo.query('.enlargeOverlay').forEach(function(element, iteration) {
		var pair = element.id.split('/');
		var gallery = pair[0];
		var image = pair[1];
		slideshowPre[iteration] = pair[1];
		var preLoad = new Image();
		preLoad.src = '/images/galleries/' + gallery + '/' + image;
	});
	return slideshowPre;
}	

function changeImage(event) {
	// default direction for slideshow
	var direction = 'rightsidenav';
	// If this is a slideshow call then there is no event, if it's a button click then which one?
	if(event) {
		dojo.stopEvent(event);
		direction = event.target.id.toLowerCase();
	}
	var imageCount = slideshow.length;
	var imagePath = dojo.byId("SlideImage").src;
	var imageName = imagePath.substring(imagePath.lastIndexOf('/') + 1);
	var position = dojo.indexOf(slideshow, imageName);
	switch(direction) {
		case 'leftsidenav':
			var animate = false;
			if(position != 0) {
				var previousImage = slideshow[position - 1];
				var newImagePath = imagePath.replace(imageName, previousImage);
				dojo.byId('ImageCount').innerHTML = 'Image ' + position + ' of ' + imageCount;
				dojo.byId('CaptionHolder').innerHTML = '<p>' + caption[position].firstChild.nodeValue + '</p>';
				animate = true;
			}
			break;
		case 'rightsidenav':
			var animate = false;
			if(position != imageCount - 1) {
				var nextImage = slideshow[position + 1];
				var newImagePath = imagePath.replace(imageName, nextImage);
				dojo.byId('ImageCount').innerHTML = 'Image ' + (position + 2) + ' of ' + imageCount;
				dojo.byId('CaptionHolder').innerHTML = '<p>' + caption[position + 2].firstChild.nodeValue + '</p>';
				animate = true;
			}
			if(position == imageCount - 1) {
				// Loop this sucker right here
				position = 0;
				var nextImage = slideshow[position];
				var newImagePath = imagePath.replace(imageName, nextImage);
				dojo.byId('ImageCount').innerHTML = 'Image ' + (position + 1) + ' of ' + imageCount;
				dojo.byId('CaptionHolder').innerHTML = '<p>' + caption[position + 1].firstChild.nodeValue + '</p>';
				animate = true;
				//clearInterval(player);
				//dojo.byId('Pause').src = '/images/buttons/slideshow_play.gif';
			}				
			break;
	}
	if(animate) {
		dojo.fx.chain([
			dojo.fadeOut({node: 'SlideImage', duration: 250, onEnd: function() {
				dojo.byId("SlideImage").src = newImagePath;
				setImageMargin(newImagePath);
			} }),
			dojo.fadeIn({node: 'SlideImage', duration: 250})
		]).play();
	}
}

function setImageMargin(imagePath) {
	var theImage = new Image();
	theImage.src = imagePath;
	theImage.onload = function() {
		var theImageHeight = theImage.height;
		var imageObject = dojo.query('#SlideImageHolder img');
		var holderHeight = dojo.byId('SlideImageHolder').offsetHeight;
		var theMoveDown = (holderHeight - theImageHeight) / 2;
		dojo.style(imageObject[0], 'paddingTop', theMoveDown + 'px');
	}
}

function setSlideshow(event) {
	dojo.stopEvent(event);
	var action = event.target.id.toLowerCase();
	switch(action) {
		case 'play':
			player = setInterval("changeImage()", 4000);
			dojo.byId('Play').src = '/images/buttons/slideshow_pause.gif';
			dojo.attr('Play', 'id', 'Pause');
			dojo.connect(dojo.byId('Pause'), 'onclick', setSlideshow);
			break;
		case 'pause':
			clearInterval(player);
			dojo.byId('Pause').src = '/images/buttons/slideshow_play.gif';
			dojo.attr('Pause', 'id', 'Play');
			dojo.connect(dojo.byId('Play'), 'onclick', setSlideshow);
			break;
	}
}

function loadImage() {

}

function resizeDialog() {

}

function destroyDialog(event) {
	dojo.stopEvent(event);
	clearInterval(player);
	var dialog = dijit.byId("Dialog");
	dialog.hide();
	//setTimeout(dojo.hitch(dialog, 'destroyRecursive'), dijit.defaultDuration);
}