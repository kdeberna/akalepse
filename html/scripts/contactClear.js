dojo.addOnLoad(function() {
	var userEmail = dojo.byId("address");
	dojo.connect(userEmail, 'onfocus', function() {
		if(userEmail.value == 'Enter your email address') {
			userEmail.value = '';
		}
	});
	dojo.connect(userEmail, 'onblur', function() {
		if(userEmail.value == '') {
			userEmail.value = 'Enter your email address';
		}
	});

	var userName = dojo.byId("subject");
	dojo.connect(userName, 'onfocus', function() {
		if(userName.value == 'Enter the email subject') {
			userName.value = '';
		}
	});
	dojo.connect(userName, 'onblur', function() {
		if(userName.value == '') {
			userName.value = 'Enter the email subject';
		}
	});

	var userBody = dojo.byId("body");
	dojo.connect(userBody, 'onfocus', function() {
		if(userBody.value == 'Enter your message here') {
			userBody.value = '';
		}
	});
	dojo.connect(userBody, 'onblur', function() {
		if(userBody.value == '') {
			userBody.value = 'Enter your message here';
		}
	});
});