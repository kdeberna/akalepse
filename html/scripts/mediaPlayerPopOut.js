dojo.addOnLoad(function() {
	dojo.connect(dojo.byId('ExternalPlayer'), 'onclick', openPlayer);
	if(getCookie()) {
		var imageHeight = get_cookie('imageHeight') + 'px';
		var flyerThumb = dojo.byId('HiddenFlyerContainer');
		var mediaPlayer = dojo.byId('MediaPlayer');
		dojo.style(mediaPlayer, 'height', '0');
		dojo.style(flyerThumb, 'height', imageHeight);
		dojo.byId('PlayerOnly').innerHTML = '';
		dojo.style(mediaPlayer, 'marginBottom', '0');
	} else {
	}
	if(window.location.pathname != '/') {
		var imageHeight = get_cookie('imageHeight') + 'px';
		var flyerThumb = dojo.byId('HiddenFlyerContainer');
		var mediaPlayer = dojo.byId('MediaPlayer');
		dojo.style(mediaPlayer, 'height', '0');
		dojo.style(mediaPlayer, 'marginBottom', '0');
		dojo.byId('PlayerOnly').innerHTML = '';
		dojo.style(flyerThumb, 'height', 'auto');
	}
	if(window.location.pathname == '/') {
		dojo.style(dojo.byId('Breadcrumbs'), 'display', 'none');	
	}
});

function openPlayer(evt) {
	dojo.stopEvent(evt);
	playerWindow = window.open('/player/', 'player', 'location=no, menubar=no, status=no, titlebar=no, toolbar=no, width=283, height=451, resizable=no');
	hideElements();
	setCookie();
}

function returnPlayer() {
	if(window.location.pathname == '/') {
		dojo.byId('PlayerOnly').innerHTML = '<object id="player" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="267" height="435" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0"><param name="name" value="media_player_cs3" /><param name="allowScriptAccess" value="always" /><param name="bgcolor" value="#ffffff" /><param name="align" value="middle" /><param name="src" value="/media_player_cs3.swf?dataUrl=/xml/releases.xml" /><param name="allowfullscreen" value="false" /><param name="quality" value="high" /><embed id="player" type="application/x-shockwave-flash" width="267" height="435" src="/media_player_cs3.swf?dataUrl=/xml/releases.xml" quality="high" allowfullscreen="false" align="middle" bgcolor="#ffffff" name="media_player_cs3"></embed></object>';
		dojo.animateProperty({
			node: 'MediaPlayer',
			duration: 1000,
			properties:{
				height: { end: 454, unit: 'px' },
				marginBottom: { end: 20, unit: 'px' }
			}
		}).play();
		dojo.animateProperty({
			node: 'HiddenFlyerContainer',
			duration: 1000,
			properties:{
				height: { end: 0, unit: 'px' }
			}
		}).play();
		dojo.fadeOut({
			node: 'HiddenFlyer',
			duration: 1000
		}).play();
		clearCookie();
	}
}

function hideElements() {
	var flyerThumb = dojo.byId('HiddenFlyer');
	var imageHeight = dojo.byId('HiddenFlyer').height;
	dojo.animateProperty({
		node: 'MediaPlayer',
		duration: 1000,
		properties:{
			height: { end: 0, unit: 'px' }
		},
		onEnd: function() {
			dojo.byId('PlayerOnly').innerHTML = '';
			dojo.style('MediaPlayer', 'marginBottom', 0);
		}
	}).play();
	dojo.animateProperty({
		node: 'HiddenFlyerContainer',
		duration: 1000,
		properties:{
			height: { end: imageHeight, unit: 'px' }
		}
	}).play();
	dojo.fadeIn({
		node: 'HiddenFlyer',
		duration: 1000
	}).play();
	var date = new Date();
	date.setTime(date.getTime()+(1*24*60*60*1000));
	var expires = "; expires="+date.toGMTString();
	document.cookie = "imageHeight=" +  imageHeight + "; expires=" + expires + "; path=/";
}

function setCookie() {
// 	var date = new Date();
// 	date.setTime(date.getTime()+(1*24*60*60*1000));
// 	var expires = "; expires="+date.toGMTString();
// 	document.cookie = "playerState=external; expires=" + expires + "; path=/";
}

function checkCookie() {
	var state = getCookie();
	if(state) {
		return true;
	} else {
		setCookie();
	}
}

function getCookie() {
	if(document.cookie.search('playerState') != -1) {
		return true;
	} else {
		return false;
	}
}

function clearCookie() {
	document.cookie = 'playerState=external; expires=Fri, 27 Jul 2001 02:47:11 UTC; path=/';
	document.cookie = 'imageHeight=; expires=Fri, 27 Jul 2001 02:47:11 UTC; path=/';
}

function get_cookie( cookie_name ) {
  var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );
  if ( results )
    return ( unescape ( results[2] ) );
  else
    return null;
}