dojo.addOnLoad(function() {
	var navTabs = dojo.byId('CalTabs');
	var nextMonth = dojo.byId('NextMonth')
	dojo.connect(navTabs, 'onclick', switchCalendarMonths);
	dojo.connect(nextMonth, 'onclick', switchCalendarMonths);
});

function switchCalendarMonths(event){
	dojo.stopEvent(event);
	var theMonths = new Array();
	var i = 0;
	var currentYear = new Date();
	dojo.query('#CalTabs div').forEach(function(element){
		if(element.id){
			theMonths[i++] = element.id;
		}
	});
	switch(event.target.id) {
		case theMonths[0]:
			dojo.byId('CalTabs').innerHTML = '<div class="tab calTabActive" id="' + theMonths[0] + '">' + theMonths[0] + '</div><a href="' + theMonths[1].toLowerCase() + '/"><div class="tab" id="' + theMonths[1] + '">' + theMonths[1] + '</div></a><a href="' + theMonths[2].toLowerCase() + '/"><div class="tab" id="' + theMonths[2] + '">' + theMonths[2] + '</div></a><span class="theDate">' + currentYear.getFullYear() + '</span><div style="clear: both;"></div>';
			dojo.xhrGet({
				url: '/events/silent/?month=' + monthToNum(theMonths[0]) + '&year=2010',
				load: function(response, ioArgs) {
					dojo.byId('AllEvents').innerHTML = response;
				}
			});
			dojo.byId('NextMonth').innerHTML = '<a id="' + theMonths[1] + '" href="' + theMonths[1].toLowerCase() + '/">&nbsp;&nbsp;Go to ' + theMonths[1] + ' »</a>';
			break;
		case theMonths[1]:
			dojo.byId('CalTabs').innerHTML = '<a href="' + theMonths[0].toLowerCase() + '/"><div class="tab" id="' + theMonths[0] + '">' + theMonths[0] + '</div></a><div class="tab calTabActive" id="' + theMonths[1] + '">' + theMonths[1] + '</div><a href="' + theMonths[2].toLowerCase() + '/"><div class="tab" id="' + theMonths[2] + '">' + theMonths[2] + '</div></a><span class="theDate">' + currentYear.getFullYear() + '</span><div style="clear: both;"></div>';
			dojo.xhrGet({
				url: '/events/silent/?month=' + monthToNum(theMonths[1]) + '&year=2010',
				load: function(response, ioArgs) {
					dojo.byId('AllEvents').innerHTML = response;
				}
			});
			dojo.byId('NextMonth').innerHTML = '<a id="' + theMonths[2] + '" href="' + theMonths[2].toLowerCase() + '/">&nbsp;&nbsp;Go to ' + theMonths[2] + ' »</a>';
			break;		
		case theMonths[2]:
			dojo.byId('CalTabs').innerHTML = '<a href="' + theMonths[0].toLowerCase() + '/"><div class="tab" id="' + theMonths[0] + '">' + theMonths[0] + '</div></a><a href="' + theMonths[1].toLowerCase() + '/"><div class="tab" id="' + theMonths[1] + '">' + theMonths[1] + '</div></a><div class="tab calTabActive" id="' + theMonths[2] + '">' + theMonths[2] + '</div><span class="theDate">' + currentYear.getFullYear() + '</span><div style="clear: both;"></div>';
			dojo.xhrGet({
				url: '/events/silent/?month=' + monthToNum(theMonths[2]) + '&year=2010',
				load: function(response, ioArgs) {
					dojo.byId('AllEvents').innerHTML = response;
				}
			});
			dojo.byId('NextMonth').innerHTML = '&nbsp;&nbsp;';
			break;
	}
}

function monthToNum(month) {
	var changes = new Array();
	changes['January']		= '01';
	changes['February']		= '02';
	changes['March']		= '03';
	changes['April']		= '04';
	changes['May']			= '05';
	changes['June']			= '06';
	changes['July']			= '07';
	changes['August']		= '08';
	changes['September']	= '09';
	changes['October']		= '10';
	changes['November']		= '11';
	changes['December']		= '12';
	return changes[month];
}
	