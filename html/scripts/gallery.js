dojo.require("dijit.Dialog");

dojo.addOnLoad(function() {
	dojo.query('div.thumbnail').connect('onmouseover', showOverlay);
	dojo.query('div.thumbnail').connect('onmouseout', hideOverlay);
	dojo.query('div.image').connect('onmouseover', showOverlay);
	dojo.query('div.image').connect('onmouseout', hideOverlay);
});

function showOverlay(event) {
	// alert(this.children[1].children[0].src);
	this.children[1].children[0].style.display = 'inline';
}

function hideOverlay(event) {
	// alert(this.children[1].children[0].src);
	this.children[1].children[0].style.display = 'none';
}