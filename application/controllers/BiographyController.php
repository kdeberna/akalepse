<?php

class BiographyController extends Zend_Controller_Action {

    public function init() {
		$activeNav = $this->view->navigation()->findByUri('/biography/');
		$activeNav->active = true;
		$activeNav->setClass("active");
		$this->view->headerImage = 'biography';
    }

    public function indexAction() {
        $mapper = new Application_Model_BiographyMapper();
        $result = $mapper->fetchAll();
        $this->view->biography = $result[0];
    }

}