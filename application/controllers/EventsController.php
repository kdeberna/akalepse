<?php

class EventsController extends Zend_Controller_Action {

    public function init() {
		$activeNav = $this->view->navigation()->findByUri('/events/');
		$activeNav->active = true;
		$activeNav->setClass("active");
		$this->view->headerImage = 'upcoming_events';
		$this->view->headTitle()->append('Events');
		$this->view->headLink()->appendStylesheet('/styles/calendar.css');
    }

    public function indexAction() {
		$this->view->headScript()->appendFile('/scripts/calendar.js');
        $events = new Application_Model_EventsMapper();
        $thisMonth = date('n');
        $this->view->eventList = $events->fetchByMonth($thisMonth);
        //$this->view->eventList = $events->fetchMonthDays();
    }
    
    public function singleAction() {
		$activeNav = $this->view->navigation()->findByUri('/events/single/');
		$activeNav->active = true;

    	// Add third level to the window title with the current item
    	$singleTitle = $this->_getParam('id');

     	$this->view->rsvpForm = new App_Forms_RsvpForm();
        if($this->getRequest()->isPost()) {
        	if($this->view->rsvpForm->isValid($this->getRequest()->getParams())) {
        		$mail = new Zend_Mail();
        		$mail->addTo('', 'DJ Akalepse')
        			->setFrom($this->_getParam('email_address'), 'Akalepse Website')
        			->setSubject($this->_getParam('first_name'))
        			->setBodyText($this->_getParam('last_name'))
        			->send();
        	} else {
        		$this->view->errorElements = $this->view->rsvpForm->getMessages();
        	}
        }
		$this->view->id = $this->_getParam('id');

		$event = new Application_Model_Events();
		$mapper = new Application_Model_EventsMapper();
		
		$mapper->find($this->_getParam('id'), $event);
		$this->view->event = $event;

		$activeNav->setLabel(stripslashes($event->title));
		$this->view->headTitle()->append(stripslashes($event->title));
    }
    
    public function silentAction() {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
    	$requestedMonth = $this->_getParam('month');
        $events = new Application_Model_EventsMapper();
        $eventList = $events->fetchByMonth($requestedMonth);
		$this->view->partialLoop()->setObjectKey('event');
		if(count($eventList) != 0) {
			echo $this->view->partialLoop('_partials/_eventListItem.phtml', $eventList);
		} else {
			echo '<div class="noEvents"><img src="/images/headers/eventless.png" style="margin-top: 21px; margin-left: 10px;" with="165" height="30" border="0" alt="No Events" /></div>';
		}
    }
}