<?php

class IndexController extends Zend_Controller_Action {

    public function init() {

    }

    public function indexAction() {
		$this->view->headScript()->appendFile('/scripts/slideshow.js');
//		$this->view->headScript()->appendFile('/scripts/hideHome.js');
		
		$activeNav = $this->view->navigation()->findByUri('/');
		$activeNav->active = true;
		$activeNav->setClass("active");		
		$this->view->headerImage = 'index';

		$registry = Zend_Registry::getInstance();
		$db = $registry['database'];
		$select = $db->select()
						->from('home_images')
						->order('order');
		$thing = $db->query($select);
		$kev = $thing->fetchAll();
		$this->view->images = $kev;
		
        $newsItems = new Application_Model_NewsMapper();
        $this->view->newsItems = $newsItems->fetchVisible();
        
		$commentCount = new Application_Model_CommentsMapper();
        $this->view->commentCount = count($commentCount->fetchAll());
	}
    
	public function sitemapAction() {
		$this->view->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		echo $this->view->navigation()->sitemap();
	}
}