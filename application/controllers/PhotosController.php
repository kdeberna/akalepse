<?php

class PhotosController extends Zend_Controller_Action {

    public function init() {
		$activeNav = $this->view->navigation()->findByUri('/photos/');
		$activeNav->active = true;
		$activeNav->setClass("active");                 
		$this->view->headScript()->appendFile('/scripts/gallery.js');
		$this->view->headLink()->appendStylesheet('/styles/galleries.css');
		$this->view->headTitle()->append('Photo Albums');
    }

    public function indexAction() {
		$this->view->headerImage = 'photo_albums';
		$mapper = new Application_Model_GalleriesMapper();
		$galleries = $this->view->galleries = $mapper->fetchAll();
		foreach($galleries as $gallery){
			// Get images per gallery
			$imageMapper = new Application_Model_ImagesMapper();
			$images = $imageMapper->FetchByAlbumId($gallery->id);
			$fake = str_replace("-", "", $gallery->dir);
			$cover = $fake . '-cover';
			$this->view->{$fake} = count($images);
			foreach($images as $image) {
				if($image->cover == 1) {
					$this->view->{$cover} = $image->filename;
				}
			}
		}
    }

	public function galleryAction() {
		$activeNav = $this->view->navigation()->findByUri('/photos/gallery/');
		$activeNav->active = true;
		$this->view->headerImage = 'photos';
		$galleryDir = $this->_getParam('gallery');
		$this->view->headScript()->appendFile('/scripts/mmgImageRotate.js');

		// Get Gallery ID from the dir name
		// Get all images from galleries_images with album_id matching above

		$mapper = new Application_Model_GalleriesMapper();
		$gallery = $mapper->fetchByDir($galleryDir);

		$imageMapper = new Application_Model_ImagesMapper();
		$this->view->images = $imageMapper->FetchByAlbumId($gallery[0]->id);
		$this->view->dir = $galleryDir;

 		$activeNav->setLabel(stripslashes($gallery[0]->title));
 		$this->view->headTitle()->append(stripslashes($gallery[0]->title));
 		$this->view->gallery = $gallery[0]->title;
	}
	
	public function slideshowAction() {
		$this->_helper->layout()->disableLayout();
		$gallery = $this->view->gallery = $this->_getParam('gallery');
		$image = $this->view->image = (int) $this->_getParam('image');
		$imageNum = (int) $image - 1;

		$theGallery = new Application_Model_GalleriesMapper();
		$dir = $theGallery->fetchByDir($gallery);
		$id = $dir[0]->id;
		
		$theShow = new Application_Model_ImagesMapper();
		$images = $theShow->fetchByAlbumId($id);

		$this->view->totalImages = count($images);

		$this->view->imageSource = '/images/galleries/' . $gallery . '/' . $images[$imageNum]->filename;
		$imageSize = getimagesize('images/galleries/' . $gallery . '/' . $images[$imageNum]->filename);

		$this->view->imageSized = $imageSize[3];
		$this->view->imageWidth = 600; // $size[0];
		$this->view->imageHeight = 400; // $size[1];
		
		if($images[$imageNum]->caption == '') {
			$images[$imageNum]->caption = '&nbsp;';
		}
		$this->view->imageStartCaption = '<p>' . stripslashes($images[$imageNum]->caption) . ' </p>';
		foreach($images as $caption) {
			if($caption->caption == '') {
				$caption->caption = '&nbsp;';
			}
			$this->view->imageCaption .= '<p>' . stripslashes($caption->caption) . ' </p>';
		}
	}

}