<?php

class NewsController extends Zend_Controller_Action {

    public function init() {
		$activeNav = $this->view->navigation()->findByUri('/news/');
		$activeNav->active = true;
		$activeNav->setClass("active");

		$this->view->headerImage = 'news';
		$this->view->headTitle()->append('News');
    }

    public function indexAction() {
        $newsItems = new Application_Model_NewsMapper();
        $this->view->newsItems = $newsItems->fetchVisible();
    }

	public function singleAction() {
 		$activeNav = $this->view->navigation()->findByUri('/news/single/');
 		$activeNav->active = true;

		$this->view->id = $this->_getParam('id');
		$this->view->commentForm = new App_Forms_CommentForm();
		
		$mapper = new Application_Model_NewsMapper();
		$item = new Application_Model_News();
		$mapper->find($this->_getParam('id'), $item);
		
		$commentsMapper = new Application_Model_CommentsMapper();
		$comments = $commentsMapper->fetchById($this->_getParam('id'));
		$this->view->comments = $comments;

		$activeNav->setLabel(stripslashes($item->title));
		$this->view->headTitle()->append(stripslashes($item->title));
		$this->view->entry = $item;
	}

}