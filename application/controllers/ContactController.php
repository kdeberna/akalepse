<?php

class ContactController extends Zend_Controller_Action {

    public function init() {
		$activeNav = $this->view->navigation()->findByUri('/contact/');
		$activeNav->active = true;
		$activeNav->setClass("active");                 
		
		$this->view->headerImage = 'contact';

		$this->view->headTitle()->append('Contact');
		$this->view->headScript()->appendFile('/scripts/dojo/dojo.js');
		$this->view->headScript()->appendFile('/scripts/contactClear.js');
		$this->view->headLink()->appendStylesheet('/styles/contact.css');
    }

    public function indexAction() {
        // action body
        $this->view->emailForm = new App_Forms_ContactForm();
        
        if($this->getRequest()->isPost()) {
        	if($this->view->emailForm->isValid($this->getRequest()->getParams())) {
        		$mail = new Zend_Mail();
        		$mail->addTo('lepse@akalepse.com', 'DJ Akalepse')
        			->setFrom($this->_getParam('address'), 'Akalepse Website')
        			->setSubject($this->_getParam('subject'))
        			->setBodyText($this->_getParam('body'))
        			->send();
        		$this->view->emailSent = 1;
        		$this->view->emailForm->populate(array('address' => 'Enter your email address',
        												'subject' => 'Enter the email subject',
        												'body' => 'Enter your message here'));
        	} else {
        		$this->view->errorElements = $this->view->emailForm->getMessages();
        	}
        }
    }

}