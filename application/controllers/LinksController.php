<?php

class LinksController extends Zend_Controller_Action {

    public function init() {
		$activeNav = $this->view->navigation()->findByUri('/links/');
		$activeNav->active = true;
		$activeNav->setClass("active");
		
		$this->view->headerImage = 'links';

		$this->view->headTitle()->append('Links');
		$this->view->headLink()->appendStylesheet('/styles/links.css');
    }

    public function indexAction() {
		$links = new Application_Model_LinksMapper();
		$this->view->links = $links->fetchAll();
    }

}