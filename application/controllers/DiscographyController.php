<?php

class DiscographyController extends Zend_Controller_Action {

    public function init() {
		$activeNav = $this->view->navigation()->findByUri('/discography/');
		$activeNav->active = true;
		$activeNav->setClass("active");
		
		$this->view->headerImage = 'discography';
		$this->view->headTitle()->append('Discography');
		$this->view->headScript()->appendFile('/scripts/discog.js');
		$this->view->headLink()->appendStylesheet('/styles/discog.css');
    }

    public function indexAction() {
		$mapper = new Application_Model_DiscographyMapper();
		$this->view->discogItems = $mapper->fetchAll();
    }

	public function singleAction() {
		$singleTitle = $this->_getParam('id');
		$activeNav = $this->view->navigation()->findByUri('/discography/single/');
		$activeNav->setLabel($singleTitle);
		$activeNav->active = true;
		// $this->view->headScript()->appendFile('/scripts/imageSwitch.js');
		
		$mapper = new Application_Model_DiscographyMapper();
		$entry = new Application_Model_Discography();
		$result = $mapper->find($singleTitle, $entry);
		$this->view->album = $entry;
		
		$tracksMapper = new Application_Model_TracksMapper();
		$this->view->tracks = $tracksMapper->fetchByAlbum($singleTitle);

		$activeNav->setLabel(stripslashes($entry->title));
		$this->view->headTitle()->append(stripslashes($entry->title));
	}
}