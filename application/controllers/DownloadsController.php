<?php

class DownloadsController extends Zend_Controller_Action
{

    public function init() {
		$activeNav = $this->view->navigation()->findByUri('/downloads/');
		$activeNav->active = true;
		$activeNav->setClass("active");                 
		
		$this->view->headerImage = 'downloads';
    }

    public function indexAction() {
        // action body
    }


}