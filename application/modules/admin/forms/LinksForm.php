<?php

class Admin_Form_LinksForm extends Zend_Dojo_Form {

	public function init() {
		$this->title = new Zend_Form_Element_Text('title');
		$this->title->setLabel('Title');
		
		$this->url = new Zend_Form_Element_Text('url');
		$this->url->setLabel('URL');

		$this->description = new Zend_Form_Element_Textarea('description');
		$this->description->setLabel('Description');

		$this->image_url = new Zend_Form_Element_Hidden('image_url');
		$this->image_url->setAttrib('id', 'LinkImageInput');
		
//		$this->submit = new Zend_Form_Element_Submit('Add');
		
		$this->addElements(array(
			$this->title,
			$this->url,
			$this->description,
			$this->image_url
		));

		$this->setDecorators(array(
			'FormElements',
			array('HtmlTag', array('tag' => 'dl')),
			'Form'
		));
		
		$this->setAttrib('id', 'LinkForm');
		$this->setAction('/')
			->setMethod('post');
	}
}