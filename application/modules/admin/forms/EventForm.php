<?php

class Admin_Form_EventForm extends Zend_Dojo_Form {

	// Make four tabs (subforms?) General, Text, Images, Venue (<-in General?) enough room for adding a venue.

    protected $_statusOptions = array(
        'single'	=> 'One Time',
        'recurring'	=> 'Recurring'
    );

	public function init() {
		$this->setAttribs(array(
			'id'	=> 'EventForm'
		));		
		$this->setMethod('post');
		
        $contentForm = new Zend_Dojo_Form_SubForm();
        $contentForm->setDecorators(array(
            'FormElements',
            array('TabContainer', array(
                'id'	=> 'tabContainer',
                'style' => 'width: 777px; height: 430px;',
                'dijitParams' => array(
                    'tabPosition' => 'top'
                )
            ))
        ));
        $contentForm->setIsArray(false);

        $synopsisForm = new Zend_Dojo_Form_SubForm();
        $synopsisForm->setAttribs(array(
        	'name'			=> 'synopsisForm',
            'legend'		=> 'Synopsis',
			'dijitParams'	=> array(
                'title'	=> 'Synopsis'
			)
		));
		$synopsisForm->setIsArray(false);

        $synopsisForm->addElement(
			'Editor',
			'synopsis',
			array(
				'plugins'		=> array('undo','redo','|','bold','italic','underline','strikethrough','|','createLink','unlink','viewsource'),
				'label'			=> 'Synopsis',
				'value' 		=> ' ',
				'inheritWidth'	=> 'true'
			)
        );
		$synopsisForm->synopsis->setDijitParam('data-dojo-props', "extraPlugins:['prettyprint']");

        $articleForm = new Zend_Dojo_Form_SubForm();
        $articleForm->setAttribs(array(
			'name'   		=> 'articleForm',
			'legend' 		=> 'Article',
			'dijitParams' 	=> array(
				'title' 	=> 'Full Description'
			)
			));
        $articleForm->setIsArray(false);
        
        $articleForm->addElement(
            'Editor',
            'description',
            array(
				'plugins'		=> array('undo','redo','|','bold','italic','underline','strikethrough','|','createLink','unlink','viewsource'),
                'label'			=> 'Description',
                'value' => ' ',
                'inheritWidth'	=> 'true',
            )
        );
		$articleForm->description->setDijitParam('data-dojo-props', "extraPlugins:['prettyprint']");

        $venueForm = new Zend_Dojo_Form_SubForm();
        $venueForm->setAttribs(array(
        	'name'			=> 'venueForm',
            'legend'		=> 'Venue',
			'dijitParams'	=> array(
                'title'	=> 'Venue Information'
			)
        ));
        $venueForm->setIsArray(false);

        $imagesForm = new Zend_Dojo_Form_SubForm();
        $imagesForm->setAttribs(array(
        	'name'			=> 'imagesForm',
            'legend'		=> 'Images',
			'dijitParams'	=> array(
                'title'	=> 'Images'
			)
        ));
        $imagesForm->setIsArray(false);

		$frontImage = new Zend_Form_Element_File('flyerfront');
		$frontImage->setLabel('Flyer (Front)');
		$frontImage->addDecorators(array(array('HtmlTag', array('tag'=>'div', 'id' => 'UploadZoneFront'))));
        $imagesForm->addElement($frontImage);

		$backImage = new Zend_Form_Element_File('flyerback');
		$backImage->setLabel('Flyer (Back)');
		$backImage->addDecorators(array(array('HtmlTag', array('tag'=>'div', 'id' => 'UploadZoneBack'))));
        $imagesForm->addElement($backImage);
        
		$title = new Zend_Form_Element_Text('title');
		$title->setLabel('Event Title')->setRequired(true);
		
		$who = new Zend_Form_Element_Text('who');
		$who->setLabel('Who\'s Playing')->setRequired(true);
		
		$venue = new Zend_Form_Element_Text('venue');
		$venue->setLabel('Venue Name')->setRequired(true);
		
		$address = new Zend_Form_Element_Text('address');
		$address->setLabel('Venue Address')->setRequired(true);
		
		$cover = new Zend_Form_Element_Text('cover');
		$cover->setLabel('Cover Charge (leave empty for no charge)')->setRequired(true);
		
		/**
		 * @todo Add the date and time boxes in the alternate method.
		 */

		$this->addElement(
			'DateTextBox',
			'date',
			array(
				'label'					=> 'Event Date',
				'required'				=> true,
				'invalidMessage'		=> 'Invalid date specified.',
				'formatLength'			=> 'long',
				'selector'				=> 'date'
			)
		);
		
		$this->addElement(
			'TimeTextBox',
			'start',
			array(
				'required'				=> true,
				'label'					=> 'Start Time',
				'visibleIncrement'		=> 'T00:15:00',
				'clickableIncrement'	=> 'T00:15:00'
			)
		);

		$this->addElement(
			'TimeTextBox',
			'end',
			array(
				'required'				=> true,
				'label'					=> 'End Time',
				'visibleIncrement'		=> 'T00:15:00',
				'clickableIncrement'	=> 'T00:15:00'
			)
		);

		$status = new Zend_Form_Element_Select('repeat');
		$status->setRequired(true);
		$status->addMultiOptions($this->_statusOptions);
		
		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setAttrib('id', 'submitbutton');
		$submit->setLabel('Publish');

		$reset = new Zend_Form_Element_Submit('cancel');
		$reset->setAttrib('id', 'cancelbutton');
		$reset->setLabel('Cancel');
		
		$this->addElements(array(
			$title,
			$status,
			$submit,
			$reset
		));
		
		$this->addDisplayGroup(array('title'), 'Title');
		$this->addDisplayGroup(array('date', 'start', 'end'), 'Date');
		$venueForm->addElements(array(
			$who,
			$venue,
			$address,
			$cover
		));
		$contentForm->addSubForm($synopsisForm, 'synopsisForm');
		$contentForm->addSubForm($articleForm, 'articleForm');
		$contentForm->addSubForm($venueForm, 'venueForm');
		$contentForm->addSubForm($imagesForm, 'imagesForm');
		$this->addSubForm($contentForm, 'content');
		$this->addDisplayGroup(array('repeat', 'submit', 'cancel'), 'Tools');
		
	}

}