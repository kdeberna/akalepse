<?php

class Admin_Form_DiscographyForm extends Zend_Dojo_Form {

    protected $_visibilityOptions = array(
        '1'	=> 'Visible',
        '0'	=> 'Hidden'
    );

    protected $_sectionOptions = array(
        'mix'	=> 'Mix',
        'discography'	=> 'Discography'
    );

    protected $checkboxDecorators = array(
		'ViewHelper',
		'Description',
		'Errors',
		array('Label', array('tag' => 'div', 'class' => 'checkboxLabel')),
		array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'checkboxRow')),
    );

    protected $fileDecorators = array(
		'File',
		'Label',
		'Description',
		'Errors',
		array(array('picsDiv' => 'HtmlTag'), array('tag' => 'div', 'id' => 'UploadZone'))
    );

    protected $fileDecorators2 = array(
		'File',
		'Label',
		'Description',
		'Errors',
		array(array('picsDiv' => 'HtmlTag'), array('tag' => 'div', 'id' => 'UploadZone2'))
    );

    protected $fileDecorators3 = array(
		'File',
		'Label',
		'Description',
		'Errors',
		array(array('picsDiv' => 'HtmlTag'), array('tag' => 'div', 'id' => 'UploadZone3'))
    );

    protected $buttonDecorators = array(
		'ViewHelper',
		'Description',
		'Errors'
    );

    protected $tracklistDecorators = array(
		array(array('tracksDiv' => 'HtmlTag'), array('tag' => 'table', 'id' => 'Tracklist')),
		'File',
		'Description',
		'Errors'
    );

	public function init() {
		$this->setAttribs(array(
			'id'		=> 'DiscogForm',
			'enctype'	=> 'multipart/form-data'
		));		
		$this->setMethod('post');

        $contentForm = new Zend_Dojo_Form_SubForm();
        $contentForm->setDecorators(array(
            'FormElements',
            array('TabContainer', array(
                'id' => 'tabContainer',
                'style' => 'width: 777px; height: 500px;',
                'dijitParams' => array(
                    'tabPosition' => 'top'
                )
            ))
        ));
        $contentForm->setIsArray(false);

        $albumForm = new Zend_Dojo_Form_SubForm();
        $albumForm->setAttribs(array(
        	'name'			=> 'albumForm',
            'legend'		=> 'Album',
			'dijitParams'	=> array(
                'title'	=> 'Album'
			)
        ));
        $albumForm->setIsArray(false);

		$title = new Zend_Form_Element_Text('title');
		$title->setLabel('Title')->setRequired(true);
		
        $albumForm->addElement(
            'Editor',
            'description',
			array(
				'plugins'		=> array('undo','redo','|','bold','italic','underline','strikethrough','|','viewsource'),
				'label'			=> 'Description',
                'inheritWidth'	=> 'true',
                'value'			=> ' '
			)
        );
		$albumForm->description->setDijitParam('data-dojo-props', "extraPlugins:['prettyprint']");

		$shortTitle = new Zend_Form_Element_Text('shorttitle');
		$shortTitle->setLabel('Short Title')->setRequired(true);
 		$shortTitle->setAttrib('class', 'shortTitle');
        $albumForm->addElement($shortTitle);

		$shortDesc = new Zend_Form_Element_Text('shortdesc');
		$shortDesc->setLabel('Short Description')->setRequired(true);
 		$shortDesc->setAttrib('class', 'shortDesc');
        $albumForm->addElement($shortDesc);

		$albumForm->addElement(
			'DateTextBox',
			'release_date',
			array(
				'label'					=> 'Release Date',
				'required'				=> true,
				'invalidMessage'		=> 'Invalid date specified.',
				'formatLength'			=> 'long',
				'selector'				=> 'date'
			)
		);
		
		$downloadable = new Zend_Form_Element_Checkbox('downloadable');
		$downloadable->setLabel('Downloadable')->setRequired(true);
		$downloadable->setDecorators($this->checkboxDecorators);
		$downloadable->setCheckedValue('1');
// 		$downloadable->setAttrib('class', 'checkbox');
		$albumForm->addElement($downloadable);

		$playable = new Zend_Form_Element_Checkbox('playable');
		$playable->setLabel('Playable')->setRequired(true);
		$playable->setDecorators($this->checkboxDecorators);
		$playable->setCheckedValue('1');
// 		$playable->setAttrib('class', 'checkbox');
		$albumForm->addElement($playable);

		$section = new Zend_Form_Element_Select('section');
		$section->setRequired(true);
		$section->addMultiOptions($this->_sectionOptions);
        $albumForm->addElement($section);

        $imagesForm = new Zend_Dojo_Form_SubForm();
        $imagesForm->setAttribs(array(
            'name'   => 'imagesForm',
            'legend' => 'Images',
            'dijitParams' => array(
                'title' => 'Images'
            )
        ));
        $imagesForm->setIsArray(false);

		$images = new Zend_Form_Element_File('album_cover');
		$images->setLabel('Cover');
		$images->setDecorators($this->fileDecorators);
        $imagesForm->addElement($images);
        
		$images2 = new Zend_Form_Element_File('album_interior');
		$images2->setLabel('Inside Spread');
		$images2->setDecorators($this->fileDecorators2);
        $imagesForm->addElement($images2);
        
		$images3 = new Zend_Form_Element_File('album_back_cover');
		$images3->setLabel('Back Cover');
		$images3->setDecorators($this->fileDecorators3);
        $imagesForm->addElement($images3);
        
        $tracksForm = new Zend_Dojo_Form_SubForm();
        $tracksForm->setAttribs(array(
            'name'   => 'tracksForm',
            'legend' => 'Tracks',
            'dijitParams' => array(
                'title' => 'Tracks'
            )
        ));
        $tracksForm->setIsArray(false);

		$addNewButton = new Zend_Form_Element_Button('AddNewTrack');
		$addNewButton->setDecorators($this->buttonDecorators);
		$addNewButton->setLabel('Add track');
        $tracksForm->addElement($addNewButton);

		//$progress = new Zend_Form_Element_Hidden('APC_UPLOAD_PROGRESS');
		//$progress->setAttrib('id', 'progress_key');
		//$progress->setValue(md5(uniqid(rand()))); // set to random ID value
		//$tracksForm->addElement($progress);

		$tracks = new Zend_Form_Element_File('tracksZip');
		$tracks->setDecorators($this->tracklistDecorators);
		$tracks->setLabel('Select album zip file.');
		$tracks->setDescription('Upload a .zip file containing all the tracks organized in iTunes.');
		$tracksForm->addElement($tracks);

		$visibility = new Zend_Form_Element_Select('visibility');
		$visibility->setRequired(true);
		$visibility->addMultiOptions($this->_visibilityOptions);
		
		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setLabel('Publish');

		$reset = new Zend_Form_Element_Reset('reset');
		$reset->setLabel('Cancel');
		
		$dir = new Zend_Form_Element_Hidden('dir');
		$zipfile = new Zend_Form_Element_Hidden('zipfile');

		$this->addElements(array(
			$title,
			$visibility,
			$dir,
			$zipfile,
			$submit,
			$reset
		));
		
		$contentForm->addSubForm($albumForm, 'albumForm');
		$contentForm->addSubForm($imagesForm, 'imagesForm');
		$contentForm->addSubForm($tracksForm, 'tracksForm');
		$this->addDisplayGroup(array('title'), 'Title');
		$this->addSubForm($contentForm, 'content');
		$this->addDisplayGroup(array('visibility', 'submit','reset'), 'Tools');
		
	}

}