<?php

class Admin_Form_BiographyForm extends Zend_Dojo_Form {

	public function init() {
		$this->setAttribs(array(
			'id'	=> 'BioForm'
		));		
		
		$biography = new Application_Model_BiographyMapper();
		$bioResults = $biography->fetchAll();
		
		$this->setName('biographyEditor');
		$this->setMethod('post');
		$this->setAction('update/');
		$this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);
		
		$this->addElement('editor', 'content', array(
			'value'		=> stripslashes($bioResults[0]->content),
			'plugins'	=> array('undo','redo','|','bold','italic','underline','strikethrough','|','createLink','unlink','viewsource','insertEntity'),
			'dojoType'	=> 'dijit.Editor',
			'stylesheets'	=> array('/modules/admin/styles/editor.css')
		));
		$this->content->setDijitParam('data-dojo-props', "extraPlugins:[{name: 'prettyprint'}, {name: 'LocalImage', uploadable: true, uploadUrl: '/admin/biography/upload/', baseImageUrl: '/images', fileMask: '*.jpg;*.jpeg;*.gif;*.png;*.bmp'}]");
		
		$this->submit = new Zend_Form_Element_Submit('Save Changes');
		
		//$this->addElements(array(
		//	$this->biography
		//));

		$this->setDecorators(array(
			'FormElements',
			array('HtmlTag', array('tag' => 'dl')),
			'Form'
		));
	}
}