<?php

class Admin_Form_LoginForm extends Zend_Dojo_Form {

	public function init() {
		$this->username = new Zend_Form_Element_Text('username');
		$this->username->setLabel('Username');
		
		$this->password = new Zend_Form_Element_Password('password');
		$this->password->setLabel('Password:');

		$this->submit = new Zend_Form_Element_Submit('Login');
		
		$this->addElements(array(
			$this->username,
			$this->password
		));

		$this->setDecorators(array(
			'FormElements',
			array('HtmlTag', array('tag' => 'dl')),
			'Form'
		));
	}
}