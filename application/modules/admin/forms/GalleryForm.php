<?php

class Admin_Form_GalleryForm extends Zend_Dojo_Form {

    protected $_visibilityOptions = array(
        '1'	=> 'Visible',
        '0'	=> 'Hidden'
    );

	public function init() {
		$this->setAttribs(array(
			'id'	=> 'NewsForm'
		));		
		$this->setMethod('post');
		
        $contentForm = new Zend_Dojo_Form_SubForm();
        $contentForm->setDecorators(array(
            'FormElements',
            array('TabContainer', array(
                'id' => 'tabContainer',
                'style' => 'width: 777px; height: 500px;',
                'dijitParams' => array(
                    'tabPosition' => 'top'
                )
            ))
        ));
        $contentForm->setIsArray(false);

        $galleryForm = new Zend_Dojo_Form_SubForm();
        $galleryForm->setAttribs(array(
        	'name'			=> 'galleryForm',
            'legend'		=> 'Gallery',
			'dijitParams'	=> array(
                'title'	=> 'Gallery'
			)
        ));
        $galleryForm->setIsArray(false);

		$title = new Zend_Form_Element_Text('title');
		$title->setLabel('Gallery Title')->setRequired(true);
        $galleryForm->addElement($title);
		
        $galleryForm->addElement(
            'Editor',
            'description',
			array(
				'plugins'		=> array('undo','redo','|','bold','italic','underline','strikethrough','|','viewsource'),
				'label'			=> 'Description',
                'inheritWidth'	=> 'true',
                'value'			=> ' '
			)
        );
		$galleryForm->description->setDijitParam('data-dojo-props', "extraPlugins:['prettyprint']");

        $imagesForm = new Zend_Dojo_Form_SubForm();
        $imagesForm->setAttribs(array(
            'name'   => 'imagesForm',
            'legend' => 'Images',
            'dijitParams' => array(
                'title' => 'Images'
            )
        ));

        $imagesForm->setIsArray(false);

		$images = new Zend_Form_Element_File('LinkImage');
		$images->setLabel('images');
		$images->addDecorators(array(array('HtmlTag', array('tag'=>'div', 'id' => 'UploadZone'))));
        $imagesForm->addElement($images);
        
		/**
		 * @todo Add the date and time boxes in the alternate method.
		 */

		$visibility = new Zend_Form_Element_Select('visibility');
		$visibility->setRequired(true);
		$visibility->addMultiOptions($this->_visibilityOptions);
		
		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setLabel('Publish');

		$reset = new Zend_Form_Element_Reset('reset');
		$reset->setLabel('Cancel');

		$galleryForm->addElement(
			'DateTextBox',
			'timestamp',
			array(
				'label'					=> 'Post Date',
				'required'				=> true,
				'invalidMessage'		=> 'Invalid date specified.',
				'formatLength'			=> 'long',
				'selector'				=> 'date'
			)
		);
		
		$this->addElements(array(
			$visibility,
			$submit,
			$reset
		));
		
		$contentForm->addSubForm($galleryForm, 'galleryForm');
		$contentForm->addSubForm($imagesForm, 'imagesForm');
		$this->addSubForm($contentForm, 'content');
		$this->addDisplayGroup(array('visibility', 'submit','reset'), 'Tools');
		
	}

}