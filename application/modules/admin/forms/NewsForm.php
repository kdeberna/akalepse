<?php

class Admin_Form_NewsForm extends Zend_Dojo_Form {

    protected $_statusOptions = array(
        'draft'		=> 'Draft',
        'published'	=> 'Published'
    );

	public function init() {
		$this->setAttribs(array(
			'id'	=> 'NewsForm'
		));		
		$this->setMethod('post');
		
        $contentForm = new Zend_Dojo_Form_SubForm();
        $contentForm->setDecorators(array(
            'FormElements',
            array('TabContainer', array(
                'id' => 'tabContainer',
                'style' => 'width: 777px; height: 500px;',
                'dijitParams' => array(
                    'tabPosition' => 'top'
                )
            ))
        ));

        $synopsisForm = new Zend_Dojo_Form_SubForm();
        $synopsisForm->setAttribs(array(
        	'name'			=> 'synopsisForm',
            'legend'		=> 'Synopsis',
			'dijitParams'	=> array(
                'title'	=> 'Synopsis'
			)
        ));
        $synopsisForm->addElement(
            'Editor',
            'synopsis',
			array(
				'plugins'		=> array('undo','redo','|','bold','italic','underline','strikethrough','|','createLink','unlink','viewsource','insertImage'),
				'label'			=> 'Synopsis',
                'inheritWidth'	=> 'true'
			)
        );
		$synopsisForm->synopsis->setDijitParam('data-dojo-props', "extraPlugins:['prettyprint']");

        $articleForm = new Zend_Dojo_Form_SubForm();
        $articleForm->setAttribs(array(
            'name'   => 'articleForm',
            'legend' => 'Article',
            'dijitParams' => array(
                'title' => 'Article'
            )
        ));
        $articleForm->addElement(
            'Editor',
            'article',
            array(
				'plugins'		=> array('undo','redo','|','bold','italic','underline','strikethrough','|','createLink','unlink','viewsource','insertImage'),
                'label'			=> 'Article',
                'inheritWidth'	=> 'true',
            )
        );
		$articleForm->article->setDijitParam('data-dojo-props', "extraPlugins:['prettyprint']");

		$title = new Zend_Form_Element_Text('title');
		$title->setLabel('Post Title')->setRequired(true);
		
		/**
		 * @todo Add the date and time boxes in the alternate method.
		 */

		$this->addElement(
			'DateTextBox',
			'creation_date',
			array(
				'label'					=> 'Post Date',
				'required'				=> true,
				'invalidMessage'		=> 'Invalid date specified.',
				'formatLength'			=> 'long',
				'selector'				=> 'date'
			)
		);
		
		$this->addElement(
			'TimeTextBox',
			'creation_time',
			array(
				'required'				=> true,
				'visibleIncrement'		=> 'T00:15:00',
				'clickableIncrement'	=> 'T00:15:00'
			)
		);

		$status = new Zend_Form_Element_Select('status');
		$status->setRequired(true);
		$status->addMultiOptions($this->_statusOptions);
		
		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setLabel('Publish');

		$reset = new Zend_Form_Element_Reset('reset');
		$reset->setLabel('Reset');
		
		$this->addElements(array(
			$title,
			$status,
			$submit,
			$reset
		));
		
		$this->addDisplayGroup(array('title'), 'Title');
		$this->addDisplayGroup(array('creation_time', 'creation_date'), 'Date');
		$contentForm->addSubForm($synopsisForm, 'synopsisForm');
		$contentForm->addSubForm($articleForm, 'articleForm');
		$this->addSubForm($contentForm, 'content');
		$this->addDisplayGroup(array('status', 'submit','reset'), 'Tools');
		
	}

}