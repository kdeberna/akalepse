<?php

class Admin_DiscographyController extends Zend_Controller_Action {

    public function init() {
		$this->view->headLink()->appendStylesheet('/modules/admin/styles/formsDiscog.css');
	}

    public function indexAction() {
		$this->view->headLink()->appendStylesheet('/modules/admin/styles/discog.css');
		$this->view->headScript()->appendFile('/modules/admin/scripts/discogDelete.js');
        $discography = new Application_Model_DiscographyMapper();
        $this->view->entries = $discography->fetchAll();
    }

	public function deleteAction() {
    	$request = $this->getRequest();
		$this->view->id = $this->_getParam('id');	
		
		if($request->isXmlHttpRequest()){
			$this->_helper->layout()->disableLayout();
		}
		
		$discogMapper = new Application_Model_DiscographyMapper();
		$discog = new Application_Model_Discography();
		$discogMapper->find($request->getParam('id'), $discog);
		$this->view->title = stripslashes($discog->title);
		
		if($request->isPost()) {
			$discogMapper->delete($request->getParam('id'));
 			$tracksMapper = new Application_Model_TracksMapper();
			$tracks = $tracksMapper->FetchByAlbum($request->getParam('id'));
			foreach($tracks as $track){
				$tracksMapper->delete($track->id);
			}
			// Open the album data folder and remove all the files
			$dir = new DirectoryIterator(APPLICATION_PATH . '/../html/releases/' . $discog->dir . '/');
			foreach($dir as $fileinfo) {
				if(!$fileinfo->isDot()) {
					unlink($fileinfo->getPathname());
				}
			}
 			// Remove the album data folder
 			rmdir(APPLICATION_PATH . '/../html/releases/' . $discog->dir . '/');
			$this->xml();
			header("location: /admin/discography/");
		}
	}

	public function addAction() {
		$this->view->headScript()->appendFile('/modules/admin/scripts/discogTracks.js');
		$request = $this->getRequest();
		$form = new Admin_Form_DiscographyForm();
	   	$form->setAction('/admin/discography/add/');
		$today = date('Y-m-d');
		$data['release_date'] = $today;
		$form->populate($data);
    	$this->view->form = $form;

		if($request->isPost()) {
			$params = $request->getParams();
			//header("location: /admin/discography/add/");
			//echo '<pre>' . htmlentities(print_r($GLOBALS['_FILES'], true)) . '</pre>';
			$newFolder = str_replace(" ", "-", strtolower($params['title']));
			$release = $newFolder = preg_replace('/[^\w-]+/', "", $newFolder);
			$newFolder = APPLICATION_PATH . '/../html/releases/' . $newFolder . '/';

			if(isset($_FILES['album_cover']['name'])) {
				$albumCover = str_replace(" ", "-", strtolower($_FILES['album_cover']['name']));
				$albumCover = preg_replace('/[^\w-.]+/', "", $albumCover);
				$params['album_cover'] = $albumCover;
			} else {
				$params['album_cover'] = '';
			}

			if(isset($_FILES['album_interior']['name'])) {
				$albumInterior = str_replace(" ", "-", strtolower($_FILES['album_interior']['name']));
				$albumInterior = preg_replace('/[^\w-.]+/', "", $albumInterior);
				$params['album_interior'] = $albumInterior;
			} else {
				$params['album_interior'] = '';
			}

			if(isset($_FILES['album_back_cover']['name'])) {
				$backCover = str_replace(" ", "-", strtolower($_FILES['album_back_cover']['name']));
				$backCover = preg_replace('/[^\w-.]+/', "", $backCover);
				$params['album_back_cover'] = $backCover;
			} else {
				$params['album_back_cover'] = '';
			}

			$params['dir'] = $release;
			$dir = new DirectoryIterator(APPLICATION_PATH . '/../html/releases/tmp/');
			foreach($dir as $fileinfo) {
				if(pathinfo($fileinfo->getFilename(), PATHINFO_EXTENSION) == 'zip') {
					$adjustedZip = str_replace(" ", "-", strtolower($fileinfo->getFilename()));
					$adjustedZip = preg_replace('/[^\w-.]+/', "", $adjustedZip);
					$params['zipfile'] = $adjustedZip;
				}
			}

			$discogMapper = new Application_Model_DiscographyMapper();
			$discog = new Application_Model_Discography($params);
			$discogID = $discogMapper->save($discog);
			$tracksMapper = new Application_Model_TracksMapper();
			if(isset($params['track'])) {
				$numTracks = count($params['track']);
			} else {
				$numTracks = 0;
			}
			$i = 0;
			$xmlData = "<playlist>\n";
 			while($i < $numTracks) {
				$data['disco_id'] = $discogID;
				$data['track_num'] = $i + 1;
				$data['title'] = $params['track'][$i];
				$data['artist'] = $params['artist'][$i];

				$replacedName = str_replace(" ", "-", strtolower($params['filename'][$i]));
				$replacedName = preg_replace('/[^\w-.]+/', "", $replacedName);

				$data['mp3'] = $replacedName;
				$track = new Application_Model_Tracks($data);
				$tracksMapper->save($track);
				$xmlData .= "\t<item>\n";
				$xmlData .= "\t\t<url>/releases/" . $release . "/" . $replacedName . "</url>\n";
				$xmlData .= "\t\t<title>" . stripslashes($params["track"][$i]) . "</title>\n";
				$xmlData .= "\t\t<artist>" . stripslashes($params["artist"][$i]) . "</artist>\n";
				$xmlData .= "\t\t<album>" . stripslashes($params["title"]) . "</album>\n";
				$xmlData .= "\t</item>\n";
				$i++;
 			}
			$xmlData .= '</playlist>';
			mkdir($newFolder);
			foreach($dir as $fileinfo) {
				if(!$fileinfo->isDot()) {
					$filenameMod = str_replace(" ", "-", strtolower($fileinfo->getFilename()));
					$filenameMod = preg_replace('/[^\w-.]+/', "", $filenameMod);
					rename($fileinfo->getPathname(), $newFolder . $filenameMod);
				}
			}
			$my_file = 'playlist.xml';
			$handle = fopen($newFolder . $my_file, 'w') or die('Cannot open file:  ' . $my_file);
			fwrite($handle, $xmlData);
			fclose($handle);
			$this->xml();
			header("location: /admin/discography/");
		}
	}

	public function xml() {
		// Move this out to a Helper something or other
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
    	$xmlData = "<albums>\n";
        $entries = new Application_Model_DiscographyMapper();
        $tracksMapper = new Application_Model_TracksMapper();
        $tracksEntry = new Application_Model_Tracks();
		$discography = $entries->fetchAll();
		foreach($discography as $album) {
			if($album->playable == "1") {
				$xmlData .= "\t<item>\n";
				$albumID = $album->id;
				$tracks = $tracksMapper->find($albumID, $tracksEntry);
				$count = count($tracks);
				$xmlData .= "\t\t<url>/releases/" . $album->dir . "/playlist.xml</url>\n";
				$xmlData .= "\t\t<title>" . stripslashes($album->title) . "</title>\n";
				$xmlData .= "\t\t<artist></artist>\n";
				$xmlData .= "\t\t<date>" . date("F d, Y", strtotime($album->release_date)) . "</date>\n";
				$xmlData .= "\t\t<trackInfo>" . $count . " tracks</trackInfo>\n";
				$xmlData .= "\t\t<extraInfo>" . stripslashes($album->shortdesc) . "</extraInfo>\n";
				$xmlData .= "\t\t<imageUrl>/releases/" . $album->dir . "/" . $album->album_cover . "</imageUrl>\n";
				if($album->downloadable == '1') {
					$xmlData .= "\t\t<downloadUrl>/releases/" . $album->dir . "/" . $album->zipfile . "</downloadUrl>\n";
				} else {
					$xmlData .= "\t\t<downloadUrl></downloadUrl>\n";
				}
				$xmlData .= "\t</item>\n";
			}
		}
		$xmlData .= '</albums>';
		// write out file
		$handle = fopen(APPLICATION_PATH . '/../html/xml/releases.xml', 'w') or die('Cannot open file.');
		fwrite($handle, $xmlData);
		fclose($handle);
	}
	
	public function editAction() {
		$this->view->headScript()->appendFile('/modules/admin/scripts/discogTracks.js');
		$this->view->headScript()->appendFile('/modules/admin/scripts/discogImages.js');

    	$request = $this->getRequest();
		$this->view->id = $this->_getParam('id');	

		$form = new Admin_Form_DiscographyForm();
		$form->setAction('/admin/discography/edit/');
		
		$mapper = new Application_Model_DiscographyMapper();
		$entry = new Application_Model_Discography();
		$mapper->find($request->getParam('id'), $entry);
		
		$this->view->title = stripslashes($entry->title);

		$form->addElement('hidden', 'id', array('value' => $request->getParam('id')));
		$form->addElement('hidden', 'album_cover_image', array('value' => $entry->album_cover));
		$form->addElement('hidden', 'album_interior_image', array('value' => $entry->album_interior));
		$form->addElement('hidden', 'album_back_cover_image', array('value' => $entry->album_back_cover));
 		$data['release_date'] = date('Y-m-d', strtotime($entry->release_date));
 		$data['shortdesc'] = stripslashes($entry->shortdesc);
 		$data['title'] = stripslashes($entry->title);
 		$data['description'] = stripslashes($entry->description);
 		$data['shorttitle'] = stripslashes($entry->shorttitle);
 		$data['downloadable'] = $entry->downloadable;
 		$data['playable'] = $entry->playable;
 		$data['album_cover'] = $entry->album_cover;
 		$data['album_interior'] = $entry->album_interior;
 		$data['album_back_cover'] = $entry->album_back_cover;
 		$data['section'] = $entry->section;
 		$data['dir'] = $entry->dir;
		
		$form->populate($data);
		$this->view->form = $form;

		if($request->isPost()) {
			$params = $request->getParams();
//			echo '<pre>' . htmlentities(print_r($params, true)) . '</pre>';
			if($_FILES['album_cover']['name'] != '') {
				$params['album_cover'] = $_FILES['album_cover']['name'];
			} else {
				$params['album_cover'] = $params['album_cover_image'];
			}
			if($_FILES['album_interior']['name'] != '') {
				$params['album_interior'] = $_FILES['album_interior']['name'];
			} else {
				$params['album_interior'] = $params['album_interior_image'];
			}
			if($_FILES['album_back_cover']['name'] != '') {
				$params['album_back_cover'] = $_FILES['album_back_cover']['name'];
			} else {
				$params['album_back_cover'] = $params['album_back_cover_image'];
			}
			$discog = new Application_Model_Discography($params);
			$mapper->save($discog);
 			$tracksMapper = new Application_Model_TracksMapper();
			$tracks = $tracksMapper->FetchByAlbum($request->getParam('id'));
			foreach($tracks as $track){
				$tracksMapper->delete($track->id);
			}
			$numTracks = count($params['track']);
			$i = 0;
			$xmlData = "<playlist>\n";
 			while($i < $numTracks) {
				$data['disco_id'] = $request->getParam('id');
				$data['track_num'] = $i + 1;
				$data['title'] = $params['track'][$i];
				$data['artist'] = $params['artist'][$i];
				$data['mp3'] = $params['filename'][$i];
				$track = new Application_Model_Tracks($data);
				$tracksMapper->save($track);
				$xmlData .= "\t<item>\n";
				if($params["filename"][$i] != '') {
					$xmlData .= "\t\t<url>/releases/" . $entry->dir . "/" . $params["filename"][$i] . "</url>\n";
				} else {
					$xmlData .= "\t\t<url></url>\n";
				}
				$xmlData .= "\t\t<title>" . stripslashes($params["track"][$i]) . "</title>\n";
				$xmlData .= "\t\t<artist>" . stripslashes($params["artist"][$i]) . "</artist>\n";
				$xmlData .= "\t\t<album>" . stripslashes($params["title"]) . "</album>\n";
				$xmlData .= "\t</item>\n";
				$i++;
 			}
			$xmlData .= "</playlist>";
			$my_file = 'playlist.xml';
			$handle = fopen(APPLICATION_PATH . '/../html/releases/' . $entry->dir . '/' . $my_file, 'w') or die('Cannot open file:  '.$my_file);
			fwrite($handle, $xmlData);
			fclose($handle);
			$this->xml();
			$dir = new DirectoryIterator(APPLICATION_PATH . '/../html/releases/tmp/');
			foreach($dir as $fileinfo) {
				if(!$fileinfo->isDot()) {
					rename($fileinfo->getPathname(), APPLICATION_PATH . '/../html/releases/' . $entry->dir . "/" . $fileinfo->getFilename());
				}
			}

			header("location: /admin/discography/");
		}
	}
	
	public function unzipAction() {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
    	$request = $this->getRequest();
    	$response = '';

		$fn = (isset($_SERVER['HTTP_X_FILENAME']) ? $_SERVER['HTTP_X_FILENAME'] : false);
		if($fn) {
			// AJAX call
			file_put_contents(
				APPLICATION_PATH . '/../html/releases/tmp/' . $fn,
				file_get_contents('php://input')
			);
		}

		$tags = new GetID3_getid3();
		$zip = new ZipArchive();
		
		$format = $tags->analyze(APPLICATION_PATH . '/../html/releases/tmp/' . $fn);
		if($format['fileformat'] == 'mp3') {
			echo ':' . $fn; // Returns for a single file, the colon denotes single file with the filename following
			exit();
		}

		if($zip->open(APPLICATION_PATH . '/../html/releases/tmp/' . $fn)) {
			$title = '';
			for($i = 0; $i < $zip->numFiles; $i++) {
				if(substr($zip->getNameIndex($i), 0, 1) != '_') { // substr filters out __MACOSX folder which is auto-created
					$path_parts = pathinfo($zip->getNameIndex($i));
					$zip->extractTo(APPLICATION_PATH . '/../html/releases/tmp/',  $zip->getNameIndex($i));
					rename(APPLICATION_PATH . '/../html/releases/tmp/' . $zip->getNameIndex($i), APPLICATION_PATH . '/../html/releases/tmp/' . $path_parts['basename']);
					if(is_dir(APPLICATION_PATH . '/../html/releases/tmp/' . $path_parts['basename'])) {
						$title = $path_parts['basename'];
					}
					$metadata = $tags->analyze(APPLICATION_PATH . '/../html/releases/tmp/' . $path_parts['basename']);
					if(isset($metadata['audio'])) {
						$response .= '<tr><td><input type="text" name="track[]" class="trackInput" value="' . $metadata['tags']['id3v2']['title']['0'] . '" /></td><td><input type="text" name="artist[]" class="artistInput" value="' . $metadata['tags']['id3v2']['artist']['0'] . '" /></td><td>' . $metadata['filename'] . '<input type="hidden" name="filename[]" value="' . $metadata['filename'] . '" /></td><td><a href="delete_track"><img src="/images/icons/delete.png" width="18" height="18" border="0" /></a></td></tr>';
					}
				}
			}
			rmdir(APPLICATION_PATH . '/../html/releases/tmp/' . $title);
		} else { 
			 $response = '<tr><td>Error reading archive!</td><td>k</td><td>d</td></tr>'; 
		}
		echo $response;
	}

    public function uploadAction() {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$fn = (isset($_SERVER['HTTP_X_FILENAME']) ? $_SERVER['HTTP_X_FILENAME'] : false);
		if($fn) {
			file_put_contents(
				APPLICATION_PATH . '/../html/releases/tmp/' . $fn,
				file_get_contents('php://input')
			);
			echo $fn;
			exit();
		}
    }
    
    public function tracksAction() {
    	$data = '';
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$tracksMapper = new Application_Model_TracksMapper();
		$tracks = $tracksMapper->FetchByAlbum($this->_getParam('id'));
		foreach($tracks as $track){
			$data .= '<tr id="track' . $track->track_num . '"><td><input type="text" name="track[]" class="trackInput" value="' . stripslashes($track->title) . '" /></td><td><input type="text" name="artist[]" class="artistInput" value="' . stripslashes($track->artist) . '" /></td><td>' . $track->mp3 . '<input type="hidden" name="filename[]" value="' . $track->mp3 . '" /></td><td><button>Upload MP3</button> <a href="delete_track"><img src="/images/icons/delete.png" width="18" height="18" border="0" /></a></td></tr>';
		}
		//echo '<pre>' . htmlentities(print_r($data, true)) . '</pre>';
		echo $data;
	}

}