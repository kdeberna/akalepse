<?php

class Admin_IndexController extends Zend_Controller_Action {

    public function init() {
		//$this->view->headScript()->appendFile('/modules/admin/scripts/login.js');
    }

    public function indexAction() {

//		$datas = $this->getRequest()->getParams();
//		print_r($datas);
//		$this->_helper->layout()->disableLayout();
//		$this->_helper->viewRenderer->setNoRender(true);

		$loginForm = new Admin_Form_LoginForm();
		$loginForm->setAction('/admin/');
		$loginForm->setMethod('post');
		
		if($this->getRequest()->isPost()) {
			$data = $this->getRequest()->getParams();
 			$db = Zend_Db_Table::getDefaultAdapter();
 			$authAdapter = new Zend_Auth_Adapter_DbTable($db, 'admin_users', 'username', 'password');
 			$authAdapter->setIdentity($data['username']);
 			$authAdapter->setCredential(md5($data['password']));
			$result = $authAdapter->authenticate();
			if($result->isValid()) {
 				$auth = Zend_Auth::getInstance();
 				$storage = $auth->getStorage();
 				$storage->write($authAdapter->getResultRowObject(array('username', 'first_name', 'last_name', 'role')));
 				$this->_helper->redirector->gotoUrl('/admin/home/');
 			} else {
 				$this->view->loginMessage = 'Invalid username or password.';
				$this->view->loginForm = $loginForm;
 			}
		} else {
			$this->view->loginForm = $loginForm;
		}
	}
    
	public function logoutAction() {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		Zend_Auth::getInstance()->clearIdentity();
		$this->_helper->redirector->gotoUrl('/admin/');
	}
	
	public function loginAction() {

	}
}