<?php

class Admin_EventsController extends Zend_Controller_Action {

    public function init() {
		$this->view->headLink()->appendStylesheet('/modules/admin/styles/eventForm.css');
		$this->view->headLink()->appendStylesheet('/modules/admin/styles/events.css');
    }

    public function indexAction() {
		$this->view->headScript()->appendFile('/modules/admin/scripts/newsActions.js');

        $events = new Application_Model_EventsMapper();
        $this->view->eventList = $events->fetchAll();
        $this->view->form = new Admin_Form_EventForm();
    }
    
    public function addAction() {
		$this->view->headScript()->appendFile('/modules/admin/scripts/FlyerUpload_Front.js');
		$this->view->headScript()->appendFile('/modules/admin/scripts/FlyerUpload_Back.js');
		$request = $this->getRequest();
		$form = new Admin_Form_EventForm();
		$form->setAction('/admin/events/add/');

		$data['date'] = date('Y-m-d');
		$data['start'] = 'T22:00:00';
		$data['end'] = 'T24:00:00';
		$form->populate($data);
		
		if($request->isPost()) {
			//if ($form->isValid($request->getPost())) {
				$params = $request->getParams();
				if(isset($params['cancel'])) {
					header("location: /admin/events/");
					exit;
				}
				$params['rsvp'] = '';
				$params['start'] = substr($params['start'], 1);
				$params['end'] = substr($params['end'], 1);
				$params['description'] = trim($params['description']);
				$params['synopsis'] = trim($params['synopsis']);
				$resize = $this->_helper->baseImageSize;
				$resize->setBaseMaxSize('528');
				if($params['flyerfront']) {
					if($resize->baseImageSize($params['flyerfront'], '/images/tmp/', '/images/events/')) {
						rename(APPLICATION_PATH . '/../html/images/tmp/' . $params['flyerfront'], APPLICATION_PATH . '/../html/images/events/' . $params['flyerfront']);
					}
				} else {
					$params['flyerfront'] = '';
				}
				if($params['flyerback']) {
					if($resize->baseImageSize($params['flyerback'], '/images/tmp/', '/images/events/')) {
						rename(APPLICATION_PATH . '/../html/images/tmp/' . $params['flyerback'], APPLICATION_PATH . '/../html/images/events/' . $params['flyerback']);
					}
				} else {
					$params['flyerback'] = '';
				}

				$entry = new Application_Model_Events($params);
 				$mapper = new Application_Model_EventsMapper();
 				$mapper->save($entry);
			//}
// 			// Kill off any leftover images that may still here due to an error or abandonment
 			$dir = opendir(APPLICATION_PATH . '/../html/images/tmp/');
// 			// $iterator = new DirectoryIterator($dir);
// 			// http://php.net/directoryiterator
// 			// List files in images directory
			while(($file = readdir($dir)) !== false) {
				if($file != '.' && $file != '..'){
					unlink(APPLICATION_PATH . '/../html/images/tmp/' . $file);
				}
			}
			closedir($dir);
			header("location: /admin/events/");
		}
		$this->view->form = $form;
    }
    
    public function editAction() {
		$this->view->headScript()->appendFile('/modules/admin/scripts/FlyerUpload_Front.js');
		$this->view->headScript()->appendFile('/modules/admin/scripts/FlyerUpload_Back.js');
		$request = $this->getRequest();
		$id =$request->id;
		$form = new Admin_Form_EventForm();
		$form->setAction('/admin/events/edit/' . $id . '/');
		$event = new Application_Model_Events();
		$mapper = new Application_Model_EventsMapper();
		$mapper->find($id, $event);
		$this->view->hiddenBack = $event->flyerback;
		$this->view->hiddenFront = $event->flyerfront;
		
		// Yuck!!!!
		$data['date'] = $event->date;
		$data['start'] = 'T' . $event->start;
		$data['end'] = 'T' . $event->end;
		$data['title'] = $event->title;
		$data['rsvp'] = $event->rsvp;
		$data['repeat'] = $event->repeat;
		$data['flyerfront'] = 'kevin';
		$data['flyerback'] = $event->flyerback;
		$data['description'] = stripslashes($event->description);
		$data['who'] = $event->who;
		$data['venue'] = $event->venue;
		$data['address'] = $event->address;
		$data['cover'] = $event->cover;
		$data['synopsis'] = stripslashes($event->synopsis);
		$form->populate($data);
		$form->getDisplayGroup('Tools')->addElement(new Zend_Form_Element_Submit('copy', 'Copy'));
		$form->getElement('submit')->setLabel('Save');
		$this->view->form = $form;

		if($request->isPost()) {
			//if ($form->isValid($request->getPost())) {
				$params = $request->getParams();
				if(isset($params['cancel'])) {
					header("location: /admin/events/");
					exit;
				}
				$params['start'] = substr($params['start'], 1, strlen($params['start']));
				$params['end'] = substr($params['end'], 1, strlen($params['end']));
				$params['rsvp'] = '';
				$params['description'] = trim($params['description']);
				$params['synopsis'] = trim($params['synopsis']);
				
				if(isset($params['copy'])) {
					unset($params['id']);
				}

				$resize = $this->_helper->baseImageSize;
				$resize->setBaseMaxSize('528');
				if($params['flyerfront']) {
					if(!file_exists(APPLICATION_PATH . '/../html/images/events/' . $params['flyerfront'])) {
						if($resize->baseImageSize($params['flyerfront'], '/images/tmp/', '/images/events/')) {
							rename(APPLICATION_PATH . '/../html/images/tmp/' . $params['flyerfront'], APPLICATION_PATH . '/../html/images/events/' . $params['flyerfront']);
						}
					}
				} else {
					$params['flyerfront'] = '';
				}
				if($params['flyerback']) {
					if(!file_exists(APPLICATION_PATH . '/../html/images/events/' . $params['flyerback'])) {
						if($resize->baseImageSize($params['flyerback'], '/images/tmp/', '/images/events/')) {
							rename(APPLICATION_PATH . '/../html/images/tmp/' . $params['flyerback'], APPLICATION_PATH . '/../html/images/events/' . $params['flyerback']);
						}
					}
				} else {
					$params['flyerback'] = '';
				}

				$entry = new Application_Model_Events($params);
 				$mapper = new Application_Model_EventsMapper();
 				$mapper->save($entry);

			//}
// 			// Kill off any leftover images that may still here due to an error or abandonment
 			$dir = opendir(APPLICATION_PATH . '/../html/images/tmp/');
// 			// $iterator = new DirectoryIterator($dir);
// 			// http://php.net/directoryiterator
// 			// List files in images directory
			while(($file = readdir($dir)) !== false) {
				if($file != '.' && $file != '..'){
					unlink(APPLICATION_PATH . '/../html/images/tmp/' . $file);
				}
			}
			closedir($dir);
			header("location: /admin/events/");
		}
    }
    
    public function deleteAction() {
		$request = $this->getRequest();
		
		if($request->isXmlHttpRequest()){
			$this->_helper->layout()->disableLayout();
		}
		
		$mapper = new Application_Model_EventsMapper();
		$entry = new Application_Model_Events();
		$mapper->find($request->getParam('id'), $entry);

		$this->view->title = stripslashes($entry->title);
		$this->view->id = $entry->id;
		
		if($request->isPost()) {
			// Remove the Event entry in the database
			$event = new Application_Model_Events();
			$mapper = new Application_Model_EventsMapper();
			$mapper->find($request->getParam('id'), $event);
			$mapper->delete($request->getParam('id'));
			// Remove all the image entries in the database

			// Remove the actual image files
			if($event->flyerfront) {
				unlink(APPLICATION_PATH . '/../html/images/events/' . $event->flyerfront);
			}
			if($event->flyerback) {
				unlink(APPLICATION_PATH . '/../html/images/events/' . $event->flyerback);
			}
			header("location: /admin/events/");
		}
    
    }
    
    public function saveAction() {
    	// Use for adding as well as editing an event
    	// Editing only prepopulates the fields
		$this->_helper->layout()->disableLayout();
		$theForm = new Admin_Form_EventForm();
		$this->view->form = $theForm;

		// If we get an ID then we must be editing, so populate the fields
		if($this->getRequest()->getParam('id')) {
			$editEvent = new Application_Model_EventsMapper();
			$newThing = new Application_Model_Events();
			$editEvent->find($this->getRequest()->getParam('id'), $newThing);
			$it['title'] = $newThing->title;
			$it['date'] = $newThing->date;
			$it['start'] = $newThing->start;
			$it['end'] = $newThing->end;
			$it['repeat'] = $newThing->repeat;
			$it['description'] = $newThing->description;
			$it['synopsis'] = $newThing->synopsis;
			$it['who'] = $newThing->who;
			$it['venue'] = $newThing->venue;
			$it['cover'] = $newThing->cover;
			$it['rsvp'] = $newThing->rsvp;
			$theForm->populate($it);
		}
		
        $this->view->confirmNewButton = $this->view->button(
			'ConfirmAddEvent', // ID and name
			'Add this event', // Display text
			array('iconClass' => 'myButtons'), // Dijit parameters
			array('class' => 'action', 'style' => 'display: none') // HTML attributes
		);
        $this->view->confirmSaveButton = $this->view->button(
			'ComfirmSaveEvent', // ID and name
			'Commit the changes', // Display text
			array('iconClass' => 'myButtons'), // Dijit parameters
			array('class' => 'action', 'style' => 'display: none') // HTML attributes
		);
        $this->view->cancelButton = $this->view->button(
			'CancelNew', //ID and name
			'Cancel', // Display text
			array('iconClass' => 'myButtons'), // Dijit parameters
			array('class' => 'cancel') // HTML attributes
		);    

		if($this->getRequest()->isPost()) {
	        $this->_helper->viewRenderer->setNoRender(true);
			$events = new Application_Model_EventsMapper();
 			$pars = $this->getRequest()->getParams();
			$theEvent = new Application_Model_Events($pars);
			if($this->getRequest()->getParam('id')) {
				$theEvent->id = $this->getRequest()->getParam('id');
			}
		    $events->save($theEvent);
		}
		$this->view->id = $this->getRequest()->getParam('id');
	}

    public function uploadAction() {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$fn = (isset($_SERVER['HTTP_X_FILENAME']) ? $_SERVER['HTTP_X_FILENAME'] : false);
		if($fn) {
			// AJAX call
			file_put_contents(
				'images/tmp/' . $fn,
				file_get_contents('php://input')
			);
			echo $fn;
			exit();
		}
    }

}