<?php

class Admin_BiographyController extends Zend_Controller_Action {

	public function init() {
		$this->view->headTitle()->append('Admin');
		$this->view->headTitle()->append('Biography');
	}

	public function indexAction() {		
		$this->view->headLink()->appendStylesheet('/scripts/dojox/editor/plugins/resources/css/InsertEntity.css');
		$this->view->headLink()->appendStylesheet('/scripts/dojox/editor/plugins/resources/css/LocalImage.css');
		$this->view->headLink()->appendStylesheet('/scripts/dojox/form/resources/FileUploader.css');

		$bioForm = new Admin_Form_BiographyForm();
		$this->view->biographyEditor = $bioForm;
	}

	public function updateAction() {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		if($this->getRequest()->isPost()){
			$bioMapper = new Application_Model_BiographyMapper();
			$content = $this->getRequest()->getParams();
			$bioModel = new Application_Model_Biography($content);
			$bioModel->id = 1;
			$bioMapper->save($bioModel);
		}
		header("location: /admin/biography/");
	}
   
	public function uploadAction() {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		move_uploaded_file($_FILES['uploadedfile']['tmp_name'], APPLICATION_PATH . '/../html/images/uploads/' . $_FILES['uploadedfile']['name']);
		list($width, $height) = getimagesize(APPLICATION_PATH . '/../html/images/uploads/' . $_FILES['uploadedfile']['name']);
		$file['file'] = 'uploads/' . $_FILES['uploadedfile']['name'];
		$file['name'] = $_FILES['uploadedfile']['name'];
		$file['width'] = $width;
		$file['height'] = $height;
		$file['type'] = end(explode(".", APPLICATION_PATH . '/../html/images/uploads/' . $_FILES['uploadedfile']['name']));
		$file['size'] = filesize(APPLICATION_PATH . '/../html/images/uploads/' . $_FILES['uploadedfile']['name']);
		$json = Zend_Json::encode($file);
		echo '<textarea>' . $json . '</textarea>';
	}
}