<?php

class Admin_HomeController extends Zend_Controller_Action {

    public function init() {
		$this->view->headLink()->appendStylesheet('/modules/admin/styles/home.css');
		$this->view->headScript()->appendFile('/modules/admin/scripts/home.js');
	}

    public function indexAction() {
		$request = $this->getRequest();

        $homepage = new Application_Model_HomepageMapper();
        $slides = $homepage->fetchAll();
		if($request->isXmlHttpRequest()){
			echo Zend_Json::encode($slides);
			exit();
		}
    }

    public function uploadAction() {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$fn = (isset($_SERVER['HTTP_X_FILENAME']) ? $_SERVER['HTTP_X_FILENAME'] : false);
		$resize = $this->_helper->baseImageSize;
		if($fn) {
			// AJAX call
			file_put_contents(
				// create temporary folder to isolate files from this session
				APPLICATION_PATH . '/../html/images/home_slideshow/' . $fn,
				file_get_contents('php://input')
			);
			$resize->baseImageSize2($fn, '/images/home_slideshow/', '/images/home_slideshow/');
			echo $fn;
			exit();
		}
    }

    public function clearimageAction() {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
    	$id = $this->_getParam('id');
    	$filename = $this->getParam('filename');
    	if($id != 'invalid') {
			$mapper = new Application_Model_HomepageMapper();
			$mapper->delete($id);
		}
		unlink(APPLICATION_PATH . '/../html/images/home_slideshow/'. $filename);
    }
    
    public function saveAction() {
		$request = $this->getRequest();
		$params = $request->getParams();
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$decoded = json_decode(stripslashes($params['theShit']), true);
		$mapper = new Application_Model_HomepageMapper();
//		echo $decoded['id'];
// 		foreach($decoded as $entry) {
			if($decoded['databaseID'] != '') {
				$data['id'] =  $decoded['databaseID'];
			} else {
				unset($data['id']);
			}
			$data['order'] = $decoded['order'];
			$data['image_url'] = $decoded['id'];
			$data['caption'] = $decoded['url'];
			$entry = new Application_Model_Homepage($data);
			echo $mapper->save($entry);
// 		}
	
	}

}