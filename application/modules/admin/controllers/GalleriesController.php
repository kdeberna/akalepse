<?php

class Admin_GalleriesController extends Zend_Controller_Action {

    public function init() {
		$this->view->headLink()->appendStylesheet('/modules/admin/styles/formsGalleries.css');
    }

    public function indexAction() {
		$this->view->headScript()->appendFile('/modules/admin/scripts/newsActions.js');
		$mapper = new Application_Model_GalleriesMapper();
		$galleries = $this->view->entries = $mapper->fetchAll();

		foreach($galleries as $gallery){
			// Get images per gallery
			$imageMapper = new Application_Model_ImagesMapper();
			$images = $imageMapper->FetchByAlbumId($gallery->id);
			$galleryName = str_replace("-", "", $gallery->dir);
			$this->view->{$galleryName} = count($images);
			$cover = $galleryName . '-cover';
			foreach($images as $image) {
				if($image->cover == 1) {
					$this->view->{$cover} = $image->filename;
				}
			}
		}
    }
    
    public function addAction() {
		$this->view->headScript()->appendFile('/modules/admin/scripts/filedrag3.js');
    	$request = $this->getRequest();
    	$form = new Admin_Form_GalleryForm();
    	$form->setAction('/admin/galleries/add/');

		$today = date('Y-m-d');
		$data['timestamp'] = $today;
		$form->populate($data);
    	
    	$this->view->form = $form;

    	if($request->isPost()) {
			//if($form->isValid($request->getPost())) {
				// Uploaded images need to be added to the galleries_images table
				$params = $request->getParams();
				$dirName = str_replace(" ", "-", strtolower($params['title']));
				$dirName = preg_replace('/[^\w-]+/', "", $dirName);
				$dirName = stripslashes($dirName);
				$params['dir'] = $dirName;
				$imageMeta = json_decode(stripslashes($params['images']));
				unset($params['images']);
				$entry = new Application_Model_Galleries($params);
				$mapper = new Application_Model_GalleriesMapper();
				$id = $mapper->save($entry);
				mkdir(APPLICATION_PATH . '/../html/images/galleries/' . $dirName, 0755, true);
// 				// If there are images, insert each into galleries_images table
				$resize = $this->_helper->baseImageSize;
				$resize->setBaseMaxSize('500');
				if(isset($imageMeta[0])) {
 					foreach($imageMeta as $image) {
 						$data['album_id'] = $id;
 						$data['order'] = $image->order;
 						$data['cover'] = $image->cover;
 						$data['filename'] = $image->filename;
 						$data['caption'] = $image->caption;
 						$data['credit'] = $image->credit;
						$images = new Application_Model_Images($data);
						$imageMapper = new Application_Model_ImagesMapper();
						$imageMapper->save($images);
						if($resize->baseImageSize($image->filename, '/images/tmp/', '/images/galleries/' . $dirName . '/')) {
							rename(APPLICATION_PATH . '/../html/images/tmp/' . $image->filename, APPLICATION_PATH . '/../html/images/galleries/' . $dirName . '/' . $image->filename);
						}
 					}
				}
			//}
			// Kill off any leftover images that may still here due to an error or abandonment
			$dir = opendir(APPLICATION_PATH . '/../html/images/tmp/');
			// $iterator = new DirectoryIterator($dir);
			// http://php.net/directoryiterator
			// List files in images directory
			while(($file = readdir($dir)) !== false) {
				if($file != '.' && $file != '..'){
					unlink(APPLICATION_PATH . '/../html/images/tmp/' . $file);
				}
			}
			closedir($dir);

			header("location: /admin/galleries/");
    	}
    }
    
    public function editAction() {
		$this->view->headScript()->appendFile('/modules/admin/scripts/filedrag3_forEditing.js');
    	$request = $this->getRequest();
		$galleryDir = $this->_getParam('gallery');

		$mapper = new Application_Model_GalleriesMapper();
		$gallery = $mapper->fetchByDir($galleryDir);

    	$form = new Admin_Form_GalleryForm();
    	$form->setAction('/admin/galleries/edit/' . $galleryDir . '/');

		$data['timestamp'] = date('Y-m-d', strtotime($gallery[0]->timestamp));
		$this->view->time = $data['timestamp'];
		$data['title'] = stripslashes($gallery[0]->title);
		$data['description'] = stripslashes($gallery[0]->description);
		$data['visibility'] = $gallery[0]->visibility;
		$data['order'] = $gallery[0]->order;
		$form->populate($data);
    	
    	$this->view->form = $form;

    	if($request->isPost()) {
			//if($form->isValid($request->getPost())) {
				// Uploaded images need to be added to the galleries_images table
				$params = $request->getParams();
				$fuck['id'] = $gallery[0]->id;
				$fuck['order'] = '0';
				$fuck['visibility'] = '1';
				$fuck['dir'] = $galleryDir;
				$fuck['title'] = $params['title'];
				$fuck['description'] = $params['description'];
				$fuck['timestamp'] = $params['timestamp'];
				$imageMeta = json_decode(stripslashes($params['images']));
				unset($params['images']);
				$entry = new Application_Model_Galleries($fuck);
				$mapper = new Application_Model_GalleriesMapper();
				$mapper->save($entry);
				$dirName = $galleryDir;
				echo "Dir name: " . $dirName;
// 				$dirName = str_replace(" ", "-", strtolower($params['title']));
// 				$dirName = preg_replace('/[^\w-]+/', "", $dirName);
// 				$dirName = stripslashes($dirName);
				// If there are new images, insert each into galleries_images table
				$resize = $this->_helper->baseImageSize;
				$resize->setBaseMaxSize('500');
				if(isset($imageMeta[0])) {
 					foreach($imageMeta as $image) {
 						if($image->added == 0) {
	 						$imageData['id'] = $image->imageID;
	 					} else {
	 						unset($imageData['id']);
	 					}
 						$imageData['album_id'] = $gallery[0]->id;
 						$imageData['order'] = $image->order;
 						$imageData['cover'] = $image->cover;
 						$imageData['filename'] = $image->filename;
 						$imageData['caption'] = $image->caption;
 						$imageData['credit'] = $image->credit;
 						print_r($imageData);
						$images = new Application_Model_Images($imageData);
						$imageMapper = new Application_Model_ImagesMapper();
						$imageMapper->save($images);
						if($image->added == 1) {
							if($resize->baseImageSize($image->filename, '/images/tmp/', '/images/galleries/' . $dirName . '/')) {
								rename(APPLICATION_PATH . '/../html/images/tmp/' . $image->filename, APPLICATION_PATH . '/../html/images/galleries/' . $dirName . '/' . $image->filename);
							}
						}
 					}
				}
// 			//}
// 			// Kill off any leftover images that may still here due to an error or abandonment
 			$dir = opendir(APPLICATION_PATH . '/../html/images/tmp/');
// 			// $iterator = new DirectoryIterator($dir);
// 			// http://php.net/directoryiterator
// 			// List files in images directory
			while(($file = readdir($dir)) !== false) {
				if($file != '.' && $file != '..'){
					unlink(APPLICATION_PATH . '/../html/images/tmp/' . $file);
				}
			}
			closedir($dir);

 			header("location: /admin/galleries/");
    	}
    }
    
    public function imagesAction() {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$galleryDir = $this->_getParam('gallery');
		$mapper = new Application_Model_GalleriesMapper();
		$gallery = $mapper->fetchByDir($galleryDir);
		$imageMapper = new Application_Model_ImagesMapper();
		$results = $imageMapper->FetchByAlbumId($gallery[0]->id);
		echo Zend_Json::encode($results);
    }
    
    public function deleteAction() {
		$request = $this->getRequest();
		
		if($request->isXmlHttpRequest()){
			$this->_helper->layout()->disableLayout();
		}
		
		$mapper = new Application_Model_GalleriesMapper();
		$entry = new Application_Model_Galleries();
		$mapper->find($request->getParam('id'), $entry);

		$this->view->title = stripslashes($entry->title);
		$this->view->id = $entry->id;
		
		if($request->isPost()) {
			// Remove the Gallery entry in the database
			$mapper = new Application_Model_GalleriesMapper();
			$mapper->delete($request->getParam('id'));
			// Remove all the image entries in the database
			$imageMapper = new Application_Model_ImagesMapper();
			$images = $imageMapper->FetchByAlbumId($request->getParam('id'));
			foreach($images as $image){
				$imageMapper->delete($image->id);
			}
			// Remove the actual image files
			$dir = opendir(APPLICATION_PATH . '/../html/images/galleries/' . $entry->dir . '/');
			//List files in images directory
			while(($file = readdir($dir)) !== false) {
				if($file != '.' && $file != '..'){
					unlink(APPLICATION_PATH . '/../html/images/galleries/' . $entry->dir . '/' . $file);
				}
			}
			closedir($dir);
			// Remove the Gallery image folder
			rmdir(APPLICATION_PATH . '/../html/images/galleries/' . $entry->dir . '/');
			header("location: /admin/galleries/");
		}
    
    }
    
    public function clearimageAction() {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
    	$filename = $this->_getParam('filename');
		$mapper = new Application_Model_ImagesMapper();
		$entry = new Application_Model_Galleries();
		$galleryMapper = new Application_Model_GalleriesMapper();
		$results = $mapper->fetchAll();
		foreach($results as $result) {
			if($result->filename == $filename) {
				$id = $result->id;
				$galleryID = $result->album_id;
			}
		}
		$response = $galleryMapper->find($galleryID, $entry);
// 		$allImages = $mapper->fetchByAlbumId($galleryID);
// 		foreach($allImages as $image) {
// 			if($image->order > $order) {
// 				echo $image->order . ' > ' . $order;
// 				$image->order = $image->order - 1;
// 				$image->save($image);
// 			}
// 		}
		unlink(APPLICATION_PATH . '/../html/images/galleries/' . $entry->dir . '/' . $filename);
		$mapper->delete($id);
    }

    public function uploadAction() {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$fn = (isset($_SERVER['HTTP_X_FILENAME']) ? $_SERVER['HTTP_X_FILENAME'] : false);
		if($fn) {
			// AJAX call
			file_put_contents(
				// create temporary folder to isolate files from this session
				APPLICATION_PATH . '/../html/images/tmp/' . $fn,
//				APPLICATION_PATH . '/../html/files/' . $fn,
				file_get_contents('php://input')
			);
			echo $fn;
			exit();
		}
    }

}