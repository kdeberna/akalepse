<?php

class Admin_LinksController extends Zend_Controller_Action {

    public function init() {
		if(Zend_Auth::getInstance()->hasIdentity()) {
			$userInfo = Zend_Auth::getInstance()->getStorage()->read();  
  			$this->view->username = $userInfo->username;
  			$this->view->firstName = $userInfo->first_name;
  			$this->view->lastName = $userInfo->last_name;
  		}
		$this->view->headLink()->appendStylesheet('/modules/admin/styles/links.css');
	}

    public function indexAction() {
		$links = new Application_Model_LinksMapper();
		$this->view->links = $links->fetchAll();
    }
    
    public function addAction() {
    	// Use for adding as well as editing an event
    	// Editing only prepopulates the fields
		$this->_helper->layout()->disableLayout();
		$linksForm = new Admin_Form_LinksForm();
		$this->view->linksForm = $linksForm;

		// If we get an ID then we must be editing, so populate the fields
		if($this->getRequest()->getParam('id')) {
			$editEvent = new Application_Model_LinksMapper();
			$newThing = new Application_Model_Links();
			$editEvent->find($this->getRequest()->getParam('id'), $newThing);
			$it['url'] = $newThing->url;
			$it['title'] = stripslashes($newThing->title);
			$it['description'] = stripslashes($newThing->description);
			$it['image_url'] = $newThing->image_url;
			$linksForm->populate($it);
		}
		
        $this->view->confirmNewButton = $this->view->button(
			'ConfirmAddEvent', // ID and name
			'Add this link', // Display text
			array('iconClass' => 'myButtons'), // Dijit parameters
			array('class' => 'action', 'style' => 'display: none') // HTML attributes
		);
        $this->view->confirmSaveButton = $this->view->button(
			'ComfirmSaveEvent', // ID and name
			'Commit the changes', // Display text
			array('iconClass' => 'myButtons'), // Dijit parameters
			array('class' => 'action', 'style' => 'display: none') // HTML attributes
		);
        $this->view->cancelButton = $this->view->button(
			'CancelNew', //ID and name
			'Cancel', // Display text
			array('iconClass' => 'myButtons'), // Dijit parameters
			array('class' => 'cancel') // HTML attributes
		);    

		if($this->getRequest()->isPost()) {
	        $this->_helper->viewRenderer->setNoRender(true);
			$links = new Application_Model_LinksMapper();
			$allRecords = $links->fetchAll();
			$count = count($allRecords);
  			$pars = $this->getRequest()->getParams();
			$theEvent = new Application_Model_Links($pars);
			if($this->getRequest()->getParam('id')) {
				$theEvent->id = $this->getRequest()->getParam('id');
			}
			$theEvent->order = $count;
			$links->save($theEvent);
		}
		$this->view->id = $this->getRequest()->getParam('id');    
    }
    
    public function deleteAction() {
    	// We need to get the image name before removing and delete the image too
		$this->_helper->layout()->disableLayout();
		if($this->getRequest()->isPost()) {
	        $this->_helper->viewRenderer->setNoRender(true);
			//Actually delete the record
	        //$links = new Application_Model_LinksMapper();

			$links = new Application_Model_LinksMapper();
			$object = new Application_Model_Links();
			$element = $links->find($this->getRequest()->getParam('id'), $object);

	        $elements = $links->fetchAll();

	        if(file_exists('images/links/thumbs/' . $element[0]->image_url)) {
	        	unlink('images/links/thumbs/' . $element[0]->image_url);
	        }
	        $order = $element[0]->order;
	        foreach($elements as $link) {
	        	if($link->order > $order) {
	        		echo $link->order . ' > ' . $order;
	        		$link->order = $link->order - 1;
	        		$links->save($link);
	        	}
	        }
	    	echo $result = $links->delete($this->getRequest()->getParam('id'));
		}
        $this->view->deleteButton = $this->view->button(
			$this->getRequest()->getParam('id'), // ID and name
			'Delete', // Display text
			array('iconClass' => 'myButtons'), // Dijit parameters
			array('class' => 'action') // HTML attributes
		);
        $this->view->cancelButton = $this->view->button(
			'CancelDelete', //ID and name
			'Cancel', // Display text
			array('iconClass' => 'myButtons'), // Dijit parameters
			array('class' => 'cancel') // HTML attributes
		);    
		// Need the event ID in order to remove it
	    $links = new Application_Model_LinksMapper();
	    $object = new Application_Model_Links();
	    $elements = $links->find($this->getRequest()->getParam('id'), $object);
        $this->view->id = $this->getRequest()->getParam('id');
		$this->view->linkTitle = $elements;
    }
    
    public function uploadAction() {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		$fn = (isset($_SERVER['HTTP_X_FILENAME']) ? $_SERVER['HTTP_X_FILENAME'] : false);
		if($fn) {
			// AJAX call
			file_put_contents(
				'images/links/thumbs/' . $fn,
				file_get_contents('php://input')
			);
			echo $fn;
			exit();
		}
    }
    
    public function reorderAction() {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

			$editEvent = new Application_Model_LinksMapper();
			$newThing = new Application_Model_Links();
			$editEvent->find($this->getRequest()->getParam('id'), $newThing);
			$newThing->order = $this->getRequest()->getParam('order');
			$editEvent->save($newThing);

// 		$links = new Application_Model_LinksMapper();
// 		$pars = $this->getRequest()->getParams();
// 		$element = new Application_Model_Links($pars);
// 		unset($element->title);
// 		$links->save($element);
    }
}