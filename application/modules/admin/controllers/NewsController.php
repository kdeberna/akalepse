<?php

class Admin_NewsController extends Zend_Controller_Action {

    public function init() {
		$this->view->headLink()->appendStylesheet('/modules/admin/styles/forms.css');
    }

    public function indexAction() {
		$this->view->headScript()->appendFile('/modules/admin/scripts/newsActions.js');
		$newsItems = new Application_Model_NewsMapper();
		$this->view->newsItems = $newsItems->fetchAll();
	}
	
	public function addAction() {
		$request = $this->getRequest();
		$form = new Admin_Form_NewsForm();
		$form->setAction('/admin/news/add/');

		$today = date('Y-m-d');
		$now = 'T' . date('H:i:s');

		$data['creation_date'] = $today;
		$data['creation_time'] = $now;
		$form->populate($data);
		
		if($request->isPost()) {
			if ($form->isValid($request->getPost())) {
				$entry = new Application_Model_News($request->getParams());

				/**
				 * @todo Fix subform handling so the data comes out clean, without reassignin the variable below.
				 */

				$deepForm = $request->getParams();
				$entry->synopsis = $deepForm['content']['synopsisForm']['synopsis'];
				$entry->article = $deepForm['content']['articleForm']['article'];
				$entry->creation_date = $request->creation_date . ' ' . substr($request->creation_time, 1, strlen($request->creation_time));
				$mapper = new Application_Model_NewsMapper();
				$mapper->save($entry);
			}
			header("location: /admin/news/");
		}
		$this->view->form = $form;
	}
	
	public function editAction() {
		$request = $this->getRequest();
		$form = new Admin_Form_NewsForm();
		$form->setAction('/admin/news/edit/');
		
		$mapper = new Application_Model_NewsMapper();
		$entry = new Application_Model_News();
		$mapper->find($request->getParam('id'), $entry);
		
		$this->view->title = $entry->title;
		
		$date = getdate(strtotime($entry->creation_date));
		
		/**
		 * @todo Add elegance to date handling.
		 */

		if($date['mon'] < 10) {
			$month = '0' . $date['mon'];
		} else {
			$month = $date['mon'];
		}
		
		if($date['mday'] < 10) {
			$day = '0' . $date['mday'];
		} else {
			$day = $date['mday'];
		}
		
		if($date['hours'] < 10) {
			$hours = '0' . $date['hours'];
		} else {
			$hours = $date['hours'];
		}
		
		if($date['minutes'] < 10) {
			$minutes = '0' . $date['minutes'];
		} else {
			$minutes = $date['minutes'];
		}
		
		if($date['seconds'] < 10) {
			$seconds = '0' . $date['seconds'];
		} else {
			$seconds = $date['seconds'];
		}
		
		$form->addElement('hidden', 'id', array('value' => $request->getParam('id')));
		$data['creation_date'] = $date['year'] . '-' . $month . '-' . $day;
		$data['creation_time'] = 'T' . $hours . ':' . $minutes . ':' . $seconds;
		$data['status'] = $entry->status;
		$data['title'] = stripslashes($entry->title);
		$data['synopsis'] = stripslashes($entry->synopsis);
		$data['article'] = stripslashes($entry->article);
		
		$form->populate($data);
		$this->view->form = $form;
		
		if($request->isPost()) {
			if ($form->isValid($request->getPost())) {
				$entry = new Application_Model_News($request->getParams());

				/**
				 * @todo Fix subform handling so the data comes out clean, without reassignin the variable below.
				 */

				$deepForm = $request->getParams();
				$entry->synopsis = $deepForm['content']['synopsisForm']['synopsis'];
				$entry->article = $deepForm['content']['articleForm']['article'];
				$entry->creation_date = $request->creation_date . ' ' . substr($request->creation_time, 1, strlen($request->creation_time));
				$mapper = new Application_Model_NewsMapper();
				$mapper->save($entry);
			}
			header("location: /admin/news/");
		}
	}

	public function deleteAction() {
		$request = $this->getRequest();
		
		if($request->isXmlHttpRequest()){
			$this->_helper->layout()->disableLayout();
		}
		
		$mapper = new Application_Model_NewsMapper();
		$entry = new Application_Model_News();
		$mapper->find($request->getParam('id'), $entry);
		
		$this->view->title = $entry->title;
		$this->view->id = $entry->id;
		
		if($request->isPost()) {
			$mapper = new Application_Model_NewsMapper();
			$mapper->delete($request->getParam('id'));
			header("location: /admin/news/");
		}
	}

}