<?php

class Admin_Bootstrap extends Zend_Application_Module_Bootstrap {

	protected function _initViewHelpers() {

	}

	protected function _initRoutes() {
		$frontController = Zend_Controller_Front::getInstance();
		$router = $frontController->getRouter();
		$route = new Zend_Controller_Router_Route(
			'/admin/events/delete/:id',
			array(
				'module'		=> 'admin',
				'controller'	=> 'events',
				'action'		=> 'delete'
			)
		);
		$router->addRoute('getEventDeleteId', $route);
		$route2 = new Zend_Controller_Router_Route(
			'/admin/events/save/:id',
			array(
				'module'		=> 'admin',
				'controller'	=> 'events',
				'action'		=> 'save'
			)
		);
		$router->addRoute('getSaveId', $route2);
		$route2 = new Zend_Controller_Router_Route(
			'/admin/events/edit/:id',
			array(
				'module'		=> 'admin',
				'controller'	=> 'events',
				'action'		=> 'edit'
			)
		);
		$router->addRoute('getEditEvents', $route2);
		$route3 = new Zend_Controller_Router_Route(
			'/admin/links/delete/:id',
			array(
				'module'		=> 'admin',
				'controller'	=> 'links',
				'action'		=> 'delete'
			)
		);
		$router->addRoute('getLinksId', $route3);
		$route4 = new Zend_Controller_Router_Route(
			'/admin/links/add/:id',
			array(
				'module'		=> 'admin',
				'controller'	=> 'links',
				'action'		=> 'add'
			)
		);
		$router->addRoute('getLinksAddId', $route4);
		$route4 = new Zend_Controller_Router_Route(
			'/admin/links/reorder/:id/:order',
			array(
				'module'		=> 'admin',
				'controller'	=> 'links',
				'action'		=> 'reorder'
			)
		);
		$router->addRoute('getLinksReorder', $route4);
		$route = new Zend_Controller_Router_Route(
			'/admin/news/edit/:id',
			array(
				'module'		=> 'admin',
				'controller'	=> 'news',
				'action'		=> 'edit'
			)
		);
		$router->addRoute('getNewsEditId', $route);
		$route = new Zend_Controller_Router_Route(
			'/admin/news/delete/:id',
			array(
				'module'		=> 'admin',
				'controller'	=> 'news',
				'action'		=> 'delete'
			)
		);
		$router->addRoute('getNewsDeleteId', $route);
		$route = new Zend_Controller_Router_Route(
			'/admin/galleries/edit/:gallery',
			array(
				'module'		=> 'admin',
				'controller'	=> 'galleries',
				'action'		=> 'edit'
			)
		);
		$router->addRoute('getGalleryEditId', $route);
		$route = new Zend_Controller_Router_Route(
			'/admin/galleries/images/:gallery',
			array(
				'module'		=> 'admin',
				'controller'	=> 'galleries',
				'action'		=> 'images'
			)
		);
		$router->addRoute('getGalleryEditIdSilent', $route);
		$route = new Zend_Controller_Router_Route(
			'/admin/galleries/delete/:id',
			array(
				'module'		=> 'admin',
				'controller'	=> 'galleries',
				'action'		=> 'delete'
			)
		);
		$router->addRoute('getGalleryDeleteId', $route);
		$route = new Zend_Controller_Router_Route(
			'/admin/discography/edit/:id',
			array(
				'module'		=> 'admin',
				'controller'	=> 'discography',
				'action'		=> 'edit'
			)
		);
		$router->addRoute('getDiscogEditId', $route);
		$route = new Zend_Controller_Router_Route(
			'/admin/discography/delete/:id',
			array(
				'module'		=> 'admin',
				'controller'	=> 'discography',
				'action'		=> 'delete'
			)
		);
		$router->addRoute('getDiscogDeleteId', $route);
		$route = new Zend_Controller_Router_Route(
			'/admin/discography/tracks/:id',
			array(
				'module'		=> 'admin',
				'controller'	=> 'discography',
				'action'		=> 'tracks'
			)
		);
		$router->addRoute('getDiscogTracks', $route);
		$route = new Zend_Controller_Router_Route(
			'/admin/discography/progress/:id',
			array(
				'module'		=> 'admin',
				'controller'	=> 'discography',
				'action'		=> 'progress'
			)
		);
		$router->addRoute('getUploadProgress', $route);

	}
	
}