<?php

class Application_Model_DiscographyMapper {

	protected $_dbTable;

	public function setDbTable($dbTable) {
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;
		return $this;
    }
 
	public function getDbTable() {
		if (null === $this->_dbTable) {
			$this->setDbTable('Application_Model_DbTable_Discography');
		}
		return $this->_dbTable;
	}
	
	public function save(Application_Model_Discography $entry) {
		$data = array(
			'id'				=> $entry->getId(),
			'section'			=> $entry->getSection(),
 			'title'				=> $entry->getTitle(),
 			'shorttitle'		=> $entry->getShorttitle(),
 			'release_date'		=> $entry->getRelease_date(),
 			'description'		=> $entry->getDescription(),
 			'shortdesc'			=> $entry->getShortdesc(),
 			'downloadable'		=> $entry->getDownloadable(),
 			'playable'			=> $entry->getPlayable(),
 			'album_cover'		=> $entry->getAlbum_cover(),
 			'album_interior'	=> $entry->getAlbum_interior(),
 			'album_back_cover'	=> $entry->getAlbum_back_cover(),
 			'dir'				=> $entry->getDir(),
 			'zipfile'			=> $entry->getZipfile()
		);

		if (null === ($id = $entry->getId())) {
			unset($data['id']);
			return $this->getDbTable()->insert($data);
		} else {
			$this->getDbTable()->update($data, array('id = ?' => $id));
			echo 'Discography updated.';
		}
	}

	public function find($id, Application_Model_Discography $entry) {
		$result = $this->getDbTable()->find($id);
		if (0 == count($result)) {
			return;
		}
		$row = $result->current();
		$entry->setId($row->id)
			->setSection($row->section)
			->setTitle($row->title)
			->setShorttitle($row->shorttitle)
			->setRelease_date($row->release_date)
			->setDescription($row->description)
			->setShortdesc($row->shortdesc)
			->setDownloadable($row->downloadable)
			->setPlayable($row->playable)
			->setAlbum_cover($row->album_cover)
			->setAlbum_interior($row->album_interior)
			->setAlbum_back_cover($row->album_back_cover)
			->setDir($row->dir)
			->setZipfile($row->zipfile);
	}

	public function fetchAll() {
		$resultSet = $this->getDbTable()->fetchAll();
		$entries = array();
 		foreach($resultSet as $row) {
			$entry = new Application_Model_Discography();
 			$entry->setId($row->id)
				->setSection($row->section)
				->setTitle($row->title)
				->setShorttitle($row->shorttitle)
				->setRelease_date($row->release_date)
				->setDescription($row->description)
				->setShortdesc($row->shortdesc)
				->setDownloadable($row->downloadable)
				->setPlayable($row->playable)
				->setAlbum_cover($row->album_cover)
				->setAlbum_interior($row->album_interior)
				->setAlbum_back_cover($row->album_back_cover)
				->setDir($row->dir)
				->setZipfile($row->zipfile);
 			$entries[] = $entry;
 		}
 		return $entries;
	}
	
	public function delete($id) {
		$this->getDbTable()->delete('id = ' . $id);
		echo 'Discography deleted.';
	}
}