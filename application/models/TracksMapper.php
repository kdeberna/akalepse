<?php

class Application_Model_TracksMapper {

	protected $_dbTable;

	public function setDbTable($dbTable) {
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;
		return $this;
    }
 
	public function getDbTable() {
		if (null === $this->_dbTable) {
			$this->setDbTable('Application_Model_DbTable_Tracks');
		}
		return $this->_dbTable;
	}
	
	public function save(Application_Model_Tracks $entry) {
		$data = array(
			'id'		=> $entry->getId(),
			'disco_id'	=> $entry->getDisco_id(),
 			'track_num'	=> $entry->getTrack_num(),
 			'title'		=> $entry->getTitle(),
 			'artist'	=> $entry->getArtist(),
 			'mp3'		=> $entry->getMp3()
		);

		if (null === ($id = $entry->getId())) {
			unset($data['id']);
			$this->getDbTable()->insert($data);
			echo 'New track added: ' . $data['title'];
		} else {
			$this->getDbTable()->update($data, array('id = ?' => $id));
			echo 'Track updated.';
		}
	}

	public function find($id, Application_Model_Tracks $entry) {
		$result = $this->getDbTable()->find($id);
		if (0 == count($result)) {
			return;
		}
		$row = $result->current();
		$entry->setId($row->id)
			->setDisco_id($row->disco_id)
			->setTrack_num($row->track_num)
			->setTitle($row->title)
			->setArtist($row->artist)
			->setMp3($row->mp3);
	}

	public function fetchAll() {
		$resultSet = $this->getDbTable()->fetchAll();
		$entries = array();
 		foreach($resultSet as $row) {
			$entry = new Application_Model_Tracks();
 			$entry->setId($row->id)
				->setDisco_id($row->disco_id)
				->setTrack_num($row->track_num)
				->setTitle($row->title)
				->setArtist($row->artist)
				->setMp3($row->mp3);
 			$entries[] = $entry;
 		}
 		return $entries;
	}

	public function fetchByAlbum($id) {
		$resultSet = $this->getDbTable()->fetchAll('disco_id = ' . "$id", 'track_num ASC');
		$entries = array();
 		foreach($resultSet as $row) {
			$entry = new Application_Model_Tracks();
 			$entry->setId($row->id)
				->setDisco_id($row->disco_id)
				->setTrack_num($row->track_num)
				->setTitle($row->title)
				->setArtist($row->artist)
				->setMp3($row->mp3);
 			$entries[] = $entry;
 		}
 		return $entries;
	}

	public function delete($id) {
		$this->getDbTable()->delete('id = ' . $id);
		echo 'Track deleted.';
	}
}