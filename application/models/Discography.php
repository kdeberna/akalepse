<?php

class Application_Model_Discography {

	protected $_id = null;
	protected $_section = '';
	protected $_title = '';
	protected $_shorttitle = '';
	protected $_release_date = '';
	protected $_description = '';
	protected $_shortdesc = '';
	protected $_downloadable = '0';
	protected $_playable = '0';
	protected $_album_cover = '';
	protected $_album_interior = '';
	protected $_album_back_cover = '';
	protected $_dir = '';
	protected $_zipfile = '';

	public function __construct(array $options = null) {
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}

	public function __set($name, $value) {
		$method = 'set' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Discography property');
		}
		$this->$method($value);
	}

	public function __get($name) {
		$method = 'get' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Discography property');
		}
		return $this->$method();
	}

	public function setOptions(array $options) {
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}

	public function setId($id) {
		$this->_id = (int) $id;
		return $this;
	}

	public function getId() {
		return $this->_id;
	}

	public function setSection($value) {
		$this->_section = (string) $value;
		return $this;
	}

	public function getSection() {
		return $this->_section;
	}
 
	public function setTitle($value) {
		$this->_title = (string) $value;
		return $this;
	}

	public function getTitle() {
		return $this->_title;
	}

	public function setShorttitle($value) {
		$this->_shorttitle = (string) $value;
		return $this;
	}

	public function getShorttitle() {
		return $this->_shorttitle;
	}

	public function setRelease_date($value) {
		$this->_release_date = $value;
		return $this;
	}

	public function getRelease_date() {
		return $this->_release_date;
	}

	public function setDescription($value) {
		$this->_description = (string) $value;
		return $this;
	}

	public function getDescription() {
		return $this->_description;
	}

	public function setShortdesc($value) {
		$this->_shortdesc = (string) $value;
		return $this;
	}

	public function getShortdesc() {
		return $this->_shortdesc;
	}

	public function setDownloadable($value) {
		$this->_downloadable = (int) $value;
		return $this;
	}

	public function getDownloadable() {
		return $this->_downloadable;
	}

	public function setPlayable($value) {
		$this->_playable = (int) $value;
		return $this;
	}

	public function getPlayable() {
		return $this->_playable;
	}

	public function setAlbum_cover($value) {
		$this->_album_cover = (string) $value;
		return $this;
	}

	public function getAlbum_cover() {
		return $this->_album_cover;
	}

	public function setAlbum_interior($value) {
		$this->_album_interior = (string) $value;
		return $this;
	}

	public function getAlbum_interior() {
		return $this->_album_interior;
	}

	public function setAlbum_back_cover($value) {
		$this->_album_back_cover = (string) $value;
		return $this;
	}

	public function getAlbum_back_cover() {
		return $this->_album_back_cover;
	}

	public function setDir($value) {
		$this->_dir = (string) $value;
		return $this;
	}

	public function getDir() {
		return $this->_dir;
	}

	public function setZipfile($value) {
		$this->_zipfile = (string) $value;
		return $this;
	}

	public function getZipfile() {
		return $this->_zipfile;
	}
}