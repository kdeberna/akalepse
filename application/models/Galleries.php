<?php

class Application_Model_Galleries {

	protected $_id = null;
	protected $_order = '';
	protected $_visibility = '';
	protected $_dir = '';
	protected $_title = '';
	protected $_description = '';
	protected $_timestamp = '';

	public function __construct(array $options = null) {
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}

	public function __set($name, $value) {
		$method = 'set' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Gallery property');
		}
		$this->$method($value);
	}

	public function __get($name) {
		$method = 'get' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Gallery property');
		}
		return $this->$method();
	}

	public function setOptions(array $options) {
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}

	public function setId($value) {
		$this->_id = (int) $value;
		return $this;
	}

	public function getId() {
		return $this->_id;
	}

	public function setOrder($value) {
		$this->_order = (int) $value;
		return $this;
	}

	public function getOrder() {
		return $this->_order;
	}
 
	public function setVisibility($value) {
		$this->_visibility = (int) $value;
		return $this;
	}

	public function getVisibility() {
		return $this->_visibility;
	}

	public function setDir($value) {
		$this->_dir = (string) $value;
		return $this;
	}

	public function getDir() {
// 		$dir = str_replace(" ", "-", strtolower($this->_title));
// 		$dir = preg_replace('/[^\w-]+/', "", $dir);
		return $this->_dir;
	}

	public function setTitle($value) {
		$this->_title = (string) $value;
		return $this;
	}

	public function getTitle() {
		return $this->_title;
	}

	public function setDescription($value) {
		$this->_description = (string) $value;
		return $this;
	}

	public function getDescription() {
		return $this->_description;
	}

	public function setTimestamp($value) {
		$this->_timestamp = $value;
		return $this;
	}

	public function getTimestamp() {
		return $this->_timestamp;
	}

}