<?php

class Application_Model_Events {

	protected $_id = null;
	protected $_date = '';
	protected $_start = '';
	protected $_end = '';
	protected $_title = '';
	protected $_rsvp = '';
	protected $_repeat = '';
	protected $_flyerfront = '';
	protected $_flyerback = '';
	protected $_description = '';
	protected $_who = '';
	protected $_venue = '';
	protected $_address = '';
	protected $_cover = '';
	protected $_synopsis = '';

	public function __construct(array $options = null) {
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}

	public function __set($name, $value) {
		$method = 'set' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Events property');
		}
		$this->$method($value);
	}

	public function __get($name) {
		$method = 'get' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Events property');
		}
		return $this->$method();
	}

	public function setOptions(array $options) {
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}

	public function setId($id) {
		$this->_id = (int) $id;
		return $this;
	}

	public function getId() {
		return $this->_id;
	}

	public function setDate($date) {
		$this->_date = (string) $date;
		return $this;
	}

	public function getDate() {
		return $this->_date;
	}
 
	public function setStart($start) {
		$this->_start = (string) $start;
		return $this;
	}

	public function getStart() {
		return $this->_start;
	}

	public function setEnd($end) {
		$this->_end = (string) $end;
		return $this;
	}

	public function getEnd() {
		return $this->_end;
	}

	public function setTitle($title) {
		$this->_title = (string) $title;
		return $this;
	}

	public function getTitle() {
		return $this->_title;
	}

	public function setRsvp($rsvp) {
		$this->_rsvp = (string) $rsvp;
		return $this;
	}

	public function getRsvp() {
		return $this->_rsvp;
	}

	public function setRepeat($repeat) {
		$this->_repeat = (string) $repeat;
		return $this;
	}

	public function getRepeat() {
		return $this->_repeat;
	}

	public function setFlyerfront($flyerfront) {
		$this->_flyerfront = (string) $flyerfront;
		return $this;
	}

	public function getFlyerfront() {
		return $this->_flyerfront;
	}

	public function setFlyerback($flyerback) {
		$this->_flyerback = (string) $flyerback;
		return $this;
	}

	public function getFlyerback() {
		return $this->_flyerback;
	}

	public function setDescription($description) {
		$this->_description = (string) $description;
		return $this;
	}

	public function getDescription() {
		return $this->_description;
	}

	public function setWho($who) {
		$this->_who = (string) $who;
		return $this;
	}

	public function getWho() {
		return $this->_who;
	}

	public function setVenue($venue) {
		$this->_venue = (string) $venue;
		return $this;
	}

	public function getVenue() {
		return $this->_venue;
	}

	public function setAddress($address) {
		$this->_address = (string) $address;
		return $this;
	}

	public function getAddress() {
		return $this->_address;
	}

	public function setCover($cover) {
		$this->_cover = (string) $cover;
		return $this;
	}

	public function getCover() {
		return $this->_cover;
	}

	public function setSynopsis($synopsis) {
		$this->_synopsis = (string) $synopsis;
		return $this;
	}

	public function getSynopsis() {
		return $this->_synopsis;
	}
}