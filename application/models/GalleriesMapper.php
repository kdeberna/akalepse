<?php

class Application_Model_GalleriesMapper {

	protected $_dbTable;

	public function setDbTable($dbTable) {
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;
		return $this;
    }
 
	public function getDbTable() {
		if (null === $this->_dbTable) {
			$this->setDbTable('Application_Model_DbTable_Galleries');
		}
		return $this->_dbTable;
	}
	
	public function save(Application_Model_Galleries $gallery) {
		$data = array(
			'id'			=> $gallery->getId(),
			'order'			=> $gallery->getOrder(),
 			'visibility'	=> $gallery->getVisibility(),
 			'dir'			=> $gallery->getDir(),
 			'title'			=> $gallery->getTitle(),
			'description'	=> $gallery->getDescription(),
			'timestamp'		=> $gallery->getTimestamp(),
		);

		if (null === ($id = $gallery->getId())) {
			unset($data['id']);
			if(isset($data['filename'])){
				$files = $data['filename'];
				unset($data['filename']);
			}
			unset($data['LinkImage']);
			// Return the ID of the inserted row
			return $this->getDbTable()->insert($data);
		} else {
			$this->getDbTable()->update($data, array('id = ?' => $id));
			// If there are image updates and/or additions, insert each into galleries_images table
			echo 'Gallery updated.';
		}
	}

	public function find($id, Application_Model_Galleries $gallery) {
		$result = $this->getDbTable()->find($id);
		if (0 == count($result)) {
			return;
		}
		$row = $result->current();
		$gallery->setId($row->id)
		->setOrder($row->order)
		->setVisibility($row->visibility)
		->setDir($row->dir)
		->setTitle($row->title)
		->setDescription($row->description)
		->setTimestamp($row->timestamp);
	}

	public function fetchAll() {
		$resultSet = $this->getDbTable()->fetchAll(null, 'order DESC');
		$entries = array();
		foreach ($resultSet as $row) {
			$entry = new Application_Model_Galleries();
			$entry->setId($row->id)
			->setOrder($row->order)
			->setVisibility($row->visibility)
			->setDir($row->dir)
			->setTitle($row->title)
			->setDescription($row->description)
			->setTimestamp($row->timestamp);
			$entries[] = $entry;
		}
		return $entries;
	}

	public function fetchByDir($dir) {
		$resultSet = $this->getDbTable()->fetchAll("dir = " . "'$dir'");
		$entries = array();
		foreach ($resultSet as $row) {
			$entry = new Application_Model_Galleries();
			$entry->setId($row->id)
			->setOrder($row->order)
			->setVisibility($row->visibility)
			->setDir($row->dir)
			->setTitle($row->title)
			->setDescription($row->description)
			->setTimestamp($row->timestamp);
			$entries[] = $entry;
		}
		return $entries;
	}
	
	
	public function delete($id) {
		$this->getDbTable()->delete('id = ' . $id);
		echo 'Gallery deleted.';
	}
}