<?php

class Application_Model_NewsMapper {

	protected $_dbTable;

	public function setDbTable($dbTable) {
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;
		return $this;
    }
 
	public function getDbTable() {
		if (null === $this->_dbTable) {
			$this->setDbTable('Application_Model_DbTable_News');
		}
		return $this->_dbTable;
	}
	
	public function save(Application_Model_News $entry) {
		$data = array(
			'id'			=> $entry->getId(),
			'status'		=> $entry->getStatus(),
 			'title'			=> $entry->getTitle(),
 			'synopsis'		=> $entry->getSynopsis(),
 			'article'		=> $entry->getArticle(),
 			'creation_date'	=> $entry->getCreation_date()
		);

		if (null === ($id = $entry->getId())) {
			unset($data['id']);
			$this->getDbTable()->insert($data);
			echo 'New news item added: ' . $data['title'];
		} else {
			$this->getDbTable()->update($data, array('id = ?' => $id));
			echo 'News updated.';
		}
	}

	public function find($id, Application_Model_News $entry) {
		$result = $this->getDbTable()->find($id);
		if (0 == count($result)) {
			return;
		}
		$row = $result->current();
		$entry->setId($row->id)
			->setStatus($row->status)
			->setTitle($row->title)
			->setSynopsis($row->synopsis)
			->setArticle($row->article)
			->setCreation_date($row->creation_date);
	}

	public function fetchAll() {
		$resultSet = $this->getDbTable()->fetchAll(null, 'creation_date DESC');
		$entries = array();

		$commentCount = new Application_Model_CommentsMapper();
        $commentCount = count($commentCount->fetchAll());

 		foreach($resultSet as $row) {
			$entry = new Application_Model_News();
			$entry->setId($row->id)
				->setStatus($row->status)
				->setTitle($row->title)
				->setSynopsis($row->synopsis)
				->setArticle($row->article)
				->setCreation_date($row->creation_date);
 			$entries[] = $entry;
 		}
 		return $entries;
	}
	
	public function fetchVisible() {
		$resultSet = $this->getDbTable()->fetchAll('status = "published" AND creation_date < NOW()', 'creation_date DESC');
		$entries = array();

		$commentCount = new Application_Model_CommentsMapper();
        $commentCount = count($commentCount->fetchAll());

 		foreach($resultSet as $row) {
			$entry = new Application_Model_News();
			$entry->setId($row->id)
				->setStatus($row->status)
				->setTitle($row->title)
				->setSynopsis($row->synopsis)
				->setArticle($row->article)
				->setCreation_date($row->creation_date);
 			$entries[] = $entry;
 		}
 		return $entries;
	}
	
	public function delete($id) {
		$this->getDbTable()->delete('id = ' . $id);
		echo 'News item deleted.';
	}
}