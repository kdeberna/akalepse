<?php

class Application_Model_Tracks {

	protected $_id = null;
	protected $_disco_id = '';
	protected $_track_num = '';
	protected $_title = '';
	protected $_artist = '';
	protected $_mp3 = '';

	public function __construct(array $options = null) {
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}

	public function __set($name, $value) {
		$method = 'set' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Tracks property');
		}
		$this->$method($value);
	}

	public function __get($name) {
		$method = 'get' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Tracks property');
		}
		return $this->$method();
	}

	public function setOptions(array $options) {
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}

	public function setId($id) {
		$this->_id = $id;
		return $this;
	}

	public function getId() {
		return $this->_id;
	}

	public function setDisco_id($value) {
		$this->_disco_id = $value;
		return $this;
	}

	public function getDisco_id() {
		return $this->_disco_id;
	}
 
	public function setTrack_num($value) {
		$this->_track_num = $value;
		return $this;
	}

	public function getTrack_num() {
		return $this->_track_num;
	}

	public function setTitle($value) {
		$this->_title = (string) $value;
		return $this;
	}

	public function getTitle() {
		return $this->_title;
	}

	public function setArtist($value) {
		$this->_artist = (string) $value;
		return $this;
	}

	public function getArtist() {
		return $this->_artist;
	}

	public function setMp3($value) {
		$this->_mp3 = (string) $value;
		return $this;
	}

	public function getMp3() {
		return $this->_mp3;
	}

}