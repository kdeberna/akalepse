<?php

class Application_Model_Links {

	protected $_id			= null;
	protected $_order		= '';
	protected $_url			= '';
	protected $_title		= '';
	protected $_description	= '';
	protected $_image_url	= '';
	protected $_tstamp		= '';

	public function __construct(array $options = null) {
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}

	public function __set($name, $value) {
		$method = 'set' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Links property');
		}
		$this->$method($value);
	}

	public function __get($name) {
		$method = 'get' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Links property');
		}
		return $this->$method();
	}

	public function setOptions(array $options) {
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}

	public function setId($id) {
		$this->_id = (int) $id;
		return $this;
	}

	public function getId() {
		return $this->_id;
	}

	public function setOrder($value) {
		$this->_order = (string) $value;
		return $this;
	}

	public function getOrder() {
		return $this->_order;
	}
 
	public function setUrl($value) {
		$this->_url = (string) $value;
		return $this;
	}

	public function getUrl() {
		$url = str_replace('http://', '', $this->_url);
		return $url;
	}

	public function setTitle($value) {
		$this->_title = (string) $value;
		return $this;
	}

	public function getTitle() {
		return $this->_title;
	}

	public function setDescription($value) {
		$this->_description = (string) $value;
		return $this;
	}

	public function getDescription() {
		return $this->_description;
	}

	public function setImage_url($value) {
		$this->_image_url = (string) $value;
		return $this;
	}

	public function getImage_url() {
		return $this->_image_url;
	}

	public function setTstamp($value) {
		$this->_tstamp = $value;
		return $this;
	}

	public function getTstamp() {
		return $this->_tstamp;
	}

}