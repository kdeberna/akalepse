<?php

class Application_Model_News {

	protected $_id = null;
	protected $_status = '';
	protected $_title = '';
	protected $_synopsis = '';
	protected $_article = '';
	protected $_creation_date = '';

	public function __construct(array $options = null) {
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}

	public function __set($name, $value) {
		$method = 'set' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid News property');
		}
		$this->$method($value);
	}

	public function __get($name) {
		$method = 'get' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid News property');
		}
		return $this->$method();
	}

	public function setOptions(array $options) {
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}

	public function setId($id) {
		$this->_id = (int) $id;
		return $this;
	}

	public function getId() {
		return $this->_id;
	}

	public function setStatus($value) {
		$this->_status = (string) $value;
		return $this;
	}

	public function getStatus() {
		return $this->_status;
	}
 
	public function setTitle($value) {
		$this->_title = (string) $value;
		return $this;
	}

	public function getTitle() {
		return $this->_title;
	}

	public function setSynopsis($value) {
		$this->_synopsis = (string) $value;
		return $this;
	}

	public function getSynopsis() {
		return $this->_synopsis;
	}

	public function setArticle($value) {
		$this->_article = (string) $value;
		return $this;
	}

	public function getArticle() {
		return $this->_article;
	}

	public function setCreation_date($value) {
		$this->_creation_date = $value;
		return $this;
	}

	public function getCreation_date() {
		return $this->_creation_date;
	}

}