<?php

class Application_Model_Images {

	public $_id = null;
	public $_album_id = '';
	public $_order = '';
	public $_cover = '';
	public $_filename = '';
	public $_caption = '';
	public $_credit = '';

	public function __construct(array $options = null) {
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}

	public function __set($name, $value) {
		$method = 'set' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Image property');
		}
		$this->$method($value);
	}

	public function __get($name) {
		$method = 'get' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Image property');
		}
		return $this->$method();
	}

	public function setOptions(array $options) {
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}

	public function setId($value) {
		$this->_id = (int) $value;
		return $this;
	}

	public function getId() {
		return $this->_id;
	}

	public function setAlbum_id($value) {
		$this->_album_id = (int) $value;
		return $this;
	}

	public function getAlbum_id() {
		return $this->_album_id;
	}

	public function setOrder($value) {
		$this->_order = (int) $value;
		return $this;
	}

	public function getOrder() {
		return $this->_order;
	}
 
	public function setCover($value) {
		$this->_cover = (int) $value;
		return $this;
	}

	public function getCover() {
		return $this->_cover;
	}

	public function setFilename($value) {
		$this->_filename = (string) $value;
		return $this;
	}

	public function getFilename() {
		return $this->_filename;
	}

	public function setCaption($value) {
		$this->_caption = (string) $value;
		return $this;
	}

	public function getCaption() {
		return $this->_caption;
	}

	public function setCredit($value) {
		$this->_credit = (string) $value;
		return $this;
	}

	public function getCredit() {
		return $this->_credit;
	}

}