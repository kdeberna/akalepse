<?php

class Application_Model_CoversMapper {

	protected $_dbTable;

	public function setDbTable($dbTable) {
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;
		return $this;
    }
 
	public function getDbTable() {
		if (null === $this->_dbTable) {
			$this->setDbTable('Application_Model_DbTable_Covers');
		}
		return $this->_dbTable;
	}
	
	public function save(Application_Model_Covers $entry) {
		$data = array(
			'id'		=> $entry->getId(),
			'disco_id'	=> $entry->getDisco_id(),
 			'order'		=> $entry->getOrder(),
 			'image_url'	=> $entry->getImage_url()
		);

		if (null === ($id = $entry->getId())) {
			unset($data['id']);
			$this->getDbTable()->insert($data);
			echo 'New cover image added: ' . $data['title'];
		} else {
			$this->getDbTable()->update($data, array('id = ?' => $id));
			echo 'Cover image updated.';
		}
	}

	public function find($id, Application_Model_Covers $entry) {
		$result = $this->getDbTable()->find($id);
		if (0 == count($result)) {
			return;
		}
		$row = $result->current();
		$entry->setId($row->id)
			->setDisco_id($row->disco_id)
			->setOrder($row->order)
			->setImage_url($row->image_url);
	}

	public function fetchAll() {
		$resultSet = $this->getDbTable()->fetchAll();
		$entries = array();
 		foreach($resultSet as $row) {
			$entry = new Application_Model_Covers();
 			$entry->setId($row->id)
				->setDisco_id($row->disco_id)
				->setOrder($row->order)
				->setImage_url($row->image_url);
 			$entries[] = $entry;
 		}
 		return $entries;
	}

	public function fetchByAlbum($id) {
		$resultSet = $this->getDbTable()->fetchAll('disco_id = ' . "$id");
		$entries = array();
 		foreach($resultSet as $row) {
			$entry = new Application_Model_Covers();
 			$entry->setId($row->id)
				->setDisco_id($row->disco_id)
				->setOrder($row->order)
				->setImage_url($row->image_url);
 			$entries[] = $entry;
 		}
 		return $entries;
	}

	public function delete($id) {
		$this->getDbTable()->delete('id = ' . $id);
		echo 'Cover image deleted.';
	}
}