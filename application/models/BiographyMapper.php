<?php

class Application_Model_BiographyMapper {

	protected $_dbTable;

	public function setDbTable($dbTable) {
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;
		return $this;
    }
 
	public function getDbTable() {
		if (null === $this->_dbTable) {
			$this->setDbTable('Application_Model_DbTable_Biography');
		}
		return $this->_dbTable;
	}
	
	public function save(Application_Model_Biography $entry) {
		$data = array(
			'id'		=> $entry->getId(),
			'content'	=> $entry->getContent()
		);

		if (null === ($id = $entry->getId())) {
			unset($data['id']);
			$this->getDbTable()->insert($data);
			echo 'New biography added: ' . $data['title'];
		} else {
			$this->getDbTable()->update($data, array('id = ?' => $id));
			echo 'Biography updated.';
		}
	}

	public function find($id, Application_Model_Biography $entry) {
		$result = $this->getDbTable()->find($id);
		if (0 == count($result)) {
			return;
		}
		$row = $result->current();
		$entry->setId($row->id)
			->seetContent($row->content);
	}

	public function fetchAll() {
		$resultSet = $this->getDbTable()->fetchAll();
		$entries = array();
 		foreach($resultSet as $row) {
			$entry = new Application_Model_Biography();
 			$entry->setId($row->id)
 				->setContent($row->content);
 			$entries[] = $entry;
 		}
 		return $entries;
	}
	
	public function delete($id) {
		$this->getDbTable()->delete('id = ' . $id);
		echo 'Biography deleted.';
	}

}