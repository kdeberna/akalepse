<?php

class Application_Model_EventsMapper {

	protected $_dbTable;

	public function setDbTable($dbTable) {
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;
		return $this;
    }
 
	public function getDbTable() {
		if (null === $this->_dbTable) {
			$this->setDbTable('Application_Model_DbTable_Events');
		}
		return $this->_dbTable;
	}
	
	public function save(Application_Model_Events $events) {
		$data = array(
			'id'			=> $events->getId(),
			'date'			=> $events->getDate(),
 			'start'			=> $events->getStart(),
 			'end'			=> $events->getEnd(),
 			'title'			=> $events->getTitle(),
			'rsvp'			=> $events->getRsvp(),
			'repeat'		=> $events->getRepeat(),
			'flyerfront'	=> $events->getFlyerfront(),
			'flyerback'		=> $events->getFlyerback(),
			'description'	=> $events->getDescription(),
			'who'			=> $events->getWho(),
			'venue'			=> $events->getVenue(),
			'address'		=> $events->getAddress(),
			'cover'			=> $events->getCover(),
			'synopsis'		=> $events->getSynopsis()
		);

		if (null === ($id = $events->getId())) {
			unset($data['id']);
			$this->getDbTable()->insert($data);
			echo 'New event added: ' . $data['title'];
		} else {
			$this->getDbTable()->update($data, array('id = ?' => $id));
			echo 'Event updated.';
		}
	}

	public function find($id, Application_Model_Events $events) {
		$result = $this->getDbTable()->find($id);
		if (0 == count($result)) {
			return;
		}
		$row = $result->current();
		$events->setId($row->id)
			->setDate($row->date)
			->setStart($row->start)
			->setEnd($row->end)
			->setTitle($row->title)
			->setRsvp($row->rsvp)
			->setRepeat($row->repeat)
			->setFlyerfront($row->flyerfront)
			->setFlyerback($row->flyerback)
			->setDescription($row->description)
			->setWho($row->who)
			->setVenue($row->venue)
			->setAddress($row->address)
			->setCover($row->cover)
			->setSynopsis($row->synopsis);
	}

	public function fetchAll() {
		$resultSet = $this->getDbTable()->fetchAll();
		$entries = array();
		foreach ($resultSet as $row) {
			$entry = new Application_Model_Events();
			$entry->setId($row->id)
				->setDate($row->date)
				->setStart($row->start)
				->setEnd($row->end)
				->setTitle($row->title)
				->setRsvp($row->rsvp)
				->setRepeat($row->repeat)
				->setFlyerfront($row->flyerfront)
				->setFlyerback($row->flyerback)
				->setDescription($row->description)
				->setWho($row->who)
				->setVenue($row->venue)
				->setAddress($row->address)
				->setCover($row->cover)
				->setSynopsis($row->synopsis);
			$entries[] = $entry;
		}
		return $entries;
	}
	
	public function fetchNext() {
		$next = $this->getDbTable()->select()->where('date >= CURDATE()')->order('date ASC');
		$resultSet = $this->getDbTable()->fetchAll($next);
		$entries = array();
		foreach ($resultSet as $row) {
			$entry = new Application_Model_Events();
			$entry->setId($row->id)
				->setDate($row->date)
				->setStart($row->start)
				->setEnd($row->end)
				->setTitle($row->title)
				->setRsvp($row->rsvp)
				->setRepeat($row->repeat)
				->setFlyerfront($row->flyerfront)
				->setFlyerback($row->flyerback)
				->setDescription($row->description)
				->setWho($row->who)
				->setVenue($row->venue)
				->setAddress($row->address)
				->setCover($row->cover)
				->setSynopsis($row->synopsis);
			$entries[] = $entry;
		}
		return $entries;
	}
	
	public function fetchByMonth($month) {
		// Get all in the month, plus weekly repeats, plus monthly repeats
		$byMonth = $this->getDbTable()->select()->where('MONTH(date) = "' . $month . '" AND CURDATE() <= date')->order('date');
		$resultSet = $this->getDbTable()->fetchAll($byMonth);
		$entries = array();
		$today = new Zend_Date();
		$daysInMonth = $today->get(Zend_Date::MONTH_DAYS);
		foreach ($resultSet as $row) {
			$entry = new Application_Model_Events();
			$entry->setId($row->id)
				->setDate($row->date)
				->setStart($row->start)
				->setEnd($row->end)
				->setTitle($row->title)
				->setRsvp($row->rsvp)
				->setRepeat($row->repeat)
				->setFlyerfront($row->flyerfront)
				->setFlyerback($row->flyerback)
				->setDescription($row->description)
				->setWho($row->who)
				->setVenue($row->venue)
				->setAddress($row->address)
				->setCover($row->cover)
				->setSynopsis($row->synopsis);
			$entries[] = $entry;
		}
		return $entries;
	}
	
	public function delete($id) {
		$this->getDbTable()->delete('id = ' . $id);
		echo 'Event deleted.';
	}
}