<?php

class Application_Model_HomepageMapper {

	protected $_dbTable;

	public function setDbTable($dbTable) {
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;
		return $this;
    }
 
	public function getDbTable() {
		if (null === $this->_dbTable) {
			$this->setDbTable('Application_Model_DbTable_Homepage');
		}
		return $this->_dbTable;
	}
	
	public function save(Application_Model_Homepage $homepage) {
		$data = array(
			'id'			=> $homepage->getId(),
			'order'			=> $homepage->getOrder(),
 			'image_url'		=> $homepage->getImage_url(),
 			'caption'		=> $homepage->getCaption(),
		);

		if (null === ($id = $homepage->getId())) {
			unset($data['id']);
			echo $this->getDbTable()->insert($data);
			// echo 'New homepage added: ' . $data['title'];
		} else {
			$this->getDbTable()->update($data, array('id = ?' => $id));
			echo 'Homepage updated.';
		}
	}

	public function find($id, Application_Model_Homepage $homepage) {
		$result = $this->getDbTable()->find($id);
		if (0 == count($result)) {
			return;
		}
		$row = $result->current();
		$homepage->setId($row->id)
			->setOrder($row->order)
			->setImage_url($row->image_url)
			->setCaption($row->caption);
	}

	public function fetchAll() {
		$resultSet = $this->getDbTable()->fetchAll(null, 'order ASC');
		$entries = array();
 		foreach ($resultSet as $row) {
			$entry = new Application_Model_Homepage();
 			$entry->setId($row->id)
				->setOrder($row->order)
				->setImage_url($row->image_url)
				->setCaption($row->caption);
 			$entries[] = $entry;
 		}
 		return $entries;
	}
	
	public function delete($id) {
		$this->getDbTable()->delete('id = ' . $id);
		echo 'Homepage deleted.';
	}
}