<?php

class Application_Model_CommentsMapper {

	protected $_dbTable;

	public function setDbTable($dbTable) {
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;
		return $this;
    }
 
	public function getDbTable() {
		if (null === $this->_dbTable) {
			$this->setDbTable('Application_Model_DbTable_Comments');
		}
		return $this->_dbTable;
	}
	
	public function save(Application_Model_Commments $entry) {
		$data = array(
			'id'			=> $entry->getId(),
			'news_id'		=> $entry->getNews_id(),
 			'ip_address'	=> $entry->getIp_address(),
 			'name'			=> $entry->getName(),
 			'email'			=> $entry->getEmail(),
 			'comment'		=> $entry->getComment(),
 			'tstamp'		=> $entry->getTstamp()
		);

		if (null === ($id = $entry->getId())) {
			unset($data['id']);
			$this->getDbTable()->insert($data);
			echo 'New comment item added: ' . $data['comment'];
		} else {
			$this->getDbTable()->update($data, array('id = ?' => $id));
			echo 'Comments updated.';
		}
	}

	public function find($id, Application_Model_Comments $entry) {
		$result = $this->getDbTable()->find($id);
		if (0 == count($result)) {
			return;
		}
		$row = $result->current();
		$entry->setId($row->id)
			->setNews_id($row->news_id)
			->setIp_address($row->ip_address)
			->setName($row->name)
			->setEmail($row->email)
			->setComment($row->comment)
			->setTstamp($row->tstamp);
	}

	public function fetchAll() {
		$resultSet = $this->getDbTable()->fetchAll();
		$entries = array();
 		foreach($resultSet as $row) {
			$entry = new Application_Model_Comments();
			$entry->setId($row->id)
				->setNews_id($row->news_id)
				->setIp_address($row->ip_address)
				->setName($row->name)
				->setEmail($row->email)
				->setComment($row->comment)
				->setTstamp($row->tstamp);
 			$entries[] = $entry;
 		}
 		return $entries;
	}
	
	public function fetchById($id) {
		$resultSet = $this->getDbTable()->fetchAll('news_id = ' . "$id");
		$entries = array();
 		foreach($resultSet as $row) {
			$entry = new Application_Model_Comments();
			$entry->setId($row->id)
				->setNews_id($row->news_id)
				->setIp_address($row->ip_address)
				->setName($row->name)
				->setEmail($row->email)
				->setComment($row->comment)
				->setTstamp($row->tstamp);
 			$entries[] = $entry;
 		}
 		return $entries;
	}
	
	public function delete($id) {
		$this->getDbTable()->delete('id = ' . $id);
		echo 'Comments item deleted.';
	}
}