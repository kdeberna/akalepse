<?php

class Application_Model_Homepage {

	public $_id = null;
	public $_order = '';
	public $_image_url = '';
	public $_caption = '';

	public function __construct(array $options = null) {
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}

	public function __set($name, $value) {
		$method = 'set' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Hompeage property');
		}
		$this->$method($value);
	}

	public function __get($name) {
		$method = 'get' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Homepage property');
		}
		return $this->$method();
	}

	public function setOptions(array $options) {
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}

	public function setId($id) {
		$this->_id = (int) $id;
		return $this;
	}

	public function getId() {
		return $this->_id;
	}

	public function setOrder($order) {
		$this->_order = (int) $order;
		return $this;
	}

	public function getOrder() {
		return $this->_order;
	}
 
	public function setImage_url($url) {
		$this->_image_url = (string) $url;
		return $this;
	}

	public function getImage_url() {
		return $this->_image_url;
	}

	public function setCaption($caption) {
		$this->_caption = (string) $caption;
		return $this;
	}

	public function getCaption() {
		return $this->_caption;
	}

}