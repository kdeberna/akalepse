<?php

class Application_Model_Covers {

	protected $_id = null;
	protected $_disco_id = '';
	protected $_order = '';
	protected $_image_url = '';

	public function __construct(array $options = null) {
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}

	public function __set($name, $value) {
		$method = 'set' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Covers property');
		}
		$this->$method($value);
	}

	public function __get($name) {
		$method = 'get' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Covers property');
		}
		return $this->$method();
	}

	public function setOptions(array $options) {
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}

	public function setId($id) {
		$this->_id = $id;
		return $this;
	}

	public function getId() {
		return $this->_id;
	}

	public function setDisco_id($value) {
		$this->_disco_id = $value;
		return $this;
	}

	public function getDisco_id() {
		return $this->_disco_id;
	}
 
	public function setOrder($value) {
		$this->_order = $value;
		return $this;
	}

	public function getOrder() {
		return $this->_order;
	}

	public function setImage_url($value) {
		$this->_image_url = (string) $value;
		return $this;
	}

	public function getImage_url() {
		return $this->_image_url;
	}

}