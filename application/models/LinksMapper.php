<?php

class Application_Model_LinksMapper {

	protected $_dbTable;

	public function setDbTable($dbTable) {
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;
		return $this;
    }
 
	public function getDbTable() {
		if (null === $this->_dbTable) {
			$this->setDbTable('Application_Model_DbTable_Links');
		}
		return $this->_dbTable;
	}
	
	public function save(Application_Model_Links $entry) {
		$data = array(
			'id'			=> $entry->getId(),
			'order'			=> $entry->getOrder(),
 			'url'			=> $entry->getUrl(),
 			'title'			=> $entry->getTitle(),
 			'description'	=> $entry->getDescription(),
 			'image_url'		=> $entry->getImage_url(),
 			'tstamp'		=> $entry->getTstamp()
		);

		if (null === ($id = $entry->getId())) {
			unset($data['id']);
			$data['id'] = $this->getDbTable()->insert($data);
			$response = '<div class="row dojoDndItem" id="' . $data['id'] . '">';
			$response .= '<div class="linkImage">';
			$response .= '<a href="http://' . $data['url'] . '" target="_blank"><img src="/images/links/thumbs/' . $data['image_url'] . '" alt="Link Image" width="75" border="0" height="75"></a>';
			$response .= '</div>';
			$response .= '<div class="linkText">';
			$response .= '<h3><a href="http://' . $data['url'] . '" target="_blank">' . stripslashes($data['title']) . '</a></h3>';
			$response .= '<p>' . $data['description'] . '</p>';
			$response .= '</div>';
			$response .= '<div class="actions">';
			$response .= '<a href="/admin/links/add/' . $data['id'] . '/"><button type="button" value="edit" class="blue edit">edit</button></a>';
			$response .= '<a href="/admin/links/delete/' . $data['id'] . '/"><button type="button" value="delete" class="red delete">delete</button></a>';
			$response .= '</div>';
			$response .= '<div style="clear: both;"></div>';
			$response .= '</div>';
			
			echo $response;
		} else {
			$this->getDbTable()->update($data, array('id = ?' => $id));
			echo $id;
		}
	}

	public function find($id, Application_Model_Links $entry) {
		$result = $this->getDbTable()->find($id);
		if (0 == count($result)) {
			return;
		}
		$row = $result->current();
		$entry->setId($row->id)
			->setOrder($row->order)
			->setUrl($row->url)
			->setTitle($row->title)
			->setDescription($row->description)
			->setImage_url($row->image_url)
			->setTstamp($row->tstamp);
		return $result;
	}

	public function fetchAll() {
		$resultSet = $this->getDbTable()->fetchAll(null, 'order ASC');
		$entries = array();
 		foreach($resultSet as $row) {
			$entry = new Application_Model_Links();
			$entry->setId($row->id)
				->setOrder($row->order)
				->setUrl($row->url)
				->setTitle($row->title)
				->setDescription($row->description)
				->setImage_url($row->image_url)
				->setTstamp($row->tstamp);
 			$entries[] = $entry;
 		}
 		return $entries;
	}
	
	public function delete($id) {
		$this->getDbTable()->delete('id = ' . $id);
		echo 'Link item deleted.';
	}
}