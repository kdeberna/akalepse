<?php

class Application_Model_Comments {

	protected $_id = null;
	protected $_news_id = '';
	protected $_ip_address = '';
	protected $_name = '';
	protected $_email = '';
	protected $_comment = '';
	protected $_tstamp = null;

	public function __construct(array $options = null) {
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}

	public function __set($name, $value) {
		$method = 'set' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Comments property');
		}
		$this->$method($value);
	}

	public function __get($name) {
		$method = 'get' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Comments property');
		}
		return $this->$method();
	}

	public function setOptions(array $options) {
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}

	public function setId($id) {
		$this->_id = (int) $id;
		return $this;
	}

	public function getId() {
		return $this->_id;
	}

	public function setNews_id($value) {
		$this->_news_id = (int) $value;
		return $this;
	}

	public function getNews_id() {
		return $this->_news_id;
	}
 
	public function setIp_address($value) {
		$this->_ip_address = (string) $value;
		return $this;
	}

	public function getIp_address() {
		return $this->_ip_address;
	}

	public function setName($value) {
		$this->_name = (string) $value;
		return $this;
	}

	public function getName() {
		return $this->_name;
	}

	public function setEmail($value) {
		$this->_email = (string) $value;
		return $this;
	}

	public function getEmail() {
		return $this->_email;
	}

	public function setComment($value) {
		$this->_comment = $value;
		return $this;
	}

	public function getComment() {
		return $this->_comment;
	}

	public function setTstamp($value) {
		$this->_tstamp = $value;
		return $this;
	}

	public function getTstamp() {
		return $this->_tstamp;
	}

}