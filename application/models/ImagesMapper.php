<?php

class Application_Model_ImagesMapper {

	protected $_dbTable;

	public function setDbTable($dbTable) {
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;
		return $this;
    }
 
	public function getDbTable() {
		if (null === $this->_dbTable) {
			$this->setDbTable('Application_Model_DbTable_Images');
		}
		return $this->_dbTable;
	}
	
	public function save(Application_Model_Images $image) {
		$data = array(
			'id'		=> $image->getId(),
			'album_id'	=> $image->getAlbum_id(),
 			'order'		=> $image->getOrder(),
 			'cover'		=> $image->getCover(),
 			'filename'	=> $image->getFilename(),
			'caption'	=> $image->getCaption(),
			'credit'	=> $image->getCredit(),
		);

		if (null === ($id = $image->getId())) {
			unset($data['id']);
			$this->getDbTable()->insert($data);
			echo 'New image added: ' . $data['filename'];
		} else {
			$this->getDbTable()->update($data, array('id = ?' => $id));
			echo 'Image updated.';
		}
	}

	public function find($id, Application_Model_Images $image) {
		$result = $this->getDbTable()->find($id);
		if (0 == count($result)) {
			return;
		}
		$row = $result->current();
		$image->setId($row->id)
		->setAlbum_id($row->album_id)
		->setOrder($row->order)
		->setCover($row->cover)
		->setFilename($row->filename)
		->setCaption($row->caption)
		->setCredit($row->credit);
	}

	public function fetchAll() {
		$resultSet = $this->getDbTable()->fetchAll();
		$entries = array();
		foreach ($resultSet as $row) {
			$entry = new Application_Model_Images();
			$entry->setId($row->id)
			->setAlbum_id($row->album_id)
			->setOrder($row->order)
			->setCover($row->cover)
			->setFilename($row->filename)
			->setCaption($row->caption)
			->setCredit($row->credit);
			$entries[] = $entry;
		}
		return $entries;
	}

	public function fetchByAlbumId($id) {
		$resultSet = $this->getDbTable()->fetchAll('album_id = ' . "$id", 'order ASC');
		$entries = array();
		foreach ($resultSet as $row) {
			$entry = new Application_Model_Images();
			$entry->setId($row->id)
			->setAlbum_id($row->album_id)
			->setOrder($row->order)
			->setCover($row->cover)
			->setFilename($row->filename)
			->setCaption($row->caption)
			->setCredit($row->credit);
			$entries[] = $entry;
		}
		return $entries;
	}
		
	public function delete($id) {
		$this->getDbTable()->delete('id = ' . $id);
		echo 'Image deleted.';
	}
}