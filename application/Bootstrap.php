<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

	protected function _initViewHelpers() {
		$this->bootstrap('view');
		$view = $this->getResource('view');
		$view->doctype('XHTML1_STRICT');
		$view->headTitle()->setSeparator(' // '); 
		$view->headTitle('dj akalepse');
		$view->addHelperPath('Zend/Dojo/View/Helper/', 'Zend_Dojo_View_Helper');
		$view->addHelperPath('App/Helper/', 'App_Helper');
	}
	
	protected function _initActionHelpers() {
		Zend_Controller_Action_HelperBroker::addPath('App/Controller/Action/Helper/', 'App_Controller_Action_Helper');
	}
	
	protected function _initNavigation() {
		$this->bootstrap('layout');
		$layout = $this->getResource('layout');
		$view = $layout->getView();
		$mainNav = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation.xml', 'nav');
		$adminNav = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation.xml', 'admin');
		$containerMain = new Zend_Navigation($mainNav);
		$containerAdmin = new Zend_Navigation($adminNav);
		Zend_Registry::set('main', $containerMain);
		Zend_Registry::set('admin', $containerAdmin);
		$view->navigation($containerMain);
	}

	protected function _initDatabase() {
		$options = $this->getOptions();
		$dbAdapter = Zend_Db::factory($options['resources']['db']['adapter'], $options['resources']['db']['params']);
		$registry = Zend_Registry::getInstance();
		$registry->database = $dbAdapter;
	}

	protected function _initRoutes() {
		$frontController = Zend_Controller_Front::getInstance();
		$router = $frontController->getRouter();
		$route = new Zend_Controller_Router_Route(
			'/events/:id',
			array(
				'module'		=> 'default',
				'controller'	=> 'events',
				'action'		=> 'single'
			)
		);
		$router->addRoute('getEventId', $route);
		$route = new Zend_Controller_Router_Route(
			'/events/silent/',
			array(
				'module'		=> 'default',
				'controller'	=> 'events',
				'action'		=> 'silent'
			)
		);
		$router->addRoute('getSilentEvent', $route);
		$download = new Zend_Controller_Router_Route(
			'/download/:id',
			array(
				'module'		=> 'default',
				'controller'	=> 'download',
				'action'		=> 'index'
			)
		);
		$router->addRoute('getDownloadId', $download);
		$discog = new Zend_Controller_Router_Route(
			'/discography/:id',
			array(
				'module'		=> 'default',
				'controller'	=> 'discography',
				'action'		=> 'single'
			)
		);
		$router->addRoute('getDiscogId', $discog);
		$gallery = new Zend_Controller_Router_Route(
			'/photos/:gallery',
			array(
				'module'		=> 'default',
				'controller'	=> 'photos',
				'action'		=> 'gallery'
			)
		);
		$router->addRoute('getGalleryId', $gallery);
		$slideshow = new Zend_Controller_Router_Route(
			'/photos/slideshow/:gallery/:image',
			array(
				'module'		=> 'default',
				'controller'	=> 'photos',
				'action'		=> 'slideshow'
			)
		);
		$router->addRoute('getSlideshowId', $slideshow);
		$news = new Zend_Controller_Router_Route(
			'/news/:id',
			array(
				'module'		=> 'default',
				'controller'	=> 'news',
				'action' 		=> 'single'
			)
		);
		$router->addRoute('getNewsId', $news);
		$album = new Zend_Controller_Router_Route(
			'/player/:id',
			array(
				'module'		=> 'default',
				'controller'	=> 'player',
				'action'		=> 'index'
			)
		);
		$router->addRoute('getAlbumId', $album);
	}
	
	protected function _initAutoLoader() {
		$autoloader = Zend_Loader_Autoloader::getInstance();
		$autoloader->registerNamespace('App_');
		$autoloader->registerNamespace('GetID3_');
	}

    protected function _initZFDebug() {
		$options = $this->getOptions();
		if(isset($options['zfdebug']['active']) && $options['zfdebug']['active']) {
			$autoloader = Zend_Loader_Autoloader::getInstance();
			$autoloader->registerNamespace('ZFDebug');
		
			$options = array(
				'plugins' => array('Variables', 
								   //'Database' => array('adapter' => $db), 
								   'File' => array('basePath' => APPLICATION_PATH . '/application'),
								   'Memory', 
								   'Time',
								   //'Text',
								   //'Interface',
								   'Html',
								   //'Auth',
								   'Registry', 
								   //'Cache' => array('backend' => $cache->getBackend()), 
								   'Exception')
			);
			$debug = new ZFDebug_Controller_Plugin_Debug($options);
		
			$this->bootstrap('frontController');
			$frontController = $this->getResource('frontController');
			$frontController->registerPlugin($debug);
		}
    }
    
    protected function _initAcl() {
		$front = Zend_Controller_Front::getInstance();
		$front->registerPlugin(new App_Controller_Plugin_Acl());
    }

}