<?php

class App_Helper_NextEvent {
	
	public $id;
	public $title;
	public $date;
	public $who;
	public $venue;
	public $address;
	public $cover;
	public $flyerfront;

    public function nextEvent() {
        $events = new Application_Model_EventsMapper();
		$eventArray = $events->fetchNext();
		if(!$eventArray) {
			$this->id			= '';
			$this->title		= 'No upcoming events';
			$this->date			= '';
			$this->who			= '';
			$this->venue		= '';
			$this->address		= '';
			$this->cover		= '';
		} else {        
			// sort by date
			$this->id			= $eventArray[0]->id;
			$this->title		= $eventArray[0]->title;
			$this->date			= $eventArray[0]->date;
			$this->flyerfront	= $eventArray[0]->flyerfront;
			$this->who			= $eventArray[0]->who;
			$this->venue		= $eventArray[0]->venue;
			$this->address		= $eventArray[0]->address;
			$this->cover		= $eventArray[0]->cover;
        }

        return $this;
    }

}