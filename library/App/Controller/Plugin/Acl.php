<?php

class App_Controller_Plugin_Acl extends Zend_Controller_Plugin_Abstract {

	public function preDispatch(Zend_Controller_Request_Abstract $request) {
 		$acl = new Zend_Acl();
 
 		$acl->addRole(new Zend_Acl_Role('guest'));
 		$acl->addRole(new Zend_Acl_Role('admin'), 'guest');
 
 		$acl->add(new Zend_Acl_Resource('default'));
  		$acl->add(new Zend_Acl_Resource('admin'));
 		
 		$acl->allow(null, 'default');
 		$acl->allow('admin', null);
		
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity()) {
			$identity = $auth->getIdentity();
			$role = strtolower($identity->role);
		} else {
			$role = 'guest';
		}

		$registry = Zend_Registry::getInstance();		
		$registry->set('Zend_Acl', $acl);
		
		$module = $request->module;
		$controller = $request->controller;
		$action = $request->action;
		
		if(!$acl->isAllowed($role, $module)) {
			if($role == 'guest') {
				$request->setModuleName('admin');
				$request->setControllerName('index');
				$request->setActionName('index');
			} else {
				$request->setControllerName('error');
				$request->setActionName('index');
			}
		}
	}
}