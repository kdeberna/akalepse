<?php

class App_Controller_Plugin_Layout extends Zend_Layout_Controller_Plugin_Layout {

    public function __construct ($layout = null) {
        parent::__construct ($layout);
    }

    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        // Insert current module layout dir to to overide any default layouts
        if ( $request->getModuleName() != 'default' ) {
			echo 'Not default';
            $layoutPath = APPLICATION_PATH . '/modules/' .
                          $request->getModuleName() . '/views/layouts';

            $paths = array();
            $paths[] = $this->getLayout()->getViewScriptPath();
            $paths[] = $layoutPath;

            $this->getLayout()->setViewScriptPath($paths);
        }
    }
}