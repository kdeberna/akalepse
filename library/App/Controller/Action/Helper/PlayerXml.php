<?php

class App_Controller_Action_Helper_PlayerXml extends Zend_Controller_Action_Helper_Abstract {

    public $pluginLoader;

    public function __construct() {
        $this->pluginLoader = new Zend_Loader_PluginLoader();
	}
	
    public function writeXml() {
    	$xmlData = "<albums>\n";
        $entries = new Application_Model_DiscographyMapper();
        $tracksMapper = new Application_Model_TracksMapper();
        $tracksEntry = new Application_Model_Tracks();
		$discography = $entries->fetchAll();
		foreach($discography as $album) {
			if($album->playable == "1") {
				$xmlData .= "\t<item>\n";
				$albumID = $album->id;
				$tracks = $tracksMapper->find($albumID, $tracksEntry);
				$count = count($tracks);
				$xmlData .= "\t\t<url>/releases/" . $album->dir . "/playlist.xml</url>\n";
				$xmlData .= "\t\t<title>" . $album->title . "</title>\n";
				$xmlData .= "\t\t<artist></artist>\n";
				$xmlData .= "\t\t<date>" . date("F d, Y", strtotime($album->release_date)) . "</date>\n";
				$xmlData .= "\t\t<trackInfo>" . $count . " tracks</trackInfo>\n";
				$xmlData .= "\t\t<extraInfo>" . $album->shortdesc . "</extraInfo>\n";
				$xmlData .= "\t\t<imageUrl>/releases/" . $album->dir . "/" . $album->album_cover . "</imageUrl>\n";
				if($album->downloadable == '1') {
					$xmlData .= "\t\t<downloadUrl>/releases/" . $album->dir . "/" . $album->dir . ".zip</downloadUrl>\n";
				} else {
					$xmlData .= "\t\t<downloadUrl></downloadUrl>\n";
				}
				$xmlData .= "\t</item>\n";
			}
		}
		$xmlData .= '</albums>';
		// write out file
		$handle = fopen(APPLICATION_PATH . '/../html/xml/releases.xml', 'w') or die('Cannot open file.');
		fwrite($handle, $xmlData);
		fclose($handle);
		
		echo 'Data: ' . $xmlData;
    }

    public function direct() {
        $this->writeXml();
    }
}