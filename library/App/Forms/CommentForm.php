<?php

class App_Forms_CommentForm extends Zend_Form {

	public function init() {
		$this->commentName = new Zend_Form_Element_Text('CommentName');
		$this->commentName->setLabel('Name')
			->setValue('Your name')
			->setRequired(true);

		$this->commentEmail = new Zend_Form_Element_Text('CommentEmail');
		$this->commentEmail->setLabel('Email Address')
			->setValue('Your email address')
			->setRequired(true);

		$this->commentComment = new Zend_Form_Element_Textarea('CommentComment');
		$this->commentComment->setLabel('Comment')
			->setValue('Comment here')
			->setRequired(true);

		$this->submit = new Zend_Form_Element_Submit('Send');
		$this->submit->setLabel('Submit Comment');

		$this->setDecorators(array(
			'FormElements',
			array('HtmlTag', array('tag' => 'dl')),
			'Form'
		));
	}
}