<?php

class App_Forms_ContactForm extends Zend_Form {

	public function init() {
		$this->address = new Zend_Form_Element_Text('address');
		$this->address->setLabel('Email Address:')
			->setRequired(true)
			->setValue('Enter your email address');
		
		$this->subject = new Zend_Form_Element_Text('subject');
		$this->subject->setLabel('Email Subject:')
			->setRequired(true)
			->setValue('Enter the email subject');

		$this->body = new Zend_Form_Element_Textarea('body');
		$this->body->setLabel('Email Message:')
			->setRequired(true)
			->setValue('Enter your message here');
		
		$this->submit = new Zend_Form_Element_Submit('Send');
		
		$this->addElements(array(
			$this->address,
			$this->subject,
			$this->body
		));
		
		$this->setDecorators(array(
			'FormElements',
			array('HtmlTag', array('tag' => 'dl')),
			'Form'
		));
	}
}