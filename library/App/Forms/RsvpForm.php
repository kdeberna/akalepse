<?php

class App_Forms_RsvpForm extends Zend_Form {

	public function init() {
		$this->emailAddress = new Zend_Form_Element_Text('email_address');
		$this->emailAddress->setLabel('Email Address:')
			->setRequired(true);

		$this->firstName = new Zend_Form_Element_Text('first_name');
		$this->firstName->setLabel('First Name:')
			->setRequired(true);
		
		$this->lastName = new Zend_Form_Element_Text('last_name');
		$this->lastName->setLabel('Last Name:')
			->setRequired(true);

		$this->mailingList = new Zend_Form_Element_Checkbox('mailing_list');
		$this->mailingList->setLabel('Subscribe me to the mailing list')
			->setChecked(true);
		
		$this->submit = new Zend_Form_Element_Submit('Send');
		
		$this->setDecorators(array(
			'FormElements',
			array('HtmlTag', array('tag' => 'dl')),
			'Form'
		));
	}
}